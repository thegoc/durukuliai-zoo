<?php
// skyriaus_forma.php
// tiesiog rodomas  tekstas ir nuoroda atgal

session_start();

if (!isset($_SESSION['prev']) || ($_SESSION['prev'] != "index"))
{ header("Location: logout.php");exit;}

?>

<html>
    <head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet" />
    <link href="public/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="public/css/demo.css" rel="stylesheet" />
    </head>
    <body>
		
<?php
		include("include/meniu.php"); //įterpiamas meniu pagal vartotojo rolę

?> 
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-16">
                            <div class="card  card-tasks">
                                <div class="card-header ">
                                    <h4 class="card-title">Renginiai</h4>
                                </div>
                                <div class="card-body ">
                                    <div class="table-full-width">
                                        <table class="table">
                                            <tbody>
                                                <thead>
                                                    <th>Pavadinimas</th>
                                                    <th>Data</th>
                                                    <th>Kaina(€)</th>
                                                    <th>Pasirinkimai</th>
                                                </thead>

                                                <?php
                                                $dbc=mysqli_connect("localhost", "root", "", "isp_zoo");
                                                 $result = mysqli_query($dbc, "SELECT * FROM renginys");
                                        while($row = mysqli_fetch_array($result)){
                                            $pavadinimas = $row["pavadinimas"];
                                            $data = $row["data"];
                                            $kaina = $row["kaina"];
                                            $id = $row["id"];
                                            echo "<tr><td>".$pavadinimas."</td>";
                                            echo "<td>".$data."</td>";
                                            echo "<td>".$kaina."</td>";
                                            echo "<td> <a href=\"subscribe_event.php?id=$id\"><button class=\"btn btn-primary\" type=\"submit\">Registruotis</button></a></td>";
                                            echo "<td> <a href=\"unsubscribe_event.php?id=$id\"><button class=\"btn btn-primary\" type=\"submit\">Atšaukti registraciją</button></a></td>";
                                            if ($userlevel == $user_roles["Administratorius"])
                                                echo "<td> <a href=\"event_edit.php?id=$id&pav=$pavadinimas&data=$data&kaina=$kaina\"><button class=\"btn btn-danger\" type=\"submit\">Redaguoti renginį</button></a></td>
                                                 <td> <a href=\"event_delete.php?id=$id\"><button class=\"btn btn-danger\" type=\"submit\">Ištrinti renginį</button></a></td>";
                                        echo "</tr>";
                                            }
                                            ?>                                          

                                            </tbody>
                                        </table>
                                    </div>

                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        
        </div>
    </div>
	
	</body>
</html>
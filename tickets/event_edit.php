<?php
// skyriaus_forma.php
// tiesiog rodomas  tekstas ir nuoroda atgal
include("include/functions.php");
session_start();

if (!isset($_SESSION['prev']) || ($_SESSION['prev'] != "index"))
{ header("Location: logout.php");exit;}

?>

<html>
    <head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet" />
    <link href="public/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="public/css/demo.css" rel="stylesheet" />
    </head>
    <body>
		
<?php
		include("include/meniu.php"); //įterpiamas meniu pagal vartotojo rolę

?> 
<section class="container-fluid bg">
	<section class="row justify-content-center">
			<form class="form-container" action="edit_event.php?edit=<?php echo $_GET['id']; ?>" method="POST" class="login">
				<div class="form-group">
                    <br>
                    <center style="font-size:18pt;"><b>Renginio redagavimas</b></center>
                    
                    <div class="form-group">
					<p style="text-align:left;">
					<label for="exampleInputPassword1">Pavadinimas</label>
					<input class="form-control" name="pavadinimas" type="text" value="<?php echo $_GET['pav']; ?>"/>
					<?php echo $_SESSION['pavadinimas_error']; 
					?>
                  </div>
                  
                  <div class="form-group">
					<p style="text-align:left;">
					<label for="exampleInputPassword1">Data</label>
					<input class="form-control" name="data" type="date" value="<?php echo $_GET['data']; ?>"/>
			  	</div>

				        <p style="text-align:left;"><br>
					<label for="exampleInputKaina">Kaina</label>
					<input class="form-control" name="kaina" type="number" value="<?php echo $_GET['kaina'];  ?>"/>
                    <?php echo $_SESSION['kaina_error']; 
                        //$siuntos_id = $_GET['edit'];
					?>
                  </div>
                  
				  	<button type="submit" name="ok" class="btn btn-primary">Redaguoti</button>
			</form>
	</section>
</section>
            
           
        </div>
    </div>
	
	</body>
</html>
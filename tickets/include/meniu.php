<?php
// meniu.php  rodomas meniu pagal vartotojo rolę

if (!isset($_SESSION)) { header("Location:include/logout.php");exit;}
include("include/nustatymai.php");
$user=$_SESSION['user'];
$userlevel=$_SESSION['ulevel'];
$role="";
{foreach($user_roles as $x=>$x_value)
			      {if ($x_value == $userlevel) $role=$x;}
} 

echo "<div class=\"wrapper\">
<div class=\"sidebar\" data-image=\"../assets/img/sidebar-5.jpg\">
    <div class=\"sidebar-wrapper\">
        <div class=\"logo\">
            <a class=\"simple-text\">Zoo</a>
        </div>
        <ul class=\"nav\">



            <li class=\"nav-item\">
                <a href=\"/users\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Vartotojai</p>
                </a>
            </li>
            <li class=\"nav-item\">
                <a href=\"/animals\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Gyvūnai</p>
                </a>
            </li>
            <li class=\"nav-item\">
                <a href=\"/feed\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Pašarai</p>
                </a>
            </li>
            <li class=\"nav-item active\">
                <a href=\"../tickets/index.php\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Bilietai</p>
                </a>
            </li>
            <li class=\"nav-item\">
                <a href=\"/marketing\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Marketingas</p>
                </a>
            </li>
        </ul>
    </div>
</div>


<div class=\"main-panel\">
    <nav class=\"navbar navbar-expand-lg\" color-on-scroll=\"500\">
        <div class=\"container-fluid\">

            <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\">

                <ul class=\"nav navbar-nav mr-auto\">

                    <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"../tickets/bilietai.php\">
                                <span class=\"no-icon\">Bilietai</span>
                            </a>
                        </li>

                        <li class=\"nav-item\">
                                <a class=\"nav-link\" href=\"../tickets/renginiai.php\">
                                    <span class=\"no-icon\">Renginiai</span>
                                </a>
                            </li>
                            
                            <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"../tickets/my_events.php\">
                                <span class=\"no-icon\">Mano renginiai</span>
                            </a>
                        </li>";

                            if ($userlevel == $user_roles["Administratorius"]) echo 
                            "<li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"../tickets/naujas_renginys.php\">
                                <span class=\"no-icon\">Sukurti naują renginį</span>
                            </a>
                        </li>

                                    <li class=\"nav-item\">
                                            <a class=\"nav-link\" href=\"../tickets/pelnas.php\">
                                                <span class=\"no-icon\">Pelno peržiūra</span>
                                            </a>
                                        </li>";
                echo "</ul>

                <ul class=\"navbar-nav ml-auto\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"logout.php\">
                            <span class=\"no-icon\">Atsijungti</span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <!-- End Navbar -->            
"

?>       
    
 
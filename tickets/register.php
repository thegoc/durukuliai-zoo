<?php
// register.php registracijos forma
// jei pats registruojasi rolė = DEFAULT_LEVEL, jei registruoja ADMIN_LEVEL vartotojas, rolę parenka
// Kaip atsiranda vartotojas: nustatymuose $uregister=
//                                         self - pats registruojasi, admin - tik ADMIN_LEVEL, both - abu atvejai galimi
// formos laukus tikrins procregister.php

session_start();
if (empty($_SESSION['prev'])) { header("Location: logout.php");exit;} // registracija galima kai nera userio arba adminas
// kitaip kai sesija expirinasi blogai, laikykim, kad prev vistik visada nustatoma
include("include/nustatymai.php");
include("include/functions.php");
if ($_SESSION['prev'] != "procregister")  inisession("part");  // pradinis bandymas registruoti

$_SESSION['prev']="register";
?>
    <html>
        <head>  
            <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8"> 
            <title>Registracija</title>
            <link href="include/styles.css" rel="stylesheet" type="text/css" >
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        </head>
        <body>   

						
			<section class="container-fluid bg">
	<section class="row justify-content-center">
			<form class="form-container" action="procregister.php" method="POST" class="login">
				<div class="form-group">
					<center style="font-size:18pt;"><b>Registracija</b></center>
				        <p style="text-align:left;"><br>
					<label for="exampleInputEmail1">Vartotojo vardas</label>
					<input class="form-control" name="user" type="text" value="<?php echo $_SESSION['name_login'];  ?>"/>
					<?php echo $_SESSION['name_error']; 
					?>
			  	</div>				
			  	<div class="form-group">
					<p style="text-align:left;">
					<label for="exampleInputPassword1">Slaptažodis</label>
					<input class="form-control" name="pass" type="password" value="<?php echo $_SESSION['pass_login']; ?>"/>
					<?php echo $_SESSION['pass_error']; 
					?>
			  	</div>
				
				<div class="form-group">
					<p style="text-align:left;">
					<label for="exampleInputPassword1">E-paštas</label>
					<input class="form-control" name="email" type="text" value="<?php echo $_SESSION['mail_login']; ?>"/>
					<?php echo $_SESSION['mail_error']; 
					?>
															<?php
										 if ($_SESSION['ulevel'] == $user_roles[ADMIN_LEVEL] )
										{echo "<p style=\"text-align:left;\">Rolė<br>";
										 echo "<select name=\"role\">";
   									   	 foreach($user_roles as $x=>$x_value)
  											{echo "<option ";
        	 									if ($x == DEFAULT_LEVEL) echo "selected ";
             									echo "value=\"".$x_value."\" ";
         	 									echo ">".$x."</option></p>";
											}
										}
									?>
			  	</div>
				
				
				  	<button type="submit" class="btn btn-primary">Registruoti</button>
					   <a href="index.php">Atgal į pradžia</a>
			</form>
	</section>
</section>
			
			
			
        </body>
    </html>
   

<?php
// skyriaus_forma.php
// tiesiog rodomas  tekstas ir nuoroda atgal

session_start();

if (!isset($_SESSION['prev']) || ($_SESSION['prev'] != "index"))
{ header("Location: logout.php");exit;}

?>

<html>
    <head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet" />
    <link href="public/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="public/css/demo.css" rel="stylesheet" />
    </head>
    <body>
		
<?php
		include("include/meniu.php"); //įterpiamas meniu pagal vartotojo rolę

?> 
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card  card-tasks">
                                <div class="card-header ">
                                    <h4 class="card-title">Pelnas</h4>
                                </div>
                                <div class="card-body ">
                                    <div class="table-full-width">
                                        <table class="table">

                                                <thead>
                                                        <th>Tipas</th>
                                                        <th>Kiekis</th>
                                                        <th>Pelnas</th>
                                                    </thead>

                                            <tbody>
                                                <tr>
                                                        <td>Bilietai</td>

                                    <?php
                                    $bilietuKiekis = 0;
                                    $bilietuKaina = 0;
                                    $dbc=mysqli_connect("localhost", "root", "", "isp_zoo");
                                    $result = mysqli_query($dbc, "SELECT * FROM bilietas");
                                        while($row = mysqli_fetch_array($result)){
                                            $kiekis = $row["kiekis"];
                                            $kaina = $row["kaina"];
                                            $bilietuKiekis += $kiekis;
                                            $asmensKaina = $kaina * $kiekis;
                                            $bilietuKaina += $asmensKaina;
                                            }
                                            echo "<td>".$bilietuKiekis."</td>";
                                            echo "<td>".$bilietuKaina."</td>";
                                    ?>

                                                       
                                                       
                                                </tr>

                                                <tr>
                                                        <td>Renginiai</td>

                                                        <?php
                                           // $sql = "SELECT renginys.pavadinimas as pavadinimas, renginys.data as data, renginys.kaina as kaina FROM registracijos, renginys WHERE registracijos.fk_reg_vartotojas='" . $_SESSION['userid'] . "";
                                            $sql = "SELECT renginys.pavadinimas as pavadinimas, renginys.data as data, renginys.kaina as kaina FROM registracijos, renginys WHERE registracijos.fk_reg_renginys = renginys.id";
                                                $dbc=mysqli_connect("localhost", "root", "", "isp_zoo");
                                                 $result = mysqli_query($dbc, $sql);
                                                 $renginiuSuma = 0;
                                                 $count = 0;
                                        while($row = mysqli_fetch_array($result)){
                                            $pavadinimas = $row["pavadinimas"];
                                            $data = $row["data"];
                                            $kaina = $row["kaina"];
                                           // echo "<tr><td>".$pavadinimas."</td>";   
                                           // echo "<td>".$data."</td>";
                                           $count++;
                                           $renginiuSuma += $kaina;
                                          
                                            }
                                            echo "<td>".$count."</td>";
                                            echo "<td>".$renginiuSuma."</td>";

                                            ?>   
                                                    </tr>

                                                    <tr>
                                                            <td>Bendras pelnas</td>

                                                            <?php
                                                                $bendrasKiekis = $count + $bilietuKiekis;
                                                                $bendraSuma = $renginiuSuma + $bilietuKaina;
                                                                echo "<td>".$bendrasKiekis."</td>";
                                                                echo "<td>".$bendraSuma."</td>";
                                                            ?>
                                                         
                                                        </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            
        </div>
    </div>
	
	</body>
</html>
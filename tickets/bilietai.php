<?php
// skyriaus_forma.php
// tiesiog rodomas  tekstas ir nuoroda atgal

session_start();

if (!isset($_SESSION['prev']) || ($_SESSION['prev'] != "index"))
{ header("Location: logout.php");exit;}

?>

<html>
    <head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet" />
    <link href="public/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="public/css/demo.css" rel="stylesheet" />
    </head>
    <body>
		
<?php
		include("include/meniu.php"); //įterpiamas meniu pagal vartotojo rolę

?> 
            <div class="content">
                <div class="container-fluid">
                    <div class="table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                                <th></th>
                                <th>Kaina(€)</th>
                                <th>Kiekis</th>

                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bilietas</td>
                                    <?php
                                    	$dbc=mysqli_connect("localhost", "root", "", "isp_zoo");
                                        $sql = "SELECT * FROM ticket WHERE ticket.id = 1";
                                        $query = mysqli_query($dbc, $sql);
                                        while($row = mysqli_fetch_array($query)){
                                        $kaina = $row["kaina"];
                                        echo "<td>".$kaina."</td>";
                                        }
                                    ?>
                                    <?php
                                    $dbc=mysqli_connect("localhost", "root", "", "isp_zoo");
                                    $result = mysqli_query($dbc, "SELECT * FROM bilietas WHERE fk_bilietas_vartotojas='" . $_SESSION['userid'] . "'");
                                    if(mysqli_num_rows($result) == 1)
                                    {
                                        while($row = mysqli_fetch_array($result)){
                                            $kiekis = $row["kiekis"];
                                            echo "<td>".$kiekis."</td>";
                                            }
                                    }
                                    else
                                    { 
                                        $kiekis = 0;
                                        echo "<td>0</td>";
                                    }
                                    ?>
                                    <td> <a href="ticket_buy.php?buy=<?php echo $kiekis ?>"><button class="btn btn-primary" type="submit">Pirkti</button></a></td>
                                   <?php if($userlevel == $user_roles["Administratorius"])
                                    echo "<td> <a href=\"ticket_edit.php?edit=$kaina\"><button class=\"btn btn-primary\" type=\"submit\">Redaguoti</button></a></td>";
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    
                    </div>
                </div>
            </div>
            
           
        </div>
    </div>
	
	</body>
</html>
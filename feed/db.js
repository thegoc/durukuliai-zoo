const mysql = require("mysql");
const config = require("config");

const connection = mysql.createConnection({
    //port: config.get("db.DB_PORT"),
    host: config.get("db.DB_HOST"),
    user: config.get("db.DB_USER"),
    //host: "localhost",
    password: config.get("db.DB_PASS"),
    database: config.get("db.DB_NAME")
});

connection.connect(err => {
    if (err) {
        console.log("db conection failed", err);
    } else {
        console.log("db connected");
    }
});

module.exports = connection;

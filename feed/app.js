const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
const flash = require("connect-flash");
const session = require("express-session");
const config = require("config");
const feedRoutes = require("./routes/feed");

app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, "/public")));
console.log("static folderis", path.join(__dirname, "/public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
    session({
        secret: "mysecret",
        resave: true,
        saveUninitialized: true
    })
);
app.use(flash());

// middleware for any route
app.use((req, res, next) => {
    res.locals.currentUser = req.user;
    res.locals.success_message = req.flash("success_message");
    res.locals.error_message = req.flash("error_message");
    res.locals.auth_message = req.flash("auth_message");
    next();
});

app.use("/feed", feedRoutes);

const port = process.env.PORT || config.get("APP_PORT");
app.listen(port, () => {
    console.log(`server listening on port ${port}`);
});

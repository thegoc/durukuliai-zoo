const jwt = require("jsonwebtoken");
const request = require("request");
const config = require("config");

const authManager = (req, res, next) => {
    let managertoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlRvbSIsImlzQWRtaW4iOiJmYWxzZSIsIm5hbWUiOiJNYXJrIiwicm9sZSI6Im1hbmFnZXIiLCJpYXQiOjE1MTYyMzkwMjJ9.dIxC_RI3C-8KsHxBeJkjjmggY4a8UMZ8YOsWYlRHhSA";
    //let managertoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkF1Z3VzdCIsImlzQWRtaW4iOiJmYWxzZSIsIm5hbWUiOiJUaW5hIiwicm9sZSI6IndvcmtlciIsImlhdCI6MTUxNjIzOTAyMn0.WfVbe0MQuzzAjmRQHOzQtjnuksHxA77Ad4EtuU5EdPw";
    if (!managertoken) {
        req.flash("auth_message", "Negalite čia patekti neprisijungęs");
        res.redirect("/feed");
    } else {
        const decoded = jwt.verify(managertoken, config.get("jwt_secret"));
        if (decoded.role !== "manager") {
            req.flash("auth_message", "Negalite čia patekti būdamas ne vadybininkas");
            return res.redirect("/feed");
        }
        console.log(decoded);
        req.user = decoded;
        console.log(req.user);
        next();
    }
};
const authWorker = (req, res, next) => {
    let workertoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkF1Z3VzdCIsImlzQWRtaW4iOiJmYWxzZSIsIm5hbWUiOiJUaW5hIiwicm9sZSI6IndvcmtlciIsImlhdCI6MTUxNjIzOTAyMn0.WfVbe0MQuzzAjmRQHOzQtjnuksHxA77Ad4EtuU5EdPw";
    //let workertoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlRvbSIsImlzQWRtaW4iOiJmYWxzZSIsIm5hbWUiOiJNYXJrIiwicm9sZSI6Im1hbmFnZXIiLCJpYXQiOjE1MTYyMzkwMjJ9.dIxC_RI3C-8KsHxBeJkjjmggY4a8UMZ8YOsWYlRHhSA";

    if (!workertoken) {
        req.flash("auth_message", "Negalite čia patekti neprisijungęs");
        return res.redirect("/feed");
    } else {
        const decoded = jwt.verify(workertoken, config.get("jwt_secret"));
        if (decoded.role !== "worker") {
            req.flash("auth_message", "Negalite čia patekti būdamas ne darbuotojas");
            return res.redirect("/feed");
        }
        console.log(decoded);
        req.user = decoded;
        next();
    }
}

module.exports = { authManager, authWorker };

const express = require("express");
const router = express.Router({ mergeParams: true });
const path = require("path");
const db = require("../db");

const { authManager, authWorker } = require("../middleware/auth");

router.get("/", (req, res) => {
    res.render("index");
});
router.get("/feedanimal", authWorker, (req, res) => {
    const getAnimalsSql = "SELECT id,rusis FROM gyvunas";
    db.query(getAnimalsSql, (err, results) => {
        if (err || !results || results.length < 1) {
            return res.render("feedanimal", {
                message: "Nepavyko rasti gyvūnų"
            });
        } else return res.render("feedanimal", { animals: results });
    });
});
router.post("/feedanimal", authWorker, (req, res) => {
    let animal = req.body.animalID;
    if (animal === "none") {
        req.flash("error_message", "Įvestas blogas gyvūnas");
        return res.redirect("back");
    }
    const sql = "INSERT INTO serimas (gyvunas_id, vartotojas_id) VALUES (?,?)";
    db.query(
        {
            sql: sql,
            timeout: 5000,
            values: [animal, req.user.id || 1]
        },
        err => {
            if (err) {
                console.log(err);
                req.flash("error_message", "Įvyko klaida papildant DB lentelę");
                return res.redirect("back");
            }
            req.flash(
                "success_message",
                `Gyvūnas su ID: ${animal} pažymėtas kaip pašertas`
            );
            return res.redirect("back");
        }
    );
});
router.get("/writeoff", authManager, (req, res) => {
    let sql = `SELECT id,pavadinimas,kam_skirta,DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,kiekis FROM zoo_pasaras`;
    if (req.query.expired) {
        sql = `SELECT id,pavadinimas,kam_skirta,DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,kiekis FROM zoo_pasaras WHERE galioja_iki < ?`;
    }
    db.query(sql, [new Date()], (err, results) => {
        if (err || !results || results.length < 1) {
            console.log(err);
            console.log(results);
            console.log("ok");
            return res.render("writeoff", { message: "Nepavyko rasti pašarų" });
        }
        return res.render("writeoff", { feeds: results });
    });
});
router.post("/writeoff", authManager, (req, res) => {
    if (req.body.ids) {
        const ids = req.body.ids;
        ids.forEach(id => {
            const sql = "DELETE FROM zoo_pasaras WHERE zoo_pasaras.id = ?";
            db.query(sql, [id], err => {
                if (err) {
                    req.flash("error_message", "Klaida šalinant pašarus");
                    return res.redirect("back");
                }
            });
        });
        req.flash("success_message", "Pašarai pašalinti");
    }
    return res.redirect("back");
});
router.get("/usage", authManager, (req, res) => {
    const sql = `SELECT 
                    id,
                    pavadinimas,
                    kam_skirta,
                    pagaminimo_data,
                    DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,
                    kiekis 
                FROM zoo_pasaras`;
    db.query(sql, (err, results) => {
        if (err || !results || results.length < 1) {
            return res.render("usage", { message: "Nepavyko rasti pašarų" });
        }
        return res.render("usage", { feed: results });
    });
});
router.get("/order", authManager, (req, res) => {
    let suppliers = [];
    let cities = [];
    let sql = `SELECT
                    tp.id, 
                    tp.pavadinimas, 
                    tp.kam_skirta,
                    tp.kaina,
                    DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,
                    t.pavadinimas AS tiekejas,
                    t.miestas  
                FROM tiekejo_pasaras tp INNER JOIN tiekejas t ON tp.tiekejas_id = t.pavadinimas`;
    const sqlSuppliers =
        "SELECT pavadinimas from tiekejas GROUP BY pavadinimas";
    const sqlCities = "SELECT miestas from tiekejas GROUP BY miestas";

    if (req.query.supplier) {
        sql = `SELECT
                    tp.id, 
                    tp.pavadinimas, 
                    tp.kam_skirta,
                    tp.kaina,
                    DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,
                    t.pavadinimas AS tiekejas,
                    t.miestas  
                FROM tiekejo_pasaras tp INNER JOIN tiekejas t ON tp.tiekejas_id = t.pavadinimas WHERE t.pavadinimas = ?`;
    } else if (req.query.city) {
        sql = `SELECT
                    tp.id, 
                    tp.pavadinimas, 
                    tp.kam_skirta,
                    tp.kaina,
                    DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,
                    t.pavadinimas AS tiekejas,
                    t.miestas  
                FROM tiekejo_pasaras tp 
                INNER JOIN tiekejas t ON tp.tiekejas_id = t.pavadinimas 
                WHERE t.miestas = ?`;
    } else if (req.query.priceFrom && req.query.priceTo) {
        sql = `SELECT
                    tp.id, 
                    tp.pavadinimas, 
                    tp.kam_skirta,
                    tp.kaina,
                    DATE_FORMAT(galioja_iki,  "%Y-%m-%d") AS galioja_iki,
                    t.pavadinimas AS tiekejas,
                    t.miestas  
                FROM tiekejo_pasaras tp 
                INNER JOIN tiekejas t ON tp.tiekejas_id = t.pavadinimas 
                WHERE tp.kaina BETWEEN ${req.query.priceFrom} AND ${req.query.priceTo}`;
    }

    db.query(sqlSuppliers, (err, results) => {
        if (!err) {
            suppliers = results;
        }
    });
    db.query(sqlCities, (err, results) => {
        if (!err) {
            cities = results;
        }
    });
    db.query(sql, [req.query.supplier || req.query.city], (err, results) => {
        if (err || !results || results.length < 1) {
            return res.render("order", {
                message: "Nepavyko rasti pašarų"
            });
        }
        return res.render("order", {
            feeds: results,
            suppliers: suppliers,
            cities: cities
        });
    });
});
router.post("/order", authManager, (req, res) => {
    const ids = [...req.body.ids];
    const sql = `INSERT INTO zoo_pasaras (pavadinimas,kam_skirta,pagaminimo_data,galioja_iki, kiekis) 
            SELECT pavadinimas,kam_skirta,pagaminimo_data,galioja_iki,30
            FROM tiekejo_pasaras tp
            WHERE tp.id = ?`;
    const sqlOrder = `INSERT INTO uzsakymas (vartotojas_id) VALUES(?)`;
    const sql_uzsakymo_pasarai =
        "INSERT INTO uzsakymo_pasarai (tiekejo_pasaras_id, uzsakymas_id) VALUES(?,?)";
    db.query(sqlOrder, req.user.id || 1, (err, row) => {
        if (err) {
            console.log(err);
            req.flash("error_message", "Klaida iterpiant į uzsakymas");
        } else {
            ids.forEach(id => {
                db.query(sql, id, err => {
                    if (err) {
                        console.log(err);
                        req.flash(
                            "error_message",
                            "Klaida iterpiant į zoo pašarus"
                        );
                    }
                });
                db.query(sql_uzsakymo_pasarai, [id, row.insertId], err => {
                    if (err) {
                        console.log(err);
                        req.flash(
                            "error_message",
                            "Klaida iterpiant į uzsakymo pasarus"
                        );
                        return res.redirect("back");
                    }
                });
            });
        }
    });

    req.flash("success_message", "Užsakymas sėkmingai įvykdytas");
    return res.redirect("back");
});
router.get("/suppliers", authManager, (req, res) => {
    let sql = "SELECT * from tiekejas";
    let supplierFeedSql =
        "SELECT t.pavadinimas, tp.pavadinimas AS pasaro_pavadinimas, tp.kaina,tp.kam_skirta FROM tiekejas t INNER JOIN tiekejo_pasaras tp ON tp.tiekejas_id = t.pavadinimas WHERE t.pavadinimas = ?";
    let suppliers = [];
    db.query(sql, (error, results) => {
        if (error || !results || results.length == 0) {
            req.flash("error_message", "Nepavyko rasti tiekėjų");
            return res.render("suppliers");
        } else {
            results.forEach(res => {
                suppliers.push({ supplierInfo: res });
            });
            suppliers.forEach((supplier, index, arr) => {
                db.query(
                    supplierFeedSql,
                    [supplier.supplierInfo.pavadinimas],
                    (err, result) => {
                        if (err) {
                            return res.render("supliers");
                        }
                        suppliers[index].supplierInfo.feed = result;
                        if (index + 1 === arr.length) {
                            return res.render("suppliers", {
                                suppliers: suppliers
                            });
                        }
                    }
                );
            });
        }
    });
});

module.exports = router;

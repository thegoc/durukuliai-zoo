const btn = document.querySelectorAll('.dispDetails');


function myFunction(button) {
    const details = document.querySelectorAll('.details')
    var index = button.getAttribute('data-table-index');
    console.log(index);

    if (details[index].style.display === "block") {
        btn[index].innerHTML="Rodyti pašarus";
        btn[index].classList.remove("active");
        details[index].style.display = "none";
    } else {
        btn[index].innerHTML="Slėpti įrašus";
        btn[index].classList.add("active");
        details[index].style.display = "block";
        
    }
}
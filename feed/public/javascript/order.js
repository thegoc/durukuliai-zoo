function handleSelect() {
    const priceForm = document.querySelector("#priceForm");
    const cityForm = document.querySelector("#cityForm");
    const supplierForm = document.querySelector("#supplierForm");

    var filterVal = document.getElementById("filterBy");
    if (filterVal.value === "none") {
        priceForm.style.display = "none";
        cityForm.style.display = "none";
        supplierForm.style.display = "none";
    } else if (filterVal.value === "price") {
        priceForm.style.display = "block";
        cityForm.style.display = "none";
        supplierForm.style.display = "none";
    } else if (filterVal.value === "city") {
        priceForm.style.display = "none";
        cityForm.style.display = "block";
        supplierForm.style.display = "none";
    } else if (filterVal.value === "supplier") {
        priceForm.style.display = "none";
        cityForm.style.display = "none";
        supplierForm.style.display = "block";
    }
}

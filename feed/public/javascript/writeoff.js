const form = document.getElementById("expiredForm");
const showAllForm = document.getElementById("allForm");
const expireDates = document.querySelectorAll("#expires");
const expireRows = document.querySelectorAll(".expireRows");

const expired = document.getElementById("expired");

expired.addEventListener("change", submitExpired);
showAllForm.addEventListener("change", submitShowAll);

let urlParams = new URLSearchParams(window.location.search);

function submitExpired() {
    expireRows.forEach((row, index) => {
        let dateString = expireDates[index].innerHTML;
        if (dateString.trim().length != 0) {
            let splittedDate = dateString.split("-");
            let date = new Date(...splittedDate);
            let currentDate = new Date();
            if (currentDate < date) {
                row.classList.toggle("d-none");
            }
        }
    });
}

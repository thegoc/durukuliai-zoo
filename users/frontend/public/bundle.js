
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.head.appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    const identity = x => x;
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function validate_store(store, name) {
        if (!store || typeof store.subscribe !== 'function') {
            throw new Error(`'${name}' is not a store with a 'subscribe' method`);
        }
    }
    function subscribe(store, callback) {
        const unsub = store.subscribe(callback);
        return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }
    function component_subscribe(component, store, callback) {
        component.$$.on_destroy.push(subscribe(store, callback));
    }
    function create_slot(definition, ctx, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, fn) {
        return definition[1]
            ? assign({}, assign(ctx.$$scope.ctx, definition[1](fn ? fn(ctx) : {})))
            : ctx.$$scope.ctx;
    }
    function get_slot_changes(definition, ctx, changed, fn) {
        return definition[1]
            ? assign({}, assign(ctx.$$scope.changed || {}, definition[1](fn ? fn(changed) : {})))
            : ctx.$$scope.changed || {};
    }
    const has_prop = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);

    const is_client = typeof window !== 'undefined';
    let now = is_client
        ? () => window.performance.now()
        : () => Date.now();
    let raf = is_client ? cb => requestAnimationFrame(cb) : noop;

    const tasks = new Set();
    let running = false;
    function run_tasks() {
        tasks.forEach(task => {
            if (!task[0](now())) {
                tasks.delete(task);
                task[1]();
            }
        });
        running = tasks.size > 0;
        if (running)
            raf(run_tasks);
    }
    function loop(fn) {
        let task;
        if (!running) {
            running = true;
            raf(run_tasks);
        }
        return {
            promise: new Promise(fulfil => {
                tasks.add(task = [fn, fulfil]);
            }),
            abort() {
                tasks.delete(task);
            }
        };
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        if (value != null || input.value) {
            input.value = value;
        }
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let stylesheet;
    let active = 0;
    let current_rules = {};
    // https://github.com/darkskyapp/string-hash/blob/master/index.js
    function hash(str) {
        let hash = 5381;
        let i = str.length;
        while (i--)
            hash = ((hash << 5) - hash) ^ str.charCodeAt(i);
        return hash >>> 0;
    }
    function create_rule(node, a, b, duration, delay, ease, fn, uid = 0) {
        const step = 16.666 / duration;
        let keyframes = '{\n';
        for (let p = 0; p <= 1; p += step) {
            const t = a + (b - a) * ease(p);
            keyframes += p * 100 + `%{${fn(t, 1 - t)}}\n`;
        }
        const rule = keyframes + `100% {${fn(b, 1 - b)}}\n}`;
        const name = `__svelte_${hash(rule)}_${uid}`;
        if (!current_rules[name]) {
            if (!stylesheet) {
                const style = element('style');
                document.head.appendChild(style);
                stylesheet = style.sheet;
            }
            current_rules[name] = true;
            stylesheet.insertRule(`@keyframes ${name} ${rule}`, stylesheet.cssRules.length);
        }
        const animation = node.style.animation || '';
        node.style.animation = `${animation ? `${animation}, ` : ``}${name} ${duration}ms linear ${delay}ms 1 both`;
        active += 1;
        return name;
    }
    function delete_rule(node, name) {
        node.style.animation = (node.style.animation || '')
            .split(', ')
            .filter(name
            ? anim => anim.indexOf(name) < 0 // remove specific animation
            : anim => anim.indexOf('__svelte') === -1 // remove all Svelte animations
        )
            .join(', ');
        if (name && !--active)
            clear_rules();
    }
    function clear_rules() {
        raf(() => {
            if (active)
                return;
            let i = stylesheet.cssRules.length;
            while (i--)
                stylesheet.deleteRule(i);
            current_rules = {};
        });
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    function add_flush_callback(fn) {
        flush_callbacks.push(fn);
    }
    function flush() {
        const seen_callbacks = new Set();
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    callback();
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update($$.dirty);
            run_all($$.before_update);
            $$.fragment && $$.fragment.p($$.dirty, $$.ctx);
            $$.dirty = null;
            $$.after_update.forEach(add_render_callback);
        }
    }

    let promise;
    function wait() {
        if (!promise) {
            promise = Promise.resolve();
            promise.then(() => {
                promise = null;
            });
        }
        return promise;
    }
    function dispatch(node, direction, kind) {
        node.dispatchEvent(custom_event(`${direction ? 'intro' : 'outro'}${kind}`));
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }
    const null_transition = { duration: 0 };
    function create_bidirectional_transition(node, fn, params, intro) {
        let config = fn(node, params);
        let t = intro ? 0 : 1;
        let running_program = null;
        let pending_program = null;
        let animation_name = null;
        function clear_animation() {
            if (animation_name)
                delete_rule(node, animation_name);
        }
        function init(program, duration) {
            const d = program.b - t;
            duration *= Math.abs(d);
            return {
                a: t,
                b: program.b,
                d,
                duration,
                start: program.start,
                end: program.start + duration,
                group: program.group
            };
        }
        function go(b) {
            const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
            const program = {
                start: now() + delay,
                b
            };
            if (!b) {
                // @ts-ignore todo: improve typings
                program.group = outros;
                outros.r += 1;
            }
            if (running_program) {
                pending_program = program;
            }
            else {
                // if this is an intro, and there's a delay, we need to do
                // an initial tick and/or apply CSS animation immediately
                if (css) {
                    clear_animation();
                    animation_name = create_rule(node, t, b, duration, delay, easing, css);
                }
                if (b)
                    tick(0, 1);
                running_program = init(program, duration);
                add_render_callback(() => dispatch(node, b, 'start'));
                loop(now => {
                    if (pending_program && now > pending_program.start) {
                        running_program = init(pending_program, duration);
                        pending_program = null;
                        dispatch(node, running_program.b, 'start');
                        if (css) {
                            clear_animation();
                            animation_name = create_rule(node, t, running_program.b, running_program.duration, 0, easing, config.css);
                        }
                    }
                    if (running_program) {
                        if (now >= running_program.end) {
                            tick(t = running_program.b, 1 - t);
                            dispatch(node, running_program.b, 'end');
                            if (!pending_program) {
                                // we're done
                                if (running_program.b) {
                                    // intro — we can tidy up immediately
                                    clear_animation();
                                }
                                else {
                                    // outro — needs to be coordinated
                                    if (!--running_program.group.r)
                                        run_all(running_program.group.c);
                                }
                            }
                            running_program = null;
                        }
                        else if (now >= running_program.start) {
                            const p = now - running_program.start;
                            t = running_program.a + running_program.d * easing(p / running_program.duration);
                            tick(t, 1 - t);
                        }
                    }
                    return !!(running_program || pending_program);
                });
            }
        }
        return {
            run(b) {
                if (is_function(config)) {
                    wait().then(() => {
                        // @ts-ignore
                        config = config();
                        go(b);
                    });
                }
                else {
                    go(b);
                }
            },
            end() {
                clear_animation();
                running_program = pending_program = null;
            }
        };
    }

    function destroy_block(block, lookup) {
        block.d(1);
        lookup.delete(block.key);
    }
    function update_keyed_each(old_blocks, changed, get_key, dynamic, ctx, list, lookup, node, destroy, create_each_block, next, get_context) {
        let o = old_blocks.length;
        let n = list.length;
        let i = o;
        const old_indexes = {};
        while (i--)
            old_indexes[old_blocks[i].key] = i;
        const new_blocks = [];
        const new_lookup = new Map();
        const deltas = new Map();
        i = n;
        while (i--) {
            const child_ctx = get_context(ctx, list, i);
            const key = get_key(child_ctx);
            let block = lookup.get(key);
            if (!block) {
                block = create_each_block(key, child_ctx);
                block.c();
            }
            else if (dynamic) {
                block.p(changed, child_ctx);
            }
            new_lookup.set(key, new_blocks[i] = block);
            if (key in old_indexes)
                deltas.set(key, Math.abs(i - old_indexes[key]));
        }
        const will_move = new Set();
        const did_move = new Set();
        function insert(block) {
            transition_in(block, 1);
            block.m(node, next);
            lookup.set(block.key, block);
            next = block.first;
            n--;
        }
        while (o && n) {
            const new_block = new_blocks[n - 1];
            const old_block = old_blocks[o - 1];
            const new_key = new_block.key;
            const old_key = old_block.key;
            if (new_block === old_block) {
                // do nothing
                next = new_block.first;
                o--;
                n--;
            }
            else if (!new_lookup.has(old_key)) {
                // remove old block
                destroy(old_block, lookup);
                o--;
            }
            else if (!lookup.has(new_key) || will_move.has(new_key)) {
                insert(new_block);
            }
            else if (did_move.has(old_key)) {
                o--;
            }
            else if (deltas.get(new_key) > deltas.get(old_key)) {
                did_move.add(new_key);
                insert(new_block);
            }
            else {
                will_move.add(old_key);
                o--;
            }
        }
        while (o--) {
            const old_block = old_blocks[o];
            if (!new_lookup.has(old_block.key))
                destroy(old_block, lookup);
        }
        while (n)
            insert(new_blocks[n - 1]);
        return new_blocks;
    }

    function bind(component, name, callback) {
        if (has_prop(component.$$.props, name)) {
            name = component.$$.props[name] || name;
            component.$$.bound[name] = callback;
            callback(component.$$.ctx[name]);
        }
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = {};
        }
    }
    function make_dirty(component, key) {
        if (!component.$$.dirty) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty = blank_object();
        }
        component.$$.dirty[key] = true;
    }
    function init(component, options, instance, create_fragment, not_equal, props) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty: null
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (key, ret, value = ret) => {
                if ($$.ctx && not_equal($$.ctx[key], $$.ctx[key] = value)) {
                    if ($$.bound[key])
                        $$.bound[key](value);
                    if (ready)
                        make_dirty(component, key);
                }
                return ret;
            })
            : prop_values;
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(children(options.target));
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, detail));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function prop_dev(node, property, value) {
        node[property] = value;
        dispatch_dev("SvelteDOMSetProperty", { node, property, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.data === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
    }

    function cubicOut(t) {
        const f = t - 1.0;
        return f * f * f + 1.0;
    }

    function fade(node, { delay = 0, duration = 400, easing = identity }) {
        const o = +getComputedStyle(node).opacity;
        return {
            delay,
            duration,
            easing,
            css: t => `opacity: ${t * o}`
        };
    }
    function fly(node, { delay = 0, duration = 400, easing = cubicOut, x = 0, y = 0, opacity = 0 }) {
        const style = getComputedStyle(node);
        const target_opacity = +style.opacity;
        const transform = style.transform === 'none' ? '' : style.transform;
        const od = target_opacity * (1 - opacity);
        return {
            delay,
            duration,
            easing,
            css: (t, u) => `
			transform: ${transform} translate(${(1 - t) * x}px, ${(1 - t) * y}px);
			opacity: ${target_opacity - (od * u)}`
        };
    }

    /* src/UI/Modal.svelte generated by Svelte v3.15.0 */
    const file = "src/UI/Modal.svelte";

    function create_fragment(ctx) {
    	let div0;
    	let div0_transition;
    	let t0;
    	let div2;
    	let h3;
    	let t1;
    	let t2;
    	let div1;
    	let div2_transition;
    	let current;
    	let dispose;
    	const default_slot_template = ctx.$$slots.default;
    	const default_slot = create_slot(default_slot_template, ctx, null);

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			t0 = space();
    			div2 = element("div");
    			h3 = element("h3");
    			t1 = text(ctx.title);
    			t2 = space();
    			div1 = element("div");
    			if (default_slot) default_slot.c();
    			attr_dev(div0, "class", "modal-backdrop-custom");
    			add_location(div0, file, 14, 0, 298);
    			attr_dev(h3, "class", "mb-3");
    			add_location(h3, file, 16, 4, 437);
    			add_location(div1, file, 17, 4, 471);
    			attr_dev(div2, "class", "modal-custom p-3");
    			add_location(div2, file, 15, 0, 374);
    			dispose = listen_dev(div0, "click", ctx.closeModal, false, false, false);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div2, anchor);
    			append_dev(div2, h3);
    			append_dev(h3, t1);
    			append_dev(div2, t2);
    			append_dev(div2, div1);

    			if (default_slot) {
    				default_slot.m(div1, null);
    			}

    			current = true;
    		},
    		p: function update(changed, ctx) {
    			if (!current || changed.title) set_data_dev(t1, ctx.title);

    			if (default_slot && default_slot.p && changed.$$scope) {
    				default_slot.p(get_slot_changes(default_slot_template, ctx, changed, null), get_slot_context(default_slot_template, ctx, null));
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			add_render_callback(() => {
    				if (!div0_transition) div0_transition = create_bidirectional_transition(div0, fade, {}, true);
    				div0_transition.run(1);
    			});

    			transition_in(default_slot, local);

    			add_render_callback(() => {
    				if (!div2_transition) div2_transition = create_bidirectional_transition(div2, fly, { y: 300 }, true);
    				div2_transition.run(1);
    			});

    			current = true;
    		},
    		o: function outro(local) {
    			if (!div0_transition) div0_transition = create_bidirectional_transition(div0, fade, {}, false);
    			div0_transition.run(0);
    			transition_out(default_slot, local);
    			if (!div2_transition) div2_transition = create_bidirectional_transition(div2, fly, { y: 300 }, false);
    			div2_transition.run(0);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching && div0_transition) div0_transition.end();
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div2);
    			if (default_slot) default_slot.d(detaching);
    			if (detaching && div2_transition) div2_transition.end();
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { title } = $$props;
    	const dispatch = createEventDispatcher();

    	function closeModal() {
    		dispatch("cancel");
    	}

    	const writable_props = ["title"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Modal> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;

    	$$self.$set = $$props => {
    		if ("title" in $$props) $$invalidate("title", title = $$props.title);
    		if ("$$scope" in $$props) $$invalidate("$$scope", $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => {
    		return { title };
    	};

    	$$self.$inject_state = $$props => {
    		if ("title" in $$props) $$invalidate("title", title = $$props.title);
    	};

    	return { title, closeModal, $$slots, $$scope };
    }

    class Modal extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { title: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Modal",
    			options,
    			id: create_fragment.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || ({});

    		if (ctx.title === undefined && !("title" in props)) {
    			console.warn("<Modal> was created without expected prop 'title'");
    		}
    	}

    	get title() {
    		throw new Error("<Modal>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set title(value) {
    		throw new Error("<Modal>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/UI/Input.svelte generated by Svelte v3.15.0 */

    const file$1 = "src/UI/Input.svelte";

    // (10:4) {#if label != null}
    function create_if_block(ctx) {
    	let label_1;
    	let t;

    	const block = {
    		c: function create() {
    			label_1 = element("label");
    			t = text(ctx.label);
    			add_location(label_1, file$1, 10, 8, 183);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, label_1, anchor);
    			append_dev(label_1, t);
    		},
    		p: function update(changed, ctx) {
    			if (changed.label) set_data_dev(t, ctx.label);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(label_1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(10:4) {#if label != null}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$1(ctx) {
    	let div;
    	let t;
    	let input;
    	let dispose;
    	let if_block = ctx.label != null && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if (if_block) if_block.c();
    			t = space();
    			input = element("input");
    			attr_dev(input, "type", "text");
    			attr_dev(input, "name", ctx.name);
    			attr_dev(input, "class", "form-control");
    			attr_dev(input, "placeholder", ctx.placeholder);
    			add_location(input, file$1, 12, 4, 220);
    			attr_dev(div, "class", "form-group");
    			add_location(div, file$1, 8, 0, 126);
    			dispose = listen_dev(input, "input", ctx.input_input_handler);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if (if_block) if_block.m(div, null);
    			append_dev(div, t);
    			append_dev(div, input);
    			set_input_value(input, ctx.value);
    		},
    		p: function update(changed, ctx) {
    			if (ctx.label != null) {
    				if (if_block) {
    					if_block.p(changed, ctx);
    				} else {
    					if_block = create_if_block(ctx);
    					if_block.c();
    					if_block.m(div, t);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}

    			if (changed.name) {
    				attr_dev(input, "name", ctx.name);
    			}

    			if (changed.placeholder) {
    				attr_dev(input, "placeholder", ctx.placeholder);
    			}

    			if (changed.value && input.value !== ctx.value) {
    				set_input_value(input, ctx.value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if (if_block) if_block.d();
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let { label = null } = $$props;
    	let { placeholder } = $$props;
    	let { name } = $$props;
    	let { value = "" } = $$props;
    	const writable_props = ["label", "placeholder", "name", "value"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Input> was created with unknown prop '${key}'`);
    	});

    	function input_input_handler() {
    		value = this.value;
    		$$invalidate("value", value);
    	}

    	$$self.$set = $$props => {
    		if ("label" in $$props) $$invalidate("label", label = $$props.label);
    		if ("placeholder" in $$props) $$invalidate("placeholder", placeholder = $$props.placeholder);
    		if ("name" in $$props) $$invalidate("name", name = $$props.name);
    		if ("value" in $$props) $$invalidate("value", value = $$props.value);
    	};

    	$$self.$capture_state = () => {
    		return { label, placeholder, name, value };
    	};

    	$$self.$inject_state = $$props => {
    		if ("label" in $$props) $$invalidate("label", label = $$props.label);
    		if ("placeholder" in $$props) $$invalidate("placeholder", placeholder = $$props.placeholder);
    		if ("name" in $$props) $$invalidate("name", name = $$props.name);
    		if ("value" in $$props) $$invalidate("value", value = $$props.value);
    	};

    	return {
    		label,
    		placeholder,
    		name,
    		value,
    		input_input_handler
    	};
    }

    class Input extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {
    			label: 0,
    			placeholder: 0,
    			name: 0,
    			value: 0
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Input",
    			options,
    			id: create_fragment$1.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || ({});

    		if (ctx.placeholder === undefined && !("placeholder" in props)) {
    			console.warn("<Input> was created without expected prop 'placeholder'");
    		}

    		if (ctx.name === undefined && !("name" in props)) {
    			console.warn("<Input> was created without expected prop 'name'");
    		}
    	}

    	get label() {
    		throw new Error("<Input>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set label(value) {
    		throw new Error("<Input>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get placeholder() {
    		throw new Error("<Input>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set placeholder(value) {
    		throw new Error("<Input>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get name() {
    		throw new Error("<Input>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set name(value) {
    		throw new Error("<Input>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get value() {
    		throw new Error("<Input>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set value(value) {
    		throw new Error("<Input>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    const subscriber_queue = [];
    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     * @param {*=}value initial value
     * @param {StartStopNotifier=}start start and stop notifications for subscriptions
     */
    function writable(value, start = noop) {
        let stop;
        const subscribers = [];
        function set(new_value) {
            if (safe_not_equal(value, new_value)) {
                value = new_value;
                if (stop) { // store is ready
                    const run_queue = !subscriber_queue.length;
                    for (let i = 0; i < subscribers.length; i += 1) {
                        const s = subscribers[i];
                        s[1]();
                        subscriber_queue.push(s, value);
                    }
                    if (run_queue) {
                        for (let i = 0; i < subscriber_queue.length; i += 2) {
                            subscriber_queue[i][0](subscriber_queue[i + 1]);
                        }
                        subscriber_queue.length = 0;
                    }
                }
            }
        }
        function update(fn) {
            set(fn(value));
        }
        function subscribe(run, invalidate = noop) {
            const subscriber = [run, invalidate];
            subscribers.push(subscriber);
            if (subscribers.length === 1) {
                stop = start(set) || noop;
            }
            run(value);
            return () => {
                const index = subscribers.indexOf(subscriber);
                if (index !== -1) {
                    subscribers.splice(index, 1);
                }
                if (subscribers.length === 0) {
                    stop();
                    stop = null;
                }
            };
        }
        return { set, update, subscribe };
    }

    //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiZWxfcGFzdGFzIjoiYWRtaW5Ac3lzdGVtIiwidmFyZGFzIjoiU3lzdGVtIiwicGF2YXJkZSI6IkFkbWluIiwiYXJfYWRtaW5pc3RyYXRvcml1cyI6dHJ1ZSwic3VrdXJpbW9fZGF0YSI6IjIwMTktMTEtMDFUMDA6MDA6MDBaIiwiaXN0cnluaW1vX2RhdGEiOm51bGwsInN1a3VydGFfdmFydG90b2pvIjoxLCJ0ZWlzZXMiOm51bGwsInJvbGVzX2lkIjpudWxsLCJyb2xlIjpudWxsLCJleHAiOjE1NzUwNDE4Njl9.7tgD1S-gEwkjATTaxFaIFN4roVIt4FypyxRbRNe1bfo
    const users = writable({ isSet: false, data: [] });


    const usersStore = {
        subscribe: users.subscribe,
        set: u => {
            users.set({ isSet: true, data: u });
        },
        add: u => {
            users.update(item => {
                let usersCopy = [ ...item.data ];
                usersCopy = [ u, ...usersCopy ];
                return { isSet: item.isSet, data: usersCopy };
            });
        },
        delete: u => {
            users.update(item => {
                let usersCopy = [ ...item.data ];
                usersCopy = usersCopy.filter(i => i.id !== u);
                return { isSet: item.isSet, data: usersCopy };
            });
        },
        updateRole: (u, r) => {
            users.update(item => {
                let index = item.data.findIndex(i => i.id === u);
                let usersCopy = [ ...item.data ];
                usersCopy[index].roles_id = r;
                return { isSet: item.isSet, data: usersCopy };
            });
        }
    };

    //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiZWxfcGFzdGFzIjoiYWRtaW5Ac3lzdGVtIiwidmFyZGFzIjoiU3lzdGVtIiwicGF2YXJkZSI6IkFkbWluIiwiYXJfYWRtaW5pc3RyYXRvcml1cyI6dHJ1ZSwic3VrdXJpbW9fZGF0YSI6IjIwMTktMTEtMDFUMDA6MDA6MDBaIiwiaXN0cnluaW1vX2RhdGEiOm51bGwsInN1a3VydGFfdmFydG90b2pvIjoxLCJ0ZWlzZXMiOm51bGwsInJvbGVzX2lkIjpudWxsLCJyb2xlIjpudWxsLCJleHAiOjE1NzUwNDE4Njl9.7tgD1S-gEwkjATTaxFaIFN4roVIt4FypyxRbRNe1bfo
    const users$1 = writable({ isSet: false, data: [] });


    const usersStore$1 = {
        subscribe: users$1.subscribe,
        set: u => {
            users$1.set({ isSet: true, data: u });
        },
        add: u => {
            users$1.update(item => {
                let usersCopy = [ ...item.data ];
                usersCopy = [ u, ...usersCopy ];
                return { isSet: item.isSet, data: usersCopy };
            });
        },
        delete: u => {
            users$1.update(item => {
                let usersCopy = [ ...item.data ];
                usersCopy = usersCopy.filter(i => i.id !== u);
                return { isSet: item.isSet, data: usersCopy };
            });
        }
    };

    function isLongerThan(val, l) {
        return val.trim().length > l;
    }
    function isValidEmail(val) {
        return new RegExp(
            "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
        ).test(val);
    }

    function GET(url, authorization = true) {
        return request("GET", url, null, authorization)
    }
    function POST(url, body, authorization = true, formData = false) {
        return request("POST", url, body, authorization, formData)
    }
    function PATCH(url, body, authorization = true, formData = false) {
        return request("PATCH", url, body, authorization, formData)
    }
    function DELETE(url, authorization = true) {
        return request("DELETE", url, null, authorization)
    }


    // --------------NOT EXPORTED------------------
    function request(method, url, body, authorization, formData) {
        let headers = {"Content-Type": "application/json"};
        if (authorization) {
            headers["Authorization"] = "Bearer " + localStorage.getItem("token");
        }
        let reqBody = {
            method: method,
            headers: headers
        };
        if (method === "POST" || method === "PUT" || method === "PATCH") {
            if (formData) {
                reqBody.body = body;
                delete reqBody.headers["Content-Type"];
            } else {
                reqBody.body = JSON.stringify(body);
            }
        }

        let error = null;
        return fetch('http://localhost:1323' + url, reqBody).then(res => {
            if(!res.ok) {
                if(authorization && res.status === 401) {
                    window.location.replace("/login");
                    localStorage.clear();
                    return;
                }
                error = {
                    status:     res.status,
                    statusText: res.statusText,
                };
            }
            return res.text();
        }).then(data => {
            if (error != null) {
                error.body = data ? JSON.parse(data) : {};
                return {
                    status: false,
                    data: error
                };
            }
            return {
                status: true,
                data: data ? JSON.parse(data) : {}
            };
        });
    }

    const roles = writable({ isSet: false, data: [] });


    const rolesStore = {
        subscribe: roles.subscribe,
        set: u => {
            roles.set({ isSet: true, data: u });
        },
        add: u => {
            roles.update(item => {
                let rolesCopy = [ ...item.data ];
                rolesCopy = [ ...rolesCopy, u ];
                return { isSet: item.isSet, data: rolesCopy };
            });
        },
        delete: r => {
            roles.update(item => {
                let rolesCopy = [ ...item.data ];
                rolesCopy = rolesCopy.filter(i => i.id !== r);
                return { isSet: item.isSet, data: rolesCopy };
            });
        }
    };

    /* src/pages/select-role.svelte generated by Svelte v3.15.0 */
    const file$2 = "src/pages/select-role.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.role = list[i];
    	child_ctx.i = i;
    	return child_ctx;
    }

    // (32:4) {#each localRoles.data as role, i (role.id)}
    function create_each_block(key_1, ctx) {
    	let label;
    	let t0_value = ctx.role.pavadinimas + "";
    	let t0;
    	let t1;
    	let input;
    	let input_value_value;
    	let t2;
    	let span;
    	let t3;
    	let dispose;

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			label = element("label");
    			t0 = text(t0_value);
    			t1 = space();
    			input = element("input");
    			t2 = space();
    			span = element("span");
    			t3 = space();
    			attr_dev(input, "type", "radio");
    			input.__value = input_value_value = ctx.role.id;
    			input.value = input.__value;
    			ctx.$$binding_groups[0].push(input);
    			add_location(input, file$2, 33, 12, 902);
    			attr_dev(span, "class", "checkmark");
    			add_location(span, file$2, 34, 12, 989);
    			attr_dev(label, "class", "container-a");
    			add_location(label, file$2, 32, 8, 844);

    			dispose = [
    				listen_dev(input, "change", ctx.input_change_handler),
    				listen_dev(input, "change", ctx.change, false, false, false)
    			];

    			this.first = label;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, label, anchor);
    			append_dev(label, t0);
    			append_dev(label, t1);
    			append_dev(label, input);
    			input.checked = input.__value === ctx.rls;
    			append_dev(label, t2);
    			append_dev(label, span);
    			append_dev(label, t3);
    		},
    		p: function update(changed, ctx) {
    			if (changed.localRoles && t0_value !== (t0_value = ctx.role.pavadinimas + "")) set_data_dev(t0, t0_value);

    			if (changed.localRoles && input_value_value !== (input_value_value = ctx.role.id)) {
    				prop_dev(input, "__value", input_value_value);
    			}

    			input.value = input.__value;

    			if (changed.rls) {
    				input.checked = input.__value === ctx.rls;
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(label);
    			ctx.$$binding_groups[0].splice(ctx.$$binding_groups[0].indexOf(input), 1);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(32:4) {#each localRoles.data as role, i (role.id)}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$2(ctx) {
    	let div;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value = ctx.localRoles.data;
    	const get_key = ctx => ctx.role.id;

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "ml-5");
    			add_location(div, file$2, 30, 0, 768);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p: function update(changed, ctx) {
    			const each_value = ctx.localRoles.data;
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, div, destroy_block, create_each_block, null, get_each_context);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let $roles;
    	validate_store(rolesStore, "roles");
    	component_subscribe($$self, rolesStore, $$value => $$invalidate("$roles", $roles = $$value));
    	let { user = null } = $$props;
    	let localRoles = $roles;
    	let rls = user.roles_id;

    	onMount(() => {
    		if (!localRoles.isSet) {
    			GET("/api/roles").then(res => {
    				if (res.status) {
    					rolesStore.set(res.data);
    					$$invalidate("localRoles", localRoles.data = res.data, localRoles);
    				}
    			});
    		}
    	});

    	function change() {
    		PATCH("/api/users/" + user.id + "/role", { roles_id: rls }).then(res => {
    			if (res.status) {
    				usersStore.updateRole(user.id, rls);
    			}
    		});
    	}

    	const writable_props = ["user"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Select_role> was created with unknown prop '${key}'`);
    	});

    	const $$binding_groups = [[]];

    	function input_change_handler() {
    		rls = this.__value;
    		$$invalidate("rls", rls);
    	}

    	$$self.$set = $$props => {
    		if ("user" in $$props) $$invalidate("user", user = $$props.user);
    	};

    	$$self.$capture_state = () => {
    		return { user, localRoles, rls, $roles };
    	};

    	$$self.$inject_state = $$props => {
    		if ("user" in $$props) $$invalidate("user", user = $$props.user);
    		if ("localRoles" in $$props) $$invalidate("localRoles", localRoles = $$props.localRoles);
    		if ("rls" in $$props) $$invalidate("rls", rls = $$props.rls);
    		if ("$roles" in $$props) rolesStore.set($roles = $$props.$roles);
    	};

    	return {
    		user,
    		localRoles,
    		rls,
    		change,
    		input_change_handler,
    		$$binding_groups
    	};
    }

    class Select_role extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, { user: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Select_role",
    			options,
    			id: create_fragment$2.name
    		});
    	}

    	get user() {
    		throw new Error("<Select_role>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set user(value) {
    		throw new Error("<Select_role>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/pages/users.svelte generated by Svelte v3.15.0 */
    const file$3 = "src/pages/users.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.user = list[i];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.user = list[i];
    	return child_ctx;
    }

    // (101:0) {#if showCreateModal}
    function create_if_block_2(ctx) {
    	let current;

    	const modal = new Modal({
    			props: {
    				title: "Pridėti naują vartotoją",
    				$$slots: { default: [create_default_slot_1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	modal.$on("cancel", ctx.closeCreateModal);

    	const block = {
    		c: function create() {
    			create_component(modal.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(modal, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const modal_changes = {};

    			if (changed.$$scope || changed.error || changed.validForm || changed.loadingCreate || changed.newUser) {
    				modal_changes.$$scope = { changed, ctx };
    			}

    			modal.$set(modal_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(modal.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(modal.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(modal, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(101:0) {#if showCreateModal}",
    		ctx
    	});

    	return block;
    }

    // (110:12) {:else}
    function create_else_block_1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Sukurti");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block_1.name,
    		type: "else",
    		source: "(110:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (108:12) {#if loadingCreate}
    function create_if_block_4(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Kraunama...");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_4.name,
    		type: "if",
    		source: "(108:12) {#if loadingCreate}",
    		ctx
    	});

    	return block;
    }

    // (115:8) {#if error != null}
    function create_if_block_3(ctx) {
    	let div;
    	let span;
    	let t1;
    	let strong;
    	let t3;
    	let t4;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			span = element("span");
    			span.textContent = "×";
    			t1 = space();
    			strong = element("strong");
    			strong.textContent = "Klaida!";
    			t3 = space();
    			t4 = text(ctx.error);
    			attr_dev(span, "class", "closebtn");
    			add_location(span, file$3, 116, 16, 3825);
    			add_location(strong, file$3, 117, 16, 3909);
    			attr_dev(div, "class", "alert bg-danger mt-3 mb-0");
    			add_location(div, file$3, 115, 12, 3769);
    			dispose = listen_dev(span, "click", ctx.click_handler, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, span);
    			append_dev(div, t1);
    			append_dev(div, strong);
    			append_dev(div, t3);
    			append_dev(div, t4);
    		},
    		p: function update(changed, ctx) {
    			if (changed.error) set_data_dev(t4, ctx.error);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_3.name,
    		type: "if",
    		source: "(115:8) {#if error != null}",
    		ctx
    	});

    	return block;
    }

    // (102:4) <Modal title="Pridėti naują vartotoją" on:cancel="{closeCreateModal}">
    function create_default_slot_1(ctx) {
    	let updating_value;
    	let t0;
    	let updating_value_1;
    	let t1;
    	let updating_value_2;
    	let t2;
    	let button;
    	let button_disabled_value;
    	let t3;
    	let if_block1_anchor;
    	let current;
    	let dispose;

    	function input0_value_binding(value) {
    		ctx.input0_value_binding.call(null, value);
    	}

    	let input0_props = { placeholder: "Vardas", name: "name" };

    	if (ctx.newUser.vardas !== void 0) {
    		input0_props.value = ctx.newUser.vardas;
    	}

    	const input0 = new Input({ props: input0_props, $$inline: true });
    	binding_callbacks.push(() => bind(input0, "value", input0_value_binding));

    	function input1_value_binding(value_1) {
    		ctx.input1_value_binding.call(null, value_1);
    	}

    	let input1_props = { placeholder: "Pavardė", name: "name" };

    	if (ctx.newUser.pavarde !== void 0) {
    		input1_props.value = ctx.newUser.pavarde;
    	}

    	const input1 = new Input({ props: input1_props, $$inline: true });
    	binding_callbacks.push(() => bind(input1, "value", input1_value_binding));

    	function input2_value_binding(value_2) {
    		ctx.input2_value_binding.call(null, value_2);
    	}

    	let input2_props = { placeholder: "El. paštas", name: "email" };

    	if (ctx.newUser.el_pastas !== void 0) {
    		input2_props.value = ctx.newUser.el_pastas;
    	}

    	const input2 = new Input({ props: input2_props, $$inline: true });
    	binding_callbacks.push(() => bind(input2, "value", input2_value_binding));

    	function select_block_type(changed, ctx) {
    		if (ctx.loadingCreate) return create_if_block_4;
    		return create_else_block_1;
    	}

    	let current_block_type = select_block_type(null, ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = ctx.error != null && create_if_block_3(ctx);

    	const block = {
    		c: function create() {
    			create_component(input0.$$.fragment);
    			t0 = space();
    			create_component(input1.$$.fragment);
    			t1 = space();
    			create_component(input2.$$.fragment);
    			t2 = space();
    			button = element("button");
    			if_block0.c();
    			t3 = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			button.disabled = button_disabled_value = !ctx.validForm;
    			attr_dev(button, "class", "btn btn-warning btn-fill w-100");
    			add_location(button, file$3, 106, 8, 3498);
    			dispose = listen_dev(button, "click", ctx.create, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			mount_component(input0, target, anchor);
    			insert_dev(target, t0, anchor);
    			mount_component(input1, target, anchor);
    			insert_dev(target, t1, anchor);
    			mount_component(input2, target, anchor);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, button, anchor);
    			if_block0.m(button, null);
    			insert_dev(target, t3, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, if_block1_anchor, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const input0_changes = {};

    			if (!updating_value && changed.newUser) {
    				updating_value = true;
    				input0_changes.value = ctx.newUser.vardas;
    				add_flush_callback(() => updating_value = false);
    			}

    			input0.$set(input0_changes);
    			const input1_changes = {};

    			if (!updating_value_1 && changed.newUser) {
    				updating_value_1 = true;
    				input1_changes.value = ctx.newUser.pavarde;
    				add_flush_callback(() => updating_value_1 = false);
    			}

    			input1.$set(input1_changes);
    			const input2_changes = {};

    			if (!updating_value_2 && changed.newUser) {
    				updating_value_2 = true;
    				input2_changes.value = ctx.newUser.el_pastas;
    				add_flush_callback(() => updating_value_2 = false);
    			}

    			input2.$set(input2_changes);

    			if (current_block_type !== (current_block_type = select_block_type(changed, ctx))) {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(button, null);
    				}
    			}

    			if (!current || changed.validForm && button_disabled_value !== (button_disabled_value = !ctx.validForm)) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}

    			if (ctx.error != null) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    				} else {
    					if_block1 = create_if_block_3(ctx);
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(input0.$$.fragment, local);
    			transition_in(input1.$$.fragment, local);
    			transition_in(input2.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(input0.$$.fragment, local);
    			transition_out(input1.$$.fragment, local);
    			transition_out(input2.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(input0, detaching);
    			if (detaching) detach_dev(t0);
    			destroy_component(input1, detaching);
    			if (detaching) detach_dev(t1);
    			destroy_component(input2, detaching);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(button);
    			if_block0.d();
    			if (detaching) detach_dev(t3);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(if_block1_anchor);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1.name,
    		type: "slot",
    		source: "(102:4) <Modal title=\\\"Pridėti naują vartotoją\\\" on:cancel=\\\"{closeCreateModal}\\\">",
    		ctx
    	});

    	return block;
    }

    // (124:0) {#if showInfoModal}
    function create_if_block_1(ctx) {
    	let current;

    	const modal = new Modal({
    			props: {
    				title: "Vartotojo rolė",
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	modal.$on("cancel", ctx.cancel_handler);

    	const block = {
    		c: function create() {
    			create_component(modal.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(modal, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const modal_changes = {};

    			if (changed.$$scope || changed.selectedUser) {
    				modal_changes.$$scope = { changed, ctx };
    			}

    			modal.$set(modal_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(modal.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(modal.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(modal, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(124:0) {#if showInfoModal}",
    		ctx
    	});

    	return block;
    }

    // (125:4) <Modal title="Vartotojo rolė" on:cancel="{() => {showInfoModal = false}}">
    function create_default_slot(ctx) {
    	let current;

    	const selectrole = new Select_role({
    			props: { user: ctx.selectedUser },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(selectrole.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(selectrole, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const selectrole_changes = {};
    			if (changed.selectedUser) selectrole_changes.user = ctx.selectedUser;
    			selectrole.$set(selectrole_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(selectrole.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(selectrole.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(selectrole, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot.name,
    		type: "slot",
    		source: "(125:4) <Modal title=\\\"Vartotojo rolė\\\" on:cancel=\\\"{() => {showInfoModal = false}}\\\">",
    		ctx
    	});

    	return block;
    }

    // (212:0) {:else}
    function create_else_block(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Šį puslapį gali matyti tik administratorius.");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(212:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (130:0) {#if isAdmin}
    function create_if_block$1(ctx) {
    	let button;
    	let t1;
    	let div3;
    	let div2;
    	let div0;
    	let h40;
    	let t3;
    	let div1;
    	let table0;
    	let thead0;
    	let tr0;
    	let th0;
    	let t5;
    	let th1;
    	let t7;
    	let th2;
    	let t9;
    	let th3;
    	let t11;
    	let th4;
    	let t13;
    	let th5;
    	let t14;
    	let tbody0;
    	let t15;
    	let div7;
    	let div6;
    	let div4;
    	let h41;
    	let t17;
    	let div5;
    	let table1;
    	let thead1;
    	let tr1;
    	let th6;
    	let t19;
    	let th7;
    	let t21;
    	let th8;
    	let t23;
    	let th9;
    	let t25;
    	let th10;
    	let t27;
    	let th11;
    	let t28;
    	let tbody1;
    	let dispose;
    	let each_value_1 = ctx.localUsers.data;
    	let each_blocks_1 = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks_1[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	let each_value = ctx.localUsersDeleted.data;
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			button = element("button");
    			button.textContent = "Pridėti naują vartotoją";
    			t1 = space();
    			div3 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			h40 = element("h4");
    			h40.textContent = "Aktyvūs sistemos vartotojai";
    			t3 = space();
    			div1 = element("div");
    			table0 = element("table");
    			thead0 = element("thead");
    			tr0 = element("tr");
    			th0 = element("th");
    			th0.textContent = "ID";
    			t5 = space();
    			th1 = element("th");
    			th1.textContent = "Vardas Pavardė";
    			t7 = space();
    			th2 = element("th");
    			th2.textContent = "El. paštas";
    			t9 = space();
    			th3 = element("th");
    			th3.textContent = "Ar administratorius";
    			t11 = space();
    			th4 = element("th");
    			th4.textContent = "Sukūrimo data";
    			t13 = space();
    			th5 = element("th");
    			t14 = space();
    			tbody0 = element("tbody");

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].c();
    			}

    			t15 = space();
    			div7 = element("div");
    			div6 = element("div");
    			div4 = element("div");
    			h41 = element("h4");
    			h41.textContent = "Archyve esantys sistemos vartotojai";
    			t17 = space();
    			div5 = element("div");
    			table1 = element("table");
    			thead1 = element("thead");
    			tr1 = element("tr");
    			th6 = element("th");
    			th6.textContent = "ID";
    			t19 = space();
    			th7 = element("th");
    			th7.textContent = "Vardas Pavardė";
    			t21 = space();
    			th8 = element("th");
    			th8.textContent = "El. paštas";
    			t23 = space();
    			th9 = element("th");
    			th9.textContent = "Ar administratorius";
    			t25 = space();
    			th10 = element("th");
    			th10.textContent = "Archyvavimo data";
    			t27 = space();
    			th11 = element("th");
    			t28 = space();
    			tbody1 = element("tbody");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(button, "type", "button");
    			attr_dev(button, "class", "btn btn-success btn-fill ml-3 mb-3");
    			add_location(button, file$3, 130, 4, 4178);
    			attr_dev(h40, "class", "card-title");
    			add_location(h40, file$3, 134, 16, 4456);
    			attr_dev(div0, "class", "card-header ");
    			add_location(div0, file$3, 133, 12, 4413);
    			add_location(th0, file$3, 140, 24, 4742);
    			add_location(th1, file$3, 141, 24, 4778);
    			add_location(th2, file$3, 142, 24, 4826);
    			add_location(th3, file$3, 143, 24, 4870);
    			add_location(th4, file$3, 144, 24, 4923);
    			add_location(th5, file$3, 145, 24, 4970);
    			add_location(tr0, file$3, 139, 20, 4713);
    			add_location(thead0, file$3, 138, 20, 4685);
    			add_location(tbody0, file$3, 149, 20, 5056);
    			attr_dev(table0, "class", "table table-hover table-striped");
    			add_location(table0, file$3, 137, 16, 4617);
    			attr_dev(div1, "class", "card-body table-full-width table-responsive");
    			add_location(div1, file$3, 136, 12, 4543);
    			attr_dev(div2, "class", "card strpied-tabled-with-hover");
    			add_location(div2, file$3, 132, 8, 4356);
    			attr_dev(div3, "class", "col-md-12");
    			add_location(div3, file$3, 131, 4, 4324);
    			attr_dev(h41, "class", "card-title");
    			add_location(h41, file$3, 174, 16, 6267);
    			attr_dev(div4, "class", "card-header ");
    			add_location(div4, file$3, 173, 12, 6224);
    			add_location(th6, file$3, 181, 28, 6570);
    			add_location(th7, file$3, 182, 28, 6610);
    			add_location(th8, file$3, 183, 28, 6662);
    			add_location(th9, file$3, 184, 28, 6710);
    			add_location(th10, file$3, 185, 28, 6767);
    			add_location(th11, file$3, 186, 28, 6821);
    			add_location(tr1, file$3, 180, 24, 6537);
    			add_location(thead1, file$3, 178, 20, 6504);
    			add_location(tbody1, file$3, 191, 20, 6912);
    			attr_dev(table1, "class", "table table-hover table-striped");
    			add_location(table1, file$3, 177, 16, 6436);
    			attr_dev(div5, "class", "card-body table-full-width table-responsive");
    			add_location(div5, file$3, 176, 12, 6362);
    			attr_dev(div6, "class", "card strpied-tabled-with-hover");
    			add_location(div6, file$3, 172, 8, 6167);
    			attr_dev(div7, "class", "col-md-12");
    			add_location(div7, file$3, 171, 4, 6135);
    			dispose = listen_dev(button, "click", ctx.click_handler_1, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div2);
    			append_dev(div2, div0);
    			append_dev(div0, h40);
    			append_dev(div2, t3);
    			append_dev(div2, div1);
    			append_dev(div1, table0);
    			append_dev(table0, thead0);
    			append_dev(thead0, tr0);
    			append_dev(tr0, th0);
    			append_dev(tr0, t5);
    			append_dev(tr0, th1);
    			append_dev(tr0, t7);
    			append_dev(tr0, th2);
    			append_dev(tr0, t9);
    			append_dev(tr0, th3);
    			append_dev(tr0, t11);
    			append_dev(tr0, th4);
    			append_dev(tr0, t13);
    			append_dev(tr0, th5);
    			append_dev(table0, t14);
    			append_dev(table0, tbody0);

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].m(tbody0, null);
    			}

    			insert_dev(target, t15, anchor);
    			insert_dev(target, div7, anchor);
    			append_dev(div7, div6);
    			append_dev(div6, div4);
    			append_dev(div4, h41);
    			append_dev(div6, t17);
    			append_dev(div6, div5);
    			append_dev(div5, table1);
    			append_dev(table1, thead1);
    			append_dev(thead1, tr1);
    			append_dev(tr1, th6);
    			append_dev(tr1, t19);
    			append_dev(tr1, th7);
    			append_dev(tr1, t21);
    			append_dev(tr1, th8);
    			append_dev(tr1, t23);
    			append_dev(tr1, th9);
    			append_dev(tr1, t25);
    			append_dev(tr1, th10);
    			append_dev(tr1, t27);
    			append_dev(tr1, th11);
    			append_dev(table1, t28);
    			append_dev(table1, tbody1);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(tbody1, null);
    			}
    		},
    		p: function update(changed, ctx) {
    			if (changed.del || changed.localUsers || changed.showInfoModal || changed.selectedUser) {
    				each_value_1 = ctx.localUsers.data;
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks_1[i]) {
    						each_blocks_1[i].p(changed, child_ctx);
    					} else {
    						each_blocks_1[i] = create_each_block_1(child_ctx);
    						each_blocks_1[i].c();
    						each_blocks_1[i].m(tbody0, null);
    					}
    				}

    				for (; i < each_blocks_1.length; i += 1) {
    					each_blocks_1[i].d(1);
    				}

    				each_blocks_1.length = each_value_1.length;
    			}

    			if (changed.antiDel || changed.localUsersDeleted) {
    				each_value = ctx.localUsersDeleted.data;
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(changed, child_ctx);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(tbody1, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div3);
    			destroy_each(each_blocks_1, detaching);
    			if (detaching) detach_dev(t15);
    			if (detaching) detach_dev(div7);
    			destroy_each(each_blocks, detaching);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(130:0) {#if isAdmin}",
    		ctx
    	});

    	return block;
    }

    // (152:20) {#each localUsers.data as user}
    function create_each_block_1(ctx) {
    	let tr;
    	let td0;
    	let t0_value = ctx.user.id + "";
    	let t0;
    	let t1;
    	let td1;
    	let t2_value = ctx.user.vardas + "";
    	let t2;
    	let t3;
    	let t4_value = ctx.user.pavarde + "";
    	let t4;
    	let t5;
    	let td2;
    	let t6_value = ctx.user.el_pastas + "";
    	let t6;
    	let t7;
    	let td3;
    	let t8_value = (ctx.user.ar_administratorius ? "Taip" : "Ne") + "";
    	let t8;
    	let t9;
    	let td4;
    	let t10_value = ctx.user.sukurimo_data.split("T")[0] + "";
    	let t10;
    	let t11;
    	let td5;
    	let button;
    	let i;
    	let t12;
    	let dispose;

    	function click_handler_2(...args) {
    		return ctx.click_handler_2(ctx, ...args);
    	}

    	function click_handler_3(...args) {
    		return ctx.click_handler_3(ctx, ...args);
    	}

    	function click_handler_4(...args) {
    		return ctx.click_handler_4(ctx, ...args);
    	}

    	const block = {
    		c: function create() {
    			tr = element("tr");
    			td0 = element("td");
    			t0 = text(t0_value);
    			t1 = space();
    			td1 = element("td");
    			t2 = text(t2_value);
    			t3 = space();
    			t4 = text(t4_value);
    			t5 = space();
    			td2 = element("td");
    			t6 = text(t6_value);
    			t7 = space();
    			td3 = element("td");
    			t8 = text(t8_value);
    			t9 = space();
    			td4 = element("td");
    			t10 = text(t10_value);
    			t11 = space();
    			td5 = element("td");
    			button = element("button");
    			i = element("i");
    			t12 = space();
    			add_location(td0, file$3, 153, 28, 5174);
    			attr_dev(td1, "class", "cursor-pointer");
    			add_location(td1, file$3, 154, 28, 5223);
    			attr_dev(td2, "class", "cursor-pointer");
    			add_location(td2, file$3, 155, 28, 5380);
    			add_location(td3, file$3, 156, 28, 5523);
    			add_location(td4, file$3, 157, 28, 5605);
    			attr_dev(i, "class", "fa fa-times");
    			add_location(i, file$3, 160, 36, 5869);
    			attr_dev(button, "type", "button");
    			attr_dev(button, "title", "");
    			attr_dev(button, "class", "cursor-pointer btn btn-danger btn-simple btn-link");
    			add_location(button, file$3, 159, 32, 5716);
    			add_location(td5, file$3, 158, 28, 5679);
    			add_location(tr, file$3, 152, 24, 5141);

    			dispose = [
    				listen_dev(td1, "click", click_handler_2, false, false, false),
    				listen_dev(td2, "click", click_handler_3, false, false, false),
    				listen_dev(button, "click", click_handler_4, false, false, false)
    			];
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, tr, anchor);
    			append_dev(tr, td0);
    			append_dev(td0, t0);
    			append_dev(tr, t1);
    			append_dev(tr, td1);
    			append_dev(td1, t2);
    			append_dev(td1, t3);
    			append_dev(td1, t4);
    			append_dev(tr, t5);
    			append_dev(tr, td2);
    			append_dev(td2, t6);
    			append_dev(tr, t7);
    			append_dev(tr, td3);
    			append_dev(td3, t8);
    			append_dev(tr, t9);
    			append_dev(tr, td4);
    			append_dev(td4, t10);
    			append_dev(tr, t11);
    			append_dev(tr, td5);
    			append_dev(td5, button);
    			append_dev(button, i);
    			append_dev(tr, t12);
    		},
    		p: function update(changed, new_ctx) {
    			ctx = new_ctx;
    			if (changed.localUsers && t0_value !== (t0_value = ctx.user.id + "")) set_data_dev(t0, t0_value);
    			if (changed.localUsers && t2_value !== (t2_value = ctx.user.vardas + "")) set_data_dev(t2, t2_value);
    			if (changed.localUsers && t4_value !== (t4_value = ctx.user.pavarde + "")) set_data_dev(t4, t4_value);
    			if (changed.localUsers && t6_value !== (t6_value = ctx.user.el_pastas + "")) set_data_dev(t6, t6_value);
    			if (changed.localUsers && t8_value !== (t8_value = (ctx.user.ar_administratorius ? "Taip" : "Ne") + "")) set_data_dev(t8, t8_value);
    			if (changed.localUsers && t10_value !== (t10_value = ctx.user.sukurimo_data.split("T")[0] + "")) set_data_dev(t10, t10_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(tr);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1.name,
    		type: "each",
    		source: "(152:20) {#each localUsers.data as user}",
    		ctx
    	});

    	return block;
    }

    // (193:20) {#each localUsersDeleted.data as user}
    function create_each_block$1(ctx) {
    	let tr;
    	let td0;
    	let t0_value = ctx.user.id + "";
    	let t0;
    	let t1;
    	let td1;
    	let t2_value = ctx.user.vardas + "";
    	let t2;
    	let t3;
    	let t4_value = ctx.user.pavarde + "";
    	let t4;
    	let t5;
    	let td2;
    	let t6_value = ctx.user.el_pastas + "";
    	let t6;
    	let t7;
    	let td3;
    	let t8_value = (ctx.user.ar_administratorius ? "Taip" : "Ne") + "";
    	let t8;
    	let t9;
    	let td4;
    	let t10_value = ctx.user.istrynimo_data.split("T")[0] + "";
    	let t10;
    	let t11;
    	let td5;
    	let button;
    	let i;
    	let t12;
    	let dispose;

    	function click_handler_5(...args) {
    		return ctx.click_handler_5(ctx, ...args);
    	}

    	const block = {
    		c: function create() {
    			tr = element("tr");
    			td0 = element("td");
    			t0 = text(t0_value);
    			t1 = space();
    			td1 = element("td");
    			t2 = text(t2_value);
    			t3 = space();
    			t4 = text(t4_value);
    			t5 = space();
    			td2 = element("td");
    			t6 = text(t6_value);
    			t7 = space();
    			td3 = element("td");
    			t8 = text(t8_value);
    			t9 = space();
    			td4 = element("td");
    			t10 = text(t10_value);
    			t11 = space();
    			td5 = element("td");
    			button = element("button");
    			i = element("i");
    			t12 = space();
    			add_location(td0, file$3, 194, 28, 7036);
    			add_location(td1, file$3, 195, 28, 7085);
    			add_location(td2, file$3, 196, 28, 7155);
    			add_location(td3, file$3, 197, 28, 7211);
    			add_location(td4, file$3, 198, 28, 7293);
    			attr_dev(i, "class", "fa fa-times");
    			add_location(i, file$3, 201, 36, 7562);
    			attr_dev(button, "type", "button");
    			attr_dev(button, "title", "");
    			attr_dev(button, "class", "cursor-pointer btn btn-danger btn-simple btn-link");
    			add_location(button, file$3, 200, 32, 7405);
    			add_location(td5, file$3, 199, 28, 7368);
    			add_location(tr, file$3, 193, 24, 7003);
    			dispose = listen_dev(button, "click", click_handler_5, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, tr, anchor);
    			append_dev(tr, td0);
    			append_dev(td0, t0);
    			append_dev(tr, t1);
    			append_dev(tr, td1);
    			append_dev(td1, t2);
    			append_dev(td1, t3);
    			append_dev(td1, t4);
    			append_dev(tr, t5);
    			append_dev(tr, td2);
    			append_dev(td2, t6);
    			append_dev(tr, t7);
    			append_dev(tr, td3);
    			append_dev(td3, t8);
    			append_dev(tr, t9);
    			append_dev(tr, td4);
    			append_dev(td4, t10);
    			append_dev(tr, t11);
    			append_dev(tr, td5);
    			append_dev(td5, button);
    			append_dev(button, i);
    			append_dev(tr, t12);
    		},
    		p: function update(changed, new_ctx) {
    			ctx = new_ctx;
    			if (changed.localUsersDeleted && t0_value !== (t0_value = ctx.user.id + "")) set_data_dev(t0, t0_value);
    			if (changed.localUsersDeleted && t2_value !== (t2_value = ctx.user.vardas + "")) set_data_dev(t2, t2_value);
    			if (changed.localUsersDeleted && t4_value !== (t4_value = ctx.user.pavarde + "")) set_data_dev(t4, t4_value);
    			if (changed.localUsersDeleted && t6_value !== (t6_value = ctx.user.el_pastas + "")) set_data_dev(t6, t6_value);
    			if (changed.localUsersDeleted && t8_value !== (t8_value = (ctx.user.ar_administratorius ? "Taip" : "Ne") + "")) set_data_dev(t8, t8_value);
    			if (changed.localUsersDeleted && t10_value !== (t10_value = ctx.user.istrynimo_data.split("T")[0] + "")) set_data_dev(t10, t10_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(tr);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(193:20) {#each localUsersDeleted.data as user}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let t0;
    	let t1;
    	let if_block2_anchor;
    	let current;
    	let if_block0 = ctx.showCreateModal && create_if_block_2(ctx);
    	let if_block1 = ctx.showInfoModal && create_if_block_1(ctx);

    	function select_block_type_1(changed, ctx) {
    		if (ctx.isAdmin) return create_if_block$1;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type_1(null, ctx);
    	let if_block2 = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if_block2.c();
    			if_block2_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert_dev(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, t1, anchor);
    			if_block2.m(target, anchor);
    			insert_dev(target, if_block2_anchor, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			if (ctx.showCreateModal) {
    				if (if_block0) {
    					if_block0.p(changed, ctx);
    					transition_in(if_block0, 1);
    				} else {
    					if_block0 = create_if_block_2(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (ctx.showInfoModal) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    					transition_in(if_block1, 1);
    				} else {
    					if_block1 = create_if_block_1(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}

    			if_block2.p(changed, ctx);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(if_block1);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block0);
    			transition_out(if_block1);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach_dev(t0);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(t1);
    			if_block2.d(detaching);
    			if (detaching) detach_dev(if_block2_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let $users;
    	let $usersDeleted;
    	validate_store(usersStore, "users");
    	component_subscribe($$self, usersStore, $$value => $$invalidate("$users", $users = $$value));
    	validate_store(usersStore$1, "usersDeleted");
    	component_subscribe($$self, usersStore$1, $$value => $$invalidate("$usersDeleted", $usersDeleted = $$value));
    	let showCreateModal = false;
    	let showInfoModal = false;
    	let selectedUser = null;
    	let isAdmin = localStorage.getItem("is_admin") === "true";
    	let loadingCreate = false;
    	let localUsers = $users;
    	let localUsersDeleted = $usersDeleted;
    	let error = null;

    	onMount(() => {
    		if (!localUsers.isSet && isAdmin) {
    			GET("/api/users").then(res => {
    				if (res.status) {
    					usersStore.set(res.data);
    					$$invalidate("localUsers", localUsers.data = res.data, localUsers);
    				}
    			});
    		}

    		if (!localUsersDeleted.isSet && isAdmin) {
    			GET("/api/users/deleted").then(res => {
    				if (res.status) {
    					usersStore$1.set(res.data);
    					$$invalidate("localUsersDeleted", localUsersDeleted.data = res.data, localUsersDeleted);
    				}
    			});
    		}
    	});

    	let newUser = { vardas: "", pavarde: "", "el_pastas": "" };

    	function create() {
    		$$invalidate("loadingCreate", loadingCreate = true);

    		POST("/api/users", newUser).then(res => {
    			$$invalidate("loadingCreate", loadingCreate = false);

    			if (res.status) {
    				usersStore.add(res.data);
    				$$invalidate("localUsers", localUsers = $users);
    				closeCreateModal();
    				return;
    			}

    			if (res.data.status === 422) $$invalidate("error", error = "Validacijos klaida. Patikrinkite įvestus duomenis."); else $$invalidate("error", error = "Įvyko klaida. Mėginkite dar kartą.");
    		});
    	}

    	function closeCreateModal() {
    		$$invalidate("showCreateModal", showCreateModal = false);
    		$$invalidate("newUser", newUser.vardas = "", newUser);
    		$$invalidate("newUser", newUser.pavarde = "", newUser);
    		$$invalidate("newUser", newUser.el_pastas = "", newUser);
    		$$invalidate("error", error = null);
    	}

    	function del(user) {
    		let result = confirm("Ar tikrai norite archyvuoti?");

    		if (result) {
    			PATCH("/api/users/" + user.id).then(res => {
    				if (res.status) {
    					usersStore.delete(user.id);
    					user.istrynimo_data = new Date().toISOString();
    					usersStore$1.add(user);
    					$$invalidate("localUsers", localUsers = $users);
    					$$invalidate("localUsersDeleted", localUsersDeleted = $usersDeleted);
    				}
    			});
    		}
    	}

    	function antiDel(user) {
    		let result = confirm("Ar tikrai norite grąžinti vartotoją?");

    		if (result) {
    			PATCH("/api/users/" + user.id).then(res => {
    				if (res.status) {
    					usersStore$1.delete(user.id);
    					usersStore.add(user);
    					$$invalidate("localUsers", localUsers = $users);
    					$$invalidate("localUsersDeleted", localUsersDeleted = $usersDeleted);
    				}
    			});
    		}
    	}

    	function input0_value_binding(value) {
    		newUser.vardas = value;
    		$$invalidate("newUser", newUser);
    	}

    	function input1_value_binding(value_1) {
    		newUser.pavarde = value_1;
    		$$invalidate("newUser", newUser);
    	}

    	function input2_value_binding(value_2) {
    		newUser.el_pastas = value_2;
    		$$invalidate("newUser", newUser);
    	}

    	const click_handler = () => $$invalidate("error", error = null);

    	const cancel_handler = () => {
    		$$invalidate("showInfoModal", showInfoModal = false);
    	};

    	const click_handler_1 = () => {
    		$$invalidate("showCreateModal", showCreateModal = true);
    	};

    	const click_handler_2 = ({ user }) => {
    		$$invalidate("showInfoModal", showInfoModal = true);
    		$$invalidate("selectedUser", selectedUser = user);
    	};

    	const click_handler_3 = ({ user }) => {
    		$$invalidate("showInfoModal", showInfoModal = true);
    		$$invalidate("selectedUser", selectedUser = user);
    	};

    	const click_handler_4 = ({ user }) => del(user);
    	const click_handler_5 = ({ user }) => antiDel(user);

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("showCreateModal" in $$props) $$invalidate("showCreateModal", showCreateModal = $$props.showCreateModal);
    		if ("showInfoModal" in $$props) $$invalidate("showInfoModal", showInfoModal = $$props.showInfoModal);
    		if ("selectedUser" in $$props) $$invalidate("selectedUser", selectedUser = $$props.selectedUser);
    		if ("isAdmin" in $$props) $$invalidate("isAdmin", isAdmin = $$props.isAdmin);
    		if ("loadingCreate" in $$props) $$invalidate("loadingCreate", loadingCreate = $$props.loadingCreate);
    		if ("localUsers" in $$props) $$invalidate("localUsers", localUsers = $$props.localUsers);
    		if ("localUsersDeleted" in $$props) $$invalidate("localUsersDeleted", localUsersDeleted = $$props.localUsersDeleted);
    		if ("error" in $$props) $$invalidate("error", error = $$props.error);
    		if ("newUser" in $$props) $$invalidate("newUser", newUser = $$props.newUser);
    		if ("$users" in $$props) usersStore.set($users = $$props.$users);
    		if ("$usersDeleted" in $$props) usersStore$1.set($usersDeleted = $$props.$usersDeleted);
    		if ("validForm" in $$props) $$invalidate("validForm", validForm = $$props.validForm);
    	};

    	let validForm;

    	$$self.$$.update = (changed = { newUser: 1 }) => {
    		if (changed.newUser) {
    			 $$invalidate("validForm", validForm = isLongerThan(newUser.vardas, 3) && isLongerThan(newUser.pavarde, 3) && isValidEmail(newUser.el_pastas));
    		}
    	};

    	return {
    		showCreateModal,
    		showInfoModal,
    		selectedUser,
    		isAdmin,
    		loadingCreate,
    		localUsers,
    		localUsersDeleted,
    		error,
    		newUser,
    		create,
    		closeCreateModal,
    		del,
    		antiDel,
    		validForm,
    		input0_value_binding,
    		input1_value_binding,
    		input2_value_binding,
    		click_handler,
    		cancel_handler,
    		click_handler_1,
    		click_handler_2,
    		click_handler_3,
    		click_handler_4,
    		click_handler_5
    	};
    }

    class Users extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Users",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src/pages/select-permission.svelte generated by Svelte v3.15.0 */
    const file$4 = "src/pages/select-permission.svelte";

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.permission = list[i];
    	child_ctx.i = i;
    	return child_ctx;
    }

    // (29:8) {#each rolePermissions as permission, i (permission.id)}
    function create_each_block$2(key_1, ctx) {
    	let tr;
    	let td0;
    	let div;
    	let label;
    	let input;
    	let input_checked_value;
    	let t0;
    	let span;
    	let t1;
    	let td1;
    	let t2_value = ctx.permission.pavadinimas + "";
    	let t2;
    	let t3;
    	let dispose;

    	function change_handler(...args) {
    		return ctx.change_handler(ctx, ...args);
    	}

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			tr = element("tr");
    			td0 = element("td");
    			div = element("div");
    			label = element("label");
    			input = element("input");
    			t0 = space();
    			span = element("span");
    			t1 = space();
    			td1 = element("td");
    			t2 = text(t2_value);
    			t3 = space();
    			attr_dev(input, "class", "form-check-input");
    			attr_dev(input, "type", "checkbox");
    			input.value = "";
    			input.checked = input_checked_value = ctx.permission.roles_id == ctx.role.id;
    			add_location(input, file$4, 33, 28, 1018);
    			attr_dev(span, "class", "form-check-sign");
    			add_location(span, file$4, 34, 28, 1175);
    			attr_dev(label, "class", "form-check-label");
    			add_location(label, file$4, 32, 24, 957);
    			attr_dev(div, "class", "form-check");
    			add_location(div, file$4, 31, 20, 908);
    			add_location(td0, file$4, 30, 16, 883);
    			add_location(td1, file$4, 38, 16, 1311);
    			add_location(tr, file$4, 29, 12, 862);
    			dispose = listen_dev(input, "change", change_handler, false, false, false);
    			this.first = tr;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, tr, anchor);
    			append_dev(tr, td0);
    			append_dev(td0, div);
    			append_dev(div, label);
    			append_dev(label, input);
    			append_dev(label, t0);
    			append_dev(label, span);
    			append_dev(tr, t1);
    			append_dev(tr, td1);
    			append_dev(td1, t2);
    			append_dev(tr, t3);
    		},
    		p: function update(changed, new_ctx) {
    			ctx = new_ctx;

    			if ((changed.rolePermissions || changed.role) && input_checked_value !== (input_checked_value = ctx.permission.roles_id == ctx.role.id)) {
    				prop_dev(input, "checked", input_checked_value);
    			}

    			if (changed.rolePermissions && t2_value !== (t2_value = ctx.permission.pavadinimas + "")) set_data_dev(t2, t2_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(tr);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$2.name,
    		type: "each",
    		source: "(29:8) {#each rolePermissions as permission, i (permission.id)}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$4(ctx) {
    	let div;
    	let table;
    	let tbody;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let each_value = ctx.rolePermissions;
    	const get_key = ctx => ctx.permission.id;

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$2(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$2(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			table = element("table");
    			tbody = element("tbody");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			add_location(tbody, file$4, 27, 8, 777);
    			attr_dev(table, "class", "table");
    			add_location(table, file$4, 26, 4, 747);
    			attr_dev(div, "class", "table-full-width");
    			add_location(div, file$4, 25, 0, 712);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, table);
    			append_dev(table, tbody);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(tbody, null);
    			}
    		},
    		p: function update(changed, ctx) {
    			const each_value = ctx.rolePermissions;
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, tbody, destroy_block, create_each_block$2, null, get_each_context$2);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let { role } = $$props;
    	let rolePermissions = [];

    	onMount(() => {
    		GET("/api/roles/" + role.id + "/permissions").then(res => {
    			if (res.status) $$invalidate("rolePermissions", rolePermissions = res.data);
    		});
    	});

    	function toggle(index) {
    		PATCH("/api/roles/" + role.id + "/permissions/" + rolePermissions[index].id, {}).then(res => {
    			let p = { ...rolePermissions[index] };
    			if (p.roles_id === role.id) $$invalidate("rolePermissions", rolePermissions[index].roles_id = null, rolePermissions); else $$invalidate("rolePermissions", rolePermissions[index].roles_id = role.id, rolePermissions);
    		});
    	}

    	const writable_props = ["role"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Select_permission> was created with unknown prop '${key}'`);
    	});

    	const change_handler = ({ i }) => toggle(i);

    	$$self.$set = $$props => {
    		if ("role" in $$props) $$invalidate("role", role = $$props.role);
    	};

    	$$self.$capture_state = () => {
    		return { role, rolePermissions };
    	};

    	$$self.$inject_state = $$props => {
    		if ("role" in $$props) $$invalidate("role", role = $$props.role);
    		if ("rolePermissions" in $$props) $$invalidate("rolePermissions", rolePermissions = $$props.rolePermissions);
    	};

    	return {
    		role,
    		rolePermissions,
    		toggle,
    		change_handler
    	};
    }

    class Select_permission extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { role: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Select_permission",
    			options,
    			id: create_fragment$4.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || ({});

    		if (ctx.role === undefined && !("role" in props)) {
    			console.warn("<Select_permission> was created without expected prop 'role'");
    		}
    	}

    	get role() {
    		throw new Error("<Select_permission>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set role(value) {
    		throw new Error("<Select_permission>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/pages/roles.svelte generated by Svelte v3.15.0 */
    const file$5 = "src/pages/roles.svelte";

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.role = list[i];
    	return child_ctx;
    }

    // (64:0) {#if showCreateModal}
    function create_if_block_1$1(ctx) {
    	let current;

    	const modal = new Modal({
    			props: {
    				title: "Pridėti naują rolę",
    				$$slots: { default: [create_default_slot_1$1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	modal.$on("cancel", ctx.cancel_handler);

    	const block = {
    		c: function create() {
    			create_component(modal.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(modal, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const modal_changes = {};

    			if (changed.$$scope || changed.error || changed.roleName || changed.loadingCreate) {
    				modal_changes.$$scope = { changed, ctx };
    			}

    			modal.$set(modal_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(modal.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(modal.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(modal, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$1.name,
    		type: "if",
    		source: "(64:0) {#if showCreateModal}",
    		ctx
    	});

    	return block;
    }

    // (71:12) {:else}
    function create_else_block$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Sukurti");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(71:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (69:12) {#if loadingCreate}
    function create_if_block_3$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Kraunama...");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_3$1.name,
    		type: "if",
    		source: "(69:12) {#if loadingCreate}",
    		ctx
    	});

    	return block;
    }

    // (76:8) {#if error != null}
    function create_if_block_2$1(ctx) {
    	let div;
    	let span;
    	let t1;
    	let strong;
    	let t3;
    	let t4;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			span = element("span");
    			span.textContent = "×";
    			t1 = space();
    			strong = element("strong");
    			strong.textContent = "Klaida!";
    			t3 = space();
    			t4 = text(ctx.error);
    			attr_dev(span, "class", "closebtn");
    			add_location(span, file$5, 77, 16, 2235);
    			add_location(strong, file$5, 78, 16, 2319);
    			attr_dev(div, "class", "alert bg-danger mt-3 mb-0");
    			add_location(div, file$5, 76, 12, 2179);
    			dispose = listen_dev(span, "click", ctx.click_handler, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, span);
    			append_dev(div, t1);
    			append_dev(div, strong);
    			append_dev(div, t3);
    			append_dev(div, t4);
    		},
    		p: function update(changed, ctx) {
    			if (changed.error) set_data_dev(t4, ctx.error);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2$1.name,
    		type: "if",
    		source: "(76:8) {#if error != null}",
    		ctx
    	});

    	return block;
    }

    // (65:4) <Modal title="Pridėti naują rolę" on:cancel="{() => {showCreateModal = false}}">
    function create_default_slot_1$1(ctx) {
    	let updating_value;
    	let t0;
    	let button;
    	let button_disabled_value;
    	let t1;
    	let if_block1_anchor;
    	let current;
    	let dispose;

    	function input_value_binding(value) {
    		ctx.input_value_binding.call(null, value);
    	}

    	let input_props = { placeholder: "Pavadinimas", name: "name" };

    	if (ctx.roleName !== void 0) {
    		input_props.value = ctx.roleName;
    	}

    	const input = new Input({ props: input_props, $$inline: true });
    	binding_callbacks.push(() => bind(input, "value", input_value_binding));

    	function select_block_type(changed, ctx) {
    		if (ctx.loadingCreate) return create_if_block_3$1;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type(null, ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = ctx.error != null && create_if_block_2$1(ctx);

    	const block = {
    		c: function create() {
    			create_component(input.$$.fragment);
    			t0 = space();
    			button = element("button");
    			if_block0.c();
    			t1 = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			button.disabled = button_disabled_value = ctx.roleName.length < 4;
    			attr_dev(button, "class", "btn btn-warning btn-fill w-100");
    			add_location(button, file$5, 67, 8, 1899);
    			dispose = listen_dev(button, "click", ctx.create, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			mount_component(input, target, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, button, anchor);
    			if_block0.m(button, null);
    			insert_dev(target, t1, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, if_block1_anchor, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const input_changes = {};

    			if (!updating_value && changed.roleName) {
    				updating_value = true;
    				input_changes.value = ctx.roleName;
    				add_flush_callback(() => updating_value = false);
    			}

    			input.$set(input_changes);

    			if (current_block_type !== (current_block_type = select_block_type(changed, ctx))) {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(button, null);
    				}
    			}

    			if (!current || changed.roleName && button_disabled_value !== (button_disabled_value = ctx.roleName.length < 4)) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}

    			if (ctx.error != null) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    				} else {
    					if_block1 = create_if_block_2$1(ctx);
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(input.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(input.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(input, detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(button);
    			if_block0.d();
    			if (detaching) detach_dev(t1);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(if_block1_anchor);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1$1.name,
    		type: "slot",
    		source: "(65:4) <Modal title=\\\"Pridėti naują rolę\\\" on:cancel=\\\"{() => {showCreateModal = false}}\\\">",
    		ctx
    	});

    	return block;
    }

    // (85:0) {#if showInfoModal}
    function create_if_block$2(ctx) {
    	let current;

    	const modal = new Modal({
    			props: {
    				title: "Rolės teisės",
    				$$slots: { default: [create_default_slot$1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	modal.$on("cancel", ctx.cancel_handler_1);

    	const block = {
    		c: function create() {
    			create_component(modal.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(modal, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const modal_changes = {};

    			if (changed.$$scope || changed.activeRole) {
    				modal_changes.$$scope = { changed, ctx };
    			}

    			modal.$set(modal_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(modal.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(modal.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(modal, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(85:0) {#if showInfoModal}",
    		ctx
    	});

    	return block;
    }

    // (86:4) <Modal title="Rolės teisės" on:cancel="{() => {showInfoModal = false}}">
    function create_default_slot$1(ctx) {
    	let current;

    	const selectpermission = new Select_permission({
    			props: { role: ctx.activeRole },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(selectpermission.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(selectpermission, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const selectpermission_changes = {};
    			if (changed.activeRole) selectpermission_changes.role = ctx.activeRole;
    			selectpermission.$set(selectpermission_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(selectpermission.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(selectpermission.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(selectpermission, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$1.name,
    		type: "slot",
    		source: "(86:4) <Modal title=\\\"Rolės teisės\\\" on:cancel=\\\"{() => {showInfoModal = false}}\\\">",
    		ctx
    	});

    	return block;
    }

    // (108:16) {#each localRoles.data as role (role.id)}
    function create_each_block$3(key_1, ctx) {
    	let tr;
    	let td0;
    	let t0_value = ctx.role.id + "";
    	let t0;
    	let t1;
    	let td1;
    	let t2_value = ctx.role.pavadinimas + "";
    	let t2;
    	let t3;
    	let td2;
    	let button;
    	let i;
    	let t4;
    	let dispose;

    	function click_handler_2(...args) {
    		return ctx.click_handler_2(ctx, ...args);
    	}

    	function click_handler_3(...args) {
    		return ctx.click_handler_3(ctx, ...args);
    	}

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			tr = element("tr");
    			td0 = element("td");
    			t0 = text(t0_value);
    			t1 = space();
    			td1 = element("td");
    			t2 = text(t2_value);
    			t3 = space();
    			td2 = element("td");
    			button = element("button");
    			i = element("i");
    			t4 = space();
    			add_location(td0, file$5, 109, 24, 3368);
    			attr_dev(td1, "class", "cursor-pointer");
    			add_location(td1, file$5, 110, 24, 3413);
    			attr_dev(i, "class", "fa fa-times");
    			add_location(i, file$5, 113, 32, 3738);
    			attr_dev(button, "type", "button");
    			attr_dev(button, "title", "");
    			attr_dev(button, "class", "cursor-pointer btn btn-danger btn-simple btn-link");
    			add_location(button, file$5, 112, 28, 3586);
    			add_location(td2, file$5, 111, 24, 3553);
    			add_location(tr, file$5, 108, 20, 3339);

    			dispose = [
    				listen_dev(td1, "click", click_handler_2, false, false, false),
    				listen_dev(button, "click", click_handler_3, false, false, false)
    			];

    			this.first = tr;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, tr, anchor);
    			append_dev(tr, td0);
    			append_dev(td0, t0);
    			append_dev(tr, t1);
    			append_dev(tr, td1);
    			append_dev(td1, t2);
    			append_dev(tr, t3);
    			append_dev(tr, td2);
    			append_dev(td2, button);
    			append_dev(button, i);
    			append_dev(tr, t4);
    		},
    		p: function update(changed, new_ctx) {
    			ctx = new_ctx;
    			if (changed.localRoles && t0_value !== (t0_value = ctx.role.id + "")) set_data_dev(t0, t0_value);
    			if (changed.localRoles && t2_value !== (t2_value = ctx.role.pavadinimas + "")) set_data_dev(t2, t2_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(tr);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$3.name,
    		type: "each",
    		source: "(108:16) {#each localRoles.data as role (role.id)}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$5(ctx) {
    	let t0;
    	let t1;
    	let button;
    	let t3;
    	let div3;
    	let div2;
    	let div0;
    	let h4;
    	let t5;
    	let div1;
    	let table;
    	let thead;
    	let tr;
    	let th0;
    	let t7;
    	let th1;
    	let t9;
    	let th2;
    	let t11;
    	let tbody;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let current;
    	let dispose;
    	let if_block0 = ctx.showCreateModal && create_if_block_1$1(ctx);
    	let if_block1 = ctx.showInfoModal && create_if_block$2(ctx);
    	let each_value = ctx.localRoles.data;
    	const get_key = ctx => ctx.role.id;

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$3(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$3(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			button = element("button");
    			button.textContent = "Pridėti naują rolę";
    			t3 = space();
    			div3 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			h4 = element("h4");
    			h4.textContent = "Sistemoje esančios vartotojų rolės";
    			t5 = space();
    			div1 = element("div");
    			table = element("table");
    			thead = element("thead");
    			tr = element("tr");
    			th0 = element("th");
    			th0.textContent = "ID";
    			t7 = space();
    			th1 = element("th");
    			th1.textContent = "Pavadinimas";
    			t9 = space();
    			th2 = element("th");
    			th2.textContent = "Veiksmai";
    			t11 = space();
    			tbody = element("tbody");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(button, "type", "button");
    			attr_dev(button, "class", "btn btn-success btn-fill ml-3 mb-3");
    			add_location(button, file$5, 90, 0, 2572);
    			attr_dev(h4, "class", "card-title");
    			add_location(h4, file$5, 94, 12, 2829);
    			attr_dev(div0, "class", "card-header ");
    			add_location(div0, file$5, 93, 8, 2790);
    			add_location(th0, file$5, 100, 20, 3098);
    			add_location(th1, file$5, 101, 20, 3130);
    			add_location(th2, file$5, 102, 20, 3171);
    			add_location(tr, file$5, 99, 16, 3073);
    			add_location(thead, file$5, 98, 16, 3049);
    			add_location(tbody, file$5, 106, 16, 3253);
    			attr_dev(table, "class", "table table-hover table-striped");
    			add_location(table, file$5, 97, 12, 2985);
    			attr_dev(div1, "class", "card-body table-full-width table-responsive");
    			add_location(div1, file$5, 96, 8, 2915);
    			attr_dev(div2, "class", "card strpied-tabled-with-hover");
    			add_location(div2, file$5, 92, 4, 2737);
    			attr_dev(div3, "class", "col-md-12");
    			add_location(div3, file$5, 91, 0, 2709);
    			dispose = listen_dev(button, "click", ctx.click_handler_1, false, false, false);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert_dev(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, button, anchor);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div2);
    			append_dev(div2, div0);
    			append_dev(div0, h4);
    			append_dev(div2, t5);
    			append_dev(div2, div1);
    			append_dev(div1, table);
    			append_dev(table, thead);
    			append_dev(thead, tr);
    			append_dev(tr, th0);
    			append_dev(tr, t7);
    			append_dev(tr, th1);
    			append_dev(tr, t9);
    			append_dev(tr, th2);
    			append_dev(table, t11);
    			append_dev(table, tbody);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(tbody, null);
    			}

    			current = true;
    		},
    		p: function update(changed, ctx) {
    			if (ctx.showCreateModal) {
    				if (if_block0) {
    					if_block0.p(changed, ctx);
    					transition_in(if_block0, 1);
    				} else {
    					if_block0 = create_if_block_1$1(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (ctx.showInfoModal) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    					transition_in(if_block1, 1);
    				} else {
    					if_block1 = create_if_block$2(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}

    			const each_value = ctx.localRoles.data;
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, tbody, destroy_block, create_each_block$3, null, get_each_context$3);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(if_block1);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block0);
    			transition_out(if_block1);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach_dev(t0);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(button);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(div3);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}

    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let $roles;
    	validate_store(rolesStore, "roles");
    	component_subscribe($$self, rolesStore, $$value => $$invalidate("$roles", $roles = $$value));
    	let showCreateModal = false;
    	let showInfoModal = false;
    	let localRoles = $roles;
    	let roleName = "";
    	let loadingCreate = false;
    	let error = null;
    	let activeRole = null;

    	onMount(() => {
    		if (!localRoles.isSet) {
    			GET("/api/roles").then(res => {
    				if (res.status) {
    					rolesStore.set(res.data);
    					$$invalidate("localRoles", localRoles.data = res.data, localRoles);
    				}
    			});
    		}
    	});

    	function create() {
    		$$invalidate("loadingCreate", loadingCreate = true);

    		POST("/api/roles", { pavadinimas: roleName }).then(res => {
    			$$invalidate("loadingCreate", loadingCreate = false);

    			if (res.status) {
    				rolesStore.add(res.data);
    				$$invalidate("localRoles", localRoles = $roles);
    				closeCreateModal();
    				return;
    			}

    			if (res.data.status === 422) $$invalidate("error", error = "Validacijos klaida. Patikrinkite įvestus duomenis."); else $$invalidate("error", error = "Įvyko klaida. Mėginkite dar kartą.");
    		});
    	}

    	function closeCreateModal() {
    		$$invalidate("showCreateModal", showCreateModal = false);
    		$$invalidate("roleName", roleName = "");
    		$$invalidate("error", error = null);
    	}

    	function del(id) {
    		let result = confirm("Ar tikrai norite ištrinti?");

    		if (result) {
    			DELETE("/api/roles/" + id).then(res => {
    				rolesStore.delete(id);
    				$$invalidate("localRoles", localRoles = $roles);
    			});
    		}
    	}

    	function input_value_binding(value) {
    		roleName = value;
    		$$invalidate("roleName", roleName);
    	}

    	const click_handler = () => $$invalidate("error", error = null);

    	const cancel_handler = () => {
    		$$invalidate("showCreateModal", showCreateModal = false);
    	};

    	const cancel_handler_1 = () => {
    		$$invalidate("showInfoModal", showInfoModal = false);
    	};

    	const click_handler_1 = () => {
    		$$invalidate("showCreateModal", showCreateModal = true);
    	};

    	const click_handler_2 = ({ role }) => {
    		$$invalidate("showInfoModal", showInfoModal = true);
    		$$invalidate("activeRole", activeRole = role);
    	};

    	const click_handler_3 = ({ role }) => del(role.id);

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("showCreateModal" in $$props) $$invalidate("showCreateModal", showCreateModal = $$props.showCreateModal);
    		if ("showInfoModal" in $$props) $$invalidate("showInfoModal", showInfoModal = $$props.showInfoModal);
    		if ("localRoles" in $$props) $$invalidate("localRoles", localRoles = $$props.localRoles);
    		if ("roleName" in $$props) $$invalidate("roleName", roleName = $$props.roleName);
    		if ("loadingCreate" in $$props) $$invalidate("loadingCreate", loadingCreate = $$props.loadingCreate);
    		if ("error" in $$props) $$invalidate("error", error = $$props.error);
    		if ("activeRole" in $$props) $$invalidate("activeRole", activeRole = $$props.activeRole);
    		if ("$roles" in $$props) rolesStore.set($roles = $$props.$roles);
    	};

    	return {
    		showCreateModal,
    		showInfoModal,
    		localRoles,
    		roleName,
    		loadingCreate,
    		error,
    		activeRole,
    		create,
    		del,
    		input_value_binding,
    		click_handler,
    		cancel_handler,
    		cancel_handler_1,
    		click_handler_1,
    		click_handler_2,
    		click_handler_3
    	};
    }

    class Roles extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Roles",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    const permissions = writable({ isSet: false, data: [] });


    const permissionsStore = {
        subscribe: permissions.subscribe,
        set: u => {
            permissions.set({ isSet: true, data: u });
        },
        add: u => {
            permissions.update(item => {
                let permissionsCopy = [ ...item.data ];
                permissionsCopy = [ ...permissionsCopy, u ];
                return { isSet: item.isSet, data: permissionsCopy };
            });
        },
        delete: r => {
            permissions.update(item => {
                let permissionsCopy = [ ...item.data ];
                permissionsCopy = permissionsCopy.filter(i => i.id !== r);
                return { isSet: item.isSet, data: permissionsCopy };
            });
        }
    };

    /* src/pages/permissions.svelte generated by Svelte v3.15.0 */
    const file$6 = "src/pages/permissions.svelte";

    function get_each_context$4(ctx, list, i) {
    	const child_ctx = Object.create(ctx);
    	child_ctx.permission = list[i];
    	return child_ctx;
    }

    // (65:0) {#if showCreateModal}
    function create_if_block$3(ctx) {
    	let current;

    	const modal = new Modal({
    			props: {
    				title: "Pridėti naują rolę",
    				$$slots: { default: [create_default_slot$2] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	modal.$on("cancel", ctx.cancel_handler);

    	const block = {
    		c: function create() {
    			create_component(modal.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(modal, target, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const modal_changes = {};

    			if (changed.$$scope || changed.error || changed.name || changed.sysName || changed.loadingCreate) {
    				modal_changes.$$scope = { changed, ctx };
    			}

    			modal.$set(modal_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(modal.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(modal.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(modal, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(65:0) {#if showCreateModal}",
    		ctx
    	});

    	return block;
    }

    // (73:12) {:else}
    function create_else_block$2(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Sukurti");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$2.name,
    		type: "else",
    		source: "(73:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (71:12) {#if loadingCreate}
    function create_if_block_2$2(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Kraunama...");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2$2.name,
    		type: "if",
    		source: "(71:12) {#if loadingCreate}",
    		ctx
    	});

    	return block;
    }

    // (78:8) {#if error != null}
    function create_if_block_1$2(ctx) {
    	let div;
    	let span;
    	let t1;
    	let strong;
    	let t3;
    	let t4;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			span = element("span");
    			span.textContent = "×";
    			t1 = space();
    			strong = element("strong");
    			strong.textContent = "Klaida!";
    			t3 = space();
    			t4 = text(ctx.error);
    			attr_dev(span, "class", "closebtn");
    			add_location(span, file$6, 79, 16, 2400);
    			add_location(strong, file$6, 80, 16, 2484);
    			attr_dev(div, "class", "alert bg-danger mt-3 mb-0");
    			add_location(div, file$6, 78, 12, 2344);
    			dispose = listen_dev(span, "click", ctx.click_handler, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, span);
    			append_dev(div, t1);
    			append_dev(div, strong);
    			append_dev(div, t3);
    			append_dev(div, t4);
    		},
    		p: function update(changed, ctx) {
    			if (changed.error) set_data_dev(t4, ctx.error);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$2.name,
    		type: "if",
    		source: "(78:8) {#if error != null}",
    		ctx
    	});

    	return block;
    }

    // (66:4) <Modal title="Pridėti naują rolę" on:cancel="{() => {showCreateModal = false}}">
    function create_default_slot$2(ctx) {
    	let updating_value;
    	let t0;
    	let updating_value_1;
    	let t1;
    	let button;
    	let button_disabled_value;
    	let t2;
    	let if_block1_anchor;
    	let current;
    	let dispose;

    	function input0_value_binding(value) {
    		ctx.input0_value_binding.call(null, value);
    	}

    	let input0_props = { placeholder: "Pavadinimas", name: "name" };

    	if (ctx.name !== void 0) {
    		input0_props.value = ctx.name;
    	}

    	const input0 = new Input({ props: input0_props, $$inline: true });
    	binding_callbacks.push(() => bind(input0, "value", input0_value_binding));

    	function input1_value_binding(value_1) {
    		ctx.input1_value_binding.call(null, value_1);
    	}

    	let input1_props = {
    		placeholder: "Sisteminis pavadinimas",
    		name: "name"
    	};

    	if (ctx.sysName !== void 0) {
    		input1_props.value = ctx.sysName;
    	}

    	const input1 = new Input({ props: input1_props, $$inline: true });
    	binding_callbacks.push(() => bind(input1, "value", input1_value_binding));

    	function select_block_type(changed, ctx) {
    		if (ctx.loadingCreate) return create_if_block_2$2;
    		return create_else_block$2;
    	}

    	let current_block_type = select_block_type(null, ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = ctx.error != null && create_if_block_1$2(ctx);

    	const block = {
    		c: function create() {
    			create_component(input0.$$.fragment);
    			t0 = space();
    			create_component(input1.$$.fragment);
    			t1 = space();
    			button = element("button");
    			if_block0.c();
    			t2 = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			button.disabled = button_disabled_value = ctx.name.length < 4 && ctx.sysName.length < 4;
    			attr_dev(button, "class", "btn btn-warning btn-fill w-100");
    			add_location(button, file$6, 69, 8, 2046);
    			dispose = listen_dev(button, "click", ctx.create, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			mount_component(input0, target, anchor);
    			insert_dev(target, t0, anchor);
    			mount_component(input1, target, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, button, anchor);
    			if_block0.m(button, null);
    			insert_dev(target, t2, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, if_block1_anchor, anchor);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			const input0_changes = {};

    			if (!updating_value && changed.name) {
    				updating_value = true;
    				input0_changes.value = ctx.name;
    				add_flush_callback(() => updating_value = false);
    			}

    			input0.$set(input0_changes);
    			const input1_changes = {};

    			if (!updating_value_1 && changed.sysName) {
    				updating_value_1 = true;
    				input1_changes.value = ctx.sysName;
    				add_flush_callback(() => updating_value_1 = false);
    			}

    			input1.$set(input1_changes);

    			if (current_block_type !== (current_block_type = select_block_type(changed, ctx))) {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(button, null);
    				}
    			}

    			if (!current || (changed.name || changed.sysName) && button_disabled_value !== (button_disabled_value = ctx.name.length < 4 && ctx.sysName.length < 4)) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}

    			if (ctx.error != null) {
    				if (if_block1) {
    					if_block1.p(changed, ctx);
    				} else {
    					if_block1 = create_if_block_1$2(ctx);
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(input0.$$.fragment, local);
    			transition_in(input1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(input0.$$.fragment, local);
    			transition_out(input1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(input0, detaching);
    			if (detaching) detach_dev(t0);
    			destroy_component(input1, detaching);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(button);
    			if_block0.d();
    			if (detaching) detach_dev(t2);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(if_block1_anchor);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$2.name,
    		type: "slot",
    		source: "(66:4) <Modal title=\\\"Pridėti naują rolę\\\" on:cancel=\\\"{() => {showCreateModal = false}}\\\">",
    		ctx
    	});

    	return block;
    }

    // (105:16) {#each localPermissions.data as permission (permission.id)}
    function create_each_block$4(key_1, ctx) {
    	let tr;
    	let td0;
    	let t0_value = ctx.permission.id + "";
    	let t0;
    	let t1;
    	let td1;
    	let t2_value = ctx.permission.pavadinimas + "";
    	let t2;
    	let t3;
    	let td2;
    	let t4_value = ctx.permission.sis_pavadinimas + "";
    	let t4;
    	let t5;
    	let td3;
    	let button;
    	let i;
    	let t6;
    	let dispose;

    	function click_handler_2(...args) {
    		return ctx.click_handler_2(ctx, ...args);
    	}

    	const block = {
    		key: key_1,
    		first: null,
    		c: function create() {
    			tr = element("tr");
    			td0 = element("td");
    			t0 = text(t0_value);
    			t1 = space();
    			td1 = element("td");
    			t2 = text(t2_value);
    			t3 = space();
    			td2 = element("td");
    			t4 = text(t4_value);
    			t5 = space();
    			td3 = element("td");
    			button = element("button");
    			i = element("i");
    			t6 = space();
    			add_location(td0, file$6, 106, 24, 3436);
    			attr_dev(td1, "class", "cursor-pointer");
    			add_location(td1, file$6, 107, 24, 3487);
    			attr_dev(td2, "class", "cursor-pointer");
    			add_location(td2, file$6, 108, 24, 3570);
    			attr_dev(i, "class", "fa fa-times");
    			add_location(i, file$6, 111, 32, 3848);
    			attr_dev(button, "type", "button");
    			attr_dev(button, "title", "");
    			attr_dev(button, "class", "cursor-pointer btn btn-danger btn-simple btn-link");
    			add_location(button, file$6, 110, 28, 3690);
    			add_location(td3, file$6, 109, 24, 3657);
    			add_location(tr, file$6, 105, 20, 3407);
    			dispose = listen_dev(button, "click", click_handler_2, false, false, false);
    			this.first = tr;
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, tr, anchor);
    			append_dev(tr, td0);
    			append_dev(td0, t0);
    			append_dev(tr, t1);
    			append_dev(tr, td1);
    			append_dev(td1, t2);
    			append_dev(tr, t3);
    			append_dev(tr, td2);
    			append_dev(td2, t4);
    			append_dev(tr, t5);
    			append_dev(tr, td3);
    			append_dev(td3, button);
    			append_dev(button, i);
    			append_dev(tr, t6);
    		},
    		p: function update(changed, new_ctx) {
    			ctx = new_ctx;
    			if (changed.localPermissions && t0_value !== (t0_value = ctx.permission.id + "")) set_data_dev(t0, t0_value);
    			if (changed.localPermissions && t2_value !== (t2_value = ctx.permission.pavadinimas + "")) set_data_dev(t2, t2_value);
    			if (changed.localPermissions && t4_value !== (t4_value = ctx.permission.sis_pavadinimas + "")) set_data_dev(t4, t4_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(tr);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$4.name,
    		type: "each",
    		source: "(105:16) {#each localPermissions.data as permission (permission.id)}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let t0;
    	let button;
    	let t2;
    	let div3;
    	let div2;
    	let div0;
    	let h4;
    	let t4;
    	let div1;
    	let table;
    	let thead;
    	let tr;
    	let th0;
    	let t6;
    	let th1;
    	let t8;
    	let th2;
    	let t10;
    	let th3;
    	let t12;
    	let tbody;
    	let each_blocks = [];
    	let each_1_lookup = new Map();
    	let current;
    	let dispose;
    	let if_block = ctx.showCreateModal && create_if_block$3(ctx);
    	let each_value = ctx.localPermissions.data;
    	const get_key = ctx => ctx.permission.id;

    	for (let i = 0; i < each_value.length; i += 1) {
    		let child_ctx = get_each_context$4(ctx, each_value, i);
    		let key = get_key(child_ctx);
    		each_1_lookup.set(key, each_blocks[i] = create_each_block$4(key, child_ctx));
    	}

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			t0 = space();
    			button = element("button");
    			button.textContent = "Pridėti naują teisę";
    			t2 = space();
    			div3 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			h4 = element("h4");
    			h4.textContent = "Sistemoje esančios rolių teisės";
    			t4 = space();
    			div1 = element("div");
    			table = element("table");
    			thead = element("thead");
    			tr = element("tr");
    			th0 = element("th");
    			th0.textContent = "ID";
    			t6 = space();
    			th1 = element("th");
    			th1.textContent = "Pavadinimas";
    			t8 = space();
    			th2 = element("th");
    			th2.textContent = "Sisteminis pavadinimas";
    			t10 = space();
    			th3 = element("th");
    			th3.textContent = "Veiksmai";
    			t12 = space();
    			tbody = element("tbody");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(button, "type", "button");
    			attr_dev(button, "class", "btn btn-success btn-fill ml-3 mb-3");
    			add_location(button, file$6, 86, 0, 2572);
    			attr_dev(h4, "class", "card-title");
    			add_location(h4, file$6, 90, 12, 2830);
    			attr_dev(div0, "class", "card-header ");
    			add_location(div0, file$6, 89, 8, 2791);
    			add_location(th0, file$6, 96, 20, 3096);
    			add_location(th1, file$6, 97, 20, 3128);
    			add_location(th2, file$6, 98, 20, 3169);
    			add_location(th3, file$6, 99, 20, 3221);
    			add_location(tr, file$6, 95, 16, 3071);
    			add_location(thead, file$6, 94, 16, 3047);
    			add_location(tbody, file$6, 103, 16, 3303);
    			attr_dev(table, "class", "table table-hover table-striped");
    			add_location(table, file$6, 93, 12, 2983);
    			attr_dev(div1, "class", "card-body table-full-width table-responsive");
    			add_location(div1, file$6, 92, 8, 2913);
    			attr_dev(div2, "class", "card strpied-tabled-with-hover");
    			add_location(div2, file$6, 88, 4, 2738);
    			attr_dev(div3, "class", "col-md-12");
    			add_location(div3, file$6, 87, 0, 2710);
    			dispose = listen_dev(button, "click", ctx.click_handler_1, false, false, false);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, button, anchor);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div2);
    			append_dev(div2, div0);
    			append_dev(div0, h4);
    			append_dev(div2, t4);
    			append_dev(div2, div1);
    			append_dev(div1, table);
    			append_dev(table, thead);
    			append_dev(thead, tr);
    			append_dev(tr, th0);
    			append_dev(tr, t6);
    			append_dev(tr, th1);
    			append_dev(tr, t8);
    			append_dev(tr, th2);
    			append_dev(tr, t10);
    			append_dev(tr, th3);
    			append_dev(table, t12);
    			append_dev(table, tbody);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(tbody, null);
    			}

    			current = true;
    		},
    		p: function update(changed, ctx) {
    			if (ctx.showCreateModal) {
    				if (if_block) {
    					if_block.p(changed, ctx);
    					transition_in(if_block, 1);
    				} else {
    					if_block = create_if_block$3(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(t0.parentNode, t0);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}

    			const each_value = ctx.localPermissions.data;
    			each_blocks = update_keyed_each(each_blocks, changed, get_key, 1, ctx, each_value, each_1_lookup, tbody, destroy_block, create_each_block$4, null, get_each_context$4);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(button);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div3);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].d();
    			}

    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    let activeRole = null;

    function instance$6($$self, $$props, $$invalidate) {
    	let $permissions;
    	validate_store(permissionsStore, "permissions");
    	component_subscribe($$self, permissionsStore, $$value => $$invalidate("$permissions", $permissions = $$value));
    	let showCreateModal = false;
    	let localPermissions = $permissions;
    	let loadingCreate = false;
    	let error = null;

    	onMount(() => {
    		if (!localPermissions.isSet) {
    			GET("/api/permissions").then(res => {
    				if (res.status) {
    					permissionsStore.set(res.data);
    					$$invalidate("localPermissions", localPermissions.data = res.data, localPermissions);
    				}
    			});
    		}
    	});

    	let name = "";
    	let sysName = "";

    	function create() {
    		$$invalidate("loadingCreate", loadingCreate = true);

    		POST("/api/permissions", {
    			pavadinimas: name,
    			sis_pavadinimas: sysName
    		}).then(res => {
    			$$invalidate("loadingCreate", loadingCreate = false);

    			if (res.status) {
    				permissionsStore.add(res.data);
    				$$invalidate("localPermissions", localPermissions = $permissions);
    				closeCreateModal();
    				return;
    			}

    			if (res.data.status === 422) $$invalidate("error", error = "Validacijos klaida. Patikrinkite įvestus duomenis."); else $$invalidate("error", error = "Įvyko klaida. Mėginkite dar kartą.");
    		});
    	}

    	function closeCreateModal() {
    		$$invalidate("showCreateModal", showCreateModal = false);
    		$$invalidate("name", name = "");
    		$$invalidate("sysName", sysName = "");
    		$$invalidate("error", error = null);
    	}

    	function del(id) {
    		let result = confirm("Ar tikrai norite ištrinti?");

    		if (result) {
    			DELETE("/api/permissions/" + id).then(res => {
    				permissionsStore.delete(id);
    				$$invalidate("localPermissions", localPermissions = $permissions);
    			});
    		}
    	}

    	function input0_value_binding(value) {
    		name = value;
    		$$invalidate("name", name);
    	}

    	function input1_value_binding(value_1) {
    		sysName = value_1;
    		$$invalidate("sysName", sysName);
    	}

    	const click_handler = () => $$invalidate("error", error = null);

    	const cancel_handler = () => {
    		$$invalidate("showCreateModal", showCreateModal = false);
    	};

    	const click_handler_1 = () => {
    		$$invalidate("showCreateModal", showCreateModal = true);
    	};

    	const click_handler_2 = ({ permission }) => del(permission.id);

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("showCreateModal" in $$props) $$invalidate("showCreateModal", showCreateModal = $$props.showCreateModal);
    		if ("localPermissions" in $$props) $$invalidate("localPermissions", localPermissions = $$props.localPermissions);
    		if ("loadingCreate" in $$props) $$invalidate("loadingCreate", loadingCreate = $$props.loadingCreate);
    		if ("error" in $$props) $$invalidate("error", error = $$props.error);
    		if ("activeRole" in $$props) activeRole = $$props.activeRole;
    		if ("name" in $$props) $$invalidate("name", name = $$props.name);
    		if ("sysName" in $$props) $$invalidate("sysName", sysName = $$props.sysName);
    		if ("$permissions" in $$props) permissionsStore.set($permissions = $$props.$permissions);
    	};

    	return {
    		showCreateModal,
    		localPermissions,
    		loadingCreate,
    		error,
    		name,
    		sysName,
    		create,
    		del,
    		input0_value_binding,
    		input1_value_binding,
    		click_handler,
    		cancel_handler,
    		click_handler_1,
    		click_handler_2
    	};
    }

    class Permissions extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Permissions",
    			options,
    			id: create_fragment$6.name
    		});
    	}
    }

    /* src/App.svelte generated by Svelte v3.15.0 */
    const file$7 = "src/App.svelte";

    // (94:58) 
    function create_if_block_2$3(ctx) {
    	let current;
    	const permissions = new Permissions({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(permissions.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(permissions, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(permissions.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(permissions.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(permissions, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2$3.name,
    		type: "if",
    		source: "(94:58) ",
    		ctx
    	});

    	return block;
    }

    // (92:52) 
    function create_if_block_1$3(ctx) {
    	let current;
    	const roles = new Roles({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(roles.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(roles, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(roles.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(roles.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(roles, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$3.name,
    		type: "if",
    		source: "(92:52) ",
    		ctx
    	});

    	return block;
    }

    // (90:20) {#if activePage == "users"}
    function create_if_block$4(ctx) {
    	let current;
    	const users = new Users({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(users.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(users, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(users.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(users.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(users, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$4.name,
    		type: "if",
    		source: "(90:20) {#if activePage == \\\"users\\\"}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$7(ctx) {
    	let div10;
    	let div2;
    	let div1;
    	let div0;
    	let a0;
    	let t1;
    	let ul0;
    	let li0;
    	let a1;
    	let i0;
    	let t2;
    	let p0;
    	let t4;
    	let li1;
    	let a2;
    	let i1;
    	let t5;
    	let p1;
    	let t7;
    	let li2;
    	let a3;
    	let i2;
    	let t8;
    	let p2;
    	let t10;
    	let li3;
    	let a4;
    	let i3;
    	let t11;
    	let p3;
    	let t13;
    	let li4;
    	let a5;
    	let i4;
    	let t14;
    	let p4;
    	let t16;
    	let div9;
    	let nav0;
    	let div4;
    	let a6;
    	let t18;
    	let button;
    	let span0;
    	let t19;
    	let span1;
    	let t20;
    	let span2;
    	let t21;
    	let div3;
    	let ul1;
    	let li5;
    	let a7;
    	let span3;
    	let t23;
    	let li6;
    	let a8;
    	let span4;
    	let t25;
    	let li7;
    	let a9;
    	let span5;
    	let t27;
    	let li8;
    	let a10;
    	let span6;
    	let t29;
    	let div7;
    	let div6;
    	let div5;
    	let current_block_type_index;
    	let if_block;
    	let t30;
    	let footer;
    	let div8;
    	let nav1;
    	let p5;
    	let current;
    	let dispose;
    	const if_block_creators = [create_if_block$4, create_if_block_1$3, create_if_block_2$3];
    	const if_blocks = [];

    	function select_block_type(changed, ctx) {
    		if (ctx.activePage == "users") return 0;
    		if (ctx.activePage == "roles") return 1;
    		if (ctx.activePage == "permissions") return 2;
    		return -1;
    	}

    	if (~(current_block_type_index = select_block_type(null, ctx))) {
    		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	}

    	const block = {
    		c: function create() {
    			div10 = element("div");
    			div2 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			a0 = element("a");
    			a0.textContent = "ZOO";
    			t1 = space();
    			ul0 = element("ul");
    			li0 = element("li");
    			a1 = element("a");
    			i0 = element("i");
    			t2 = space();
    			p0 = element("p");
    			p0.textContent = "Vartotojai";
    			t4 = space();
    			li1 = element("li");
    			a2 = element("a");
    			i1 = element("i");
    			t5 = space();
    			p1 = element("p");
    			p1.textContent = "Gyvūnai";
    			t7 = space();
    			li2 = element("li");
    			a3 = element("a");
    			i2 = element("i");
    			t8 = space();
    			p2 = element("p");
    			p2.textContent = "Pašarai";
    			t10 = space();
    			li3 = element("li");
    			a4 = element("a");
    			i3 = element("i");
    			t11 = space();
    			p3 = element("p");
    			p3.textContent = "Bilietai";
    			t13 = space();
    			li4 = element("li");
    			a5 = element("a");
    			i4 = element("i");
    			t14 = space();
    			p4 = element("p");
    			p4.textContent = "Marketingas";
    			t16 = space();
    			div9 = element("div");
    			nav0 = element("nav");
    			div4 = element("div");
    			a6 = element("a");
    			a6.textContent = "Vartotojai";
    			t18 = space();
    			button = element("button");
    			span0 = element("span");
    			t19 = space();
    			span1 = element("span");
    			t20 = space();
    			span2 = element("span");
    			t21 = space();
    			div3 = element("div");
    			ul1 = element("ul");
    			li5 = element("li");
    			a7 = element("a");
    			span3 = element("span");
    			span3.textContent = "Vartotojai";
    			t23 = space();
    			li6 = element("li");
    			a8 = element("a");
    			span4 = element("span");
    			span4.textContent = "Rolės";
    			t25 = space();
    			li7 = element("li");
    			a9 = element("a");
    			span5 = element("span");
    			span5.textContent = "Teisės";
    			t27 = space();
    			li8 = element("li");
    			a10 = element("a");
    			span6 = element("span");
    			span6.textContent = "Atsijungti";
    			t29 = space();
    			div7 = element("div");
    			div6 = element("div");
    			div5 = element("div");
    			if (if_block) if_block.c();
    			t30 = space();
    			footer = element("footer");
    			div8 = element("div");
    			nav1 = element("nav");
    			p5 = element("p");
    			p5.textContent = "© Durukuliai";
    			attr_dev(a0, "href", "http://www.creative-tim.com");
    			attr_dev(a0, "class", "simple-text");
    			add_location(a0, file$7, 12, 4, 292);
    			attr_dev(div0, "class", "logo");
    			add_location(div0, file$7, 11, 3, 269);
    			attr_dev(i0, "class", "nc-icon nc-circle-09");
    			add_location(i0, file$7, 19, 6, 478);
    			add_location(p0, file$7, 20, 6, 521);
    			attr_dev(a1, "class", "nav-link");
    			attr_dev(a1, "href", "/users");
    			add_location(a1, file$7, 18, 5, 437);
    			attr_dev(li0, "class", "nav-item active");
    			add_location(li0, file$7, 17, 4, 403);
    			attr_dev(i1, "class", "nc-icon nc-chart-pie-35");
    			add_location(i1, file$7, 25, 6, 616);
    			add_location(p1, file$7, 26, 6, 662);
    			attr_dev(a2, "class", "nav-link");
    			attr_dev(a2, "href", "/animals");
    			add_location(a2, file$7, 24, 5, 573);
    			add_location(li1, file$7, 23, 4, 563);
    			attr_dev(i2, "class", "nc-icon nc-chart-pie-35");
    			add_location(i2, file$7, 31, 6, 751);
    			add_location(p2, file$7, 32, 6, 797);
    			attr_dev(a3, "class", "nav-link");
    			attr_dev(a3, "href", "/feed");
    			add_location(a3, file$7, 30, 5, 711);
    			add_location(li2, file$7, 29, 4, 701);
    			attr_dev(i3, "class", "nc-icon nc-chart-pie-35");
    			add_location(i3, file$7, 37, 6, 889);
    			add_location(p3, file$7, 38, 6, 935);
    			attr_dev(a4, "class", "nav-link");
    			attr_dev(a4, "href", "/tickets");
    			add_location(a4, file$7, 36, 5, 846);
    			add_location(li3, file$7, 35, 4, 836);
    			attr_dev(i4, "class", "nc-icon nc-chart-pie-35");
    			add_location(i4, file$7, 43, 6, 1030);
    			add_location(p4, file$7, 44, 6, 1076);
    			attr_dev(a5, "class", "nav-link");
    			attr_dev(a5, "href", "/marketing");
    			add_location(a5, file$7, 42, 5, 985);
    			add_location(li4, file$7, 41, 4, 975);
    			attr_dev(ul0, "class", "nav");
    			add_location(ul0, file$7, 16, 3, 382);
    			attr_dev(div1, "class", "sidebar-wrapper");
    			add_location(div1, file$7, 10, 2, 236);
    			attr_dev(div2, "class", "sidebar");
    			add_location(div2, file$7, 9, 1, 212);
    			attr_dev(a6, "class", "navbar-brand");
    			attr_dev(a6, "href", "#pablo");
    			add_location(a6, file$7, 53, 4, 1266);
    			attr_dev(span0, "class", "navbar-toggler-bar burger-lines");
    			add_location(span0, file$7, 55, 5, 1514);
    			attr_dev(span1, "class", "navbar-toggler-bar burger-lines");
    			add_location(span1, file$7, 56, 5, 1573);
    			attr_dev(span2, "class", "navbar-toggler-bar burger-lines");
    			add_location(span2, file$7, 57, 5, 1632);
    			attr_dev(button, "href", "");
    			attr_dev(button, "class", "navbar-toggler navbar-toggler-right");
    			attr_dev(button, "type", "button");
    			attr_dev(button, "data-toggle", "collapse");
    			attr_dev(button, "aria-controls", "navigation-index");
    			attr_dev(button, "aria-expanded", "false");
    			attr_dev(button, "aria-label", "Toggle navigation");
    			add_location(button, file$7, 54, 4, 1325);
    			attr_dev(span3, "class", "no-icon");
    			toggle_class(span3, "text-info", ctx.activePage == "users");
    			add_location(span3, file$7, 63, 32, 2003);
    			attr_dev(a7, "class", "nav-link");
    			attr_dev(a7, "href", "javascript:;");
    			add_location(a7, file$7, 62, 28, 1930);
    			attr_dev(li5, "class", "nav-item");
    			add_location(li5, file$7, 61, 24, 1840);
    			attr_dev(span4, "class", "no-icon");
    			toggle_class(span4, "text-info", ctx.activePage == "roles");
    			add_location(span4, file$7, 68, 32, 2335);
    			attr_dev(a8, "class", "nav-link");
    			attr_dev(a8, "href", "javascript:;");
    			add_location(a8, file$7, 67, 28, 2262);
    			attr_dev(li6, "class", "nav-item");
    			add_location(li6, file$7, 66, 24, 2172);
    			attr_dev(span5, "class", "no-icon");
    			toggle_class(span5, "text-info", ctx.activePage == "permissions");
    			add_location(span5, file$7, 73, 32, 2668);
    			attr_dev(a9, "class", "nav-link");
    			attr_dev(a9, "href", "javascript:;");
    			add_location(a9, file$7, 72, 28, 2595);
    			attr_dev(li7, "class", "nav-item");
    			add_location(li7, file$7, 71, 24, 2499);
    			attr_dev(span6, "class", "no-icon");
    			add_location(span6, file$7, 78, 8, 2977);
    			attr_dev(a10, "class", "nav-link");
    			attr_dev(a10, "href", "javascript:;");
    			add_location(a10, file$7, 77, 7, 2928);
    			attr_dev(li8, "class", "nav-item");
    			add_location(li8, file$7, 76, 6, 2821);
    			attr_dev(ul1, "class", "navbar-nav ml-auto");
    			add_location(ul1, file$7, 60, 5, 1784);
    			attr_dev(div3, "class", "collapse navbar-collapse justify-content-end");
    			attr_dev(div3, "id", "navigation");
    			add_location(div3, file$7, 59, 4, 1704);
    			attr_dev(div4, "class", "container-fluid");
    			add_location(div4, file$7, 52, 3, 1232);
    			attr_dev(nav0, "class", "navbar navbar-expand-lg");
    			attr_dev(nav0, "color-on-scroll", "500");
    			add_location(nav0, file$7, 51, 2, 1169);
    			attr_dev(div5, "class", "row");
    			add_location(div5, file$7, 88, 4, 3175);
    			attr_dev(div6, "class", "container-fluid");
    			add_location(div6, file$7, 87, 3, 3141);
    			attr_dev(div7, "class", "content");
    			add_location(div7, file$7, 86, 2, 3116);
    			attr_dev(p5, "class", "copyright text-center");
    			add_location(p5, file$7, 102, 5, 3588);
    			add_location(nav1, file$7, 101, 4, 3577);
    			attr_dev(div8, "class", "container-fluid");
    			add_location(div8, file$7, 100, 3, 3543);
    			attr_dev(footer, "class", "footer");
    			add_location(footer, file$7, 99, 2, 3516);
    			attr_dev(div9, "class", "main-panel");
    			add_location(div9, file$7, 50, 1, 1142);
    			attr_dev(div10, "class", "wrapper");
    			add_location(div10, file$7, 8, 0, 189);

    			dispose = [
    				listen_dev(li5, "click", ctx.click_handler, false, false, false),
    				listen_dev(li6, "click", ctx.click_handler_1, false, false, false),
    				listen_dev(li7, "click", ctx.click_handler_2, false, false, false),
    				listen_dev(li8, "click", ctx.click_handler_3, false, false, false)
    			];
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div10, anchor);
    			append_dev(div10, div2);
    			append_dev(div2, div1);
    			append_dev(div1, div0);
    			append_dev(div0, a0);
    			append_dev(div1, t1);
    			append_dev(div1, ul0);
    			append_dev(ul0, li0);
    			append_dev(li0, a1);
    			append_dev(a1, i0);
    			append_dev(a1, t2);
    			append_dev(a1, p0);
    			append_dev(ul0, t4);
    			append_dev(ul0, li1);
    			append_dev(li1, a2);
    			append_dev(a2, i1);
    			append_dev(a2, t5);
    			append_dev(a2, p1);
    			append_dev(ul0, t7);
    			append_dev(ul0, li2);
    			append_dev(li2, a3);
    			append_dev(a3, i2);
    			append_dev(a3, t8);
    			append_dev(a3, p2);
    			append_dev(ul0, t10);
    			append_dev(ul0, li3);
    			append_dev(li3, a4);
    			append_dev(a4, i3);
    			append_dev(a4, t11);
    			append_dev(a4, p3);
    			append_dev(ul0, t13);
    			append_dev(ul0, li4);
    			append_dev(li4, a5);
    			append_dev(a5, i4);
    			append_dev(a5, t14);
    			append_dev(a5, p4);
    			append_dev(div10, t16);
    			append_dev(div10, div9);
    			append_dev(div9, nav0);
    			append_dev(nav0, div4);
    			append_dev(div4, a6);
    			append_dev(div4, t18);
    			append_dev(div4, button);
    			append_dev(button, span0);
    			append_dev(button, t19);
    			append_dev(button, span1);
    			append_dev(button, t20);
    			append_dev(button, span2);
    			append_dev(div4, t21);
    			append_dev(div4, div3);
    			append_dev(div3, ul1);
    			append_dev(ul1, li5);
    			append_dev(li5, a7);
    			append_dev(a7, span3);
    			append_dev(ul1, t23);
    			append_dev(ul1, li6);
    			append_dev(li6, a8);
    			append_dev(a8, span4);
    			append_dev(ul1, t25);
    			append_dev(ul1, li7);
    			append_dev(li7, a9);
    			append_dev(a9, span5);
    			append_dev(ul1, t27);
    			append_dev(ul1, li8);
    			append_dev(li8, a10);
    			append_dev(a10, span6);
    			append_dev(div9, t29);
    			append_dev(div9, div7);
    			append_dev(div7, div6);
    			append_dev(div6, div5);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].m(div5, null);
    			}

    			append_dev(div9, t30);
    			append_dev(div9, footer);
    			append_dev(footer, div8);
    			append_dev(div8, nav1);
    			append_dev(nav1, p5);
    			current = true;
    		},
    		p: function update(changed, ctx) {
    			if (changed.activePage) {
    				toggle_class(span3, "text-info", ctx.activePage == "users");
    			}

    			if (changed.activePage) {
    				toggle_class(span4, "text-info", ctx.activePage == "roles");
    			}

    			if (changed.activePage) {
    				toggle_class(span5, "text-info", ctx.activePage == "permissions");
    			}

    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(changed, ctx);

    			if (current_block_type_index !== previous_block_index) {
    				if (if_block) {
    					group_outros();

    					transition_out(if_blocks[previous_block_index], 1, 1, () => {
    						if_blocks[previous_block_index] = null;
    					});

    					check_outros();
    				}

    				if (~current_block_type_index) {
    					if_block = if_blocks[current_block_type_index];

    					if (!if_block) {
    						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    						if_block.c();
    					}

    					transition_in(if_block, 1);
    					if_block.m(div5, null);
    				} else {
    					if_block = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div10);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].d();
    			}

    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$7($$self, $$props, $$invalidate) {
    	let activePage = "users";
    	const click_handler = () => $$invalidate("activePage", activePage = "users");
    	const click_handler_1 = () => $$invalidate("activePage", activePage = "roles");
    	const click_handler_2 = () => $$invalidate("activePage", activePage = "permissions");

    	const click_handler_3 = () => {
    		localStorage.clear();
    		window.location.replace("/login");
    	};

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("activePage" in $$props) $$invalidate("activePage", activePage = $$props.activePage);
    	};

    	return {
    		activePage,
    		click_handler,
    		click_handler_1,
    		click_handler_2,
    		click_handler_3
    	};
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$7.name
    		});
    	}
    }

    const app = new App({
    	target: document.body,
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map

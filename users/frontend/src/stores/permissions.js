import { writable } from 'svelte/store';

const permissions = writable({ isSet: false, data: [] });


const permissionsStore = {
    subscribe: permissions.subscribe,
    set: u => {
        permissions.set({ isSet: true, data: u });
    },
    add: u => {
        permissions.update(item => {
            let permissionsCopy = [ ...item.data ];
            permissionsCopy = [ ...permissionsCopy, u ];
            return { isSet: item.isSet, data: permissionsCopy };
        });
    },
    delete: r => {
        permissions.update(item => {
            let permissionsCopy = [ ...item.data ];
            permissionsCopy = permissionsCopy.filter(i => i.id !== r);
            return { isSet: item.isSet, data: permissionsCopy };
        });
    }
};

export default permissionsStore;

import { writable } from 'svelte/store';

const roles = writable({ isSet: false, data: [] });


const rolesStore = {
    subscribe: roles.subscribe,
    set: u => {
        roles.set({ isSet: true, data: u });
    },
    add: u => {
        roles.update(item => {
            let rolesCopy = [ ...item.data ];
            rolesCopy = [ ...rolesCopy, u ];
            return { isSet: item.isSet, data: rolesCopy };
        });
    },
    delete: r => {
        roles.update(item => {
            let rolesCopy = [ ...item.data ];
            rolesCopy = rolesCopy.filter(i => i.id !== r);
            return { isSet: item.isSet, data: rolesCopy };
        });
    }
};

export default rolesStore;

import { writable } from 'svelte/store';

//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiZWxfcGFzdGFzIjoiYWRtaW5Ac3lzdGVtIiwidmFyZGFzIjoiU3lzdGVtIiwicGF2YXJkZSI6IkFkbWluIiwiYXJfYWRtaW5pc3RyYXRvcml1cyI6dHJ1ZSwic3VrdXJpbW9fZGF0YSI6IjIwMTktMTEtMDFUMDA6MDA6MDBaIiwiaXN0cnluaW1vX2RhdGEiOm51bGwsInN1a3VydGFfdmFydG90b2pvIjoxLCJ0ZWlzZXMiOm51bGwsInJvbGVzX2lkIjpudWxsLCJyb2xlIjpudWxsLCJleHAiOjE1NzUwNDE4Njl9.7tgD1S-gEwkjATTaxFaIFN4roVIt4FypyxRbRNe1bfo
const users = writable({ isSet: false, data: [] });


const usersStore = {
    subscribe: users.subscribe,
    set: u => {
        users.set({ isSet: true, data: u });
    },
    add: u => {
        users.update(item => {
            let usersCopy = [ ...item.data ];
            usersCopy = [ u, ...usersCopy ];
            return { isSet: item.isSet, data: usersCopy };
        });
    },
    delete: u => {
        users.update(item => {
            let usersCopy = [ ...item.data ];
            usersCopy = usersCopy.filter(i => i.id !== u);
            return { isSet: item.isSet, data: usersCopy };
        });
    },
    updateRole: (u, r) => {
        users.update(item => {
            let index = item.data.findIndex(i => i.id === u);
            let usersCopy = [ ...item.data ];
            usersCopy[index].roles_id = r;
            return { isSet: item.isSet, data: usersCopy };
        });
    }
};

export default usersStore;

export function isEmpty(val) {
    return val.trim().length === 0;
}
export function isLongerThan(val, l) {
    return val.trim().length > l;
}
export function isIn(val, array) {
    return array.includes(val);
}
export function isValidEmail(val) {
    return new RegExp(
        "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
    ).test(val);
}

export function isValidPhone(val) {
    return new RegExp(
        "(86[0-9]{7}|[+]370[0-9]{8})"
    ).test(val);
}

export function GET(url, authorization = true) {
    return request("GET", url, null, authorization)
}
export function POST(url, body, authorization = true, formData = false) {
    return request("POST", url, body, authorization, formData)
}
export function PUT(url, body, authorization = true, formData = false) {
    return request("PUT", url, body, authorization, formData)
}
export function PATCH(url, body, authorization = true, formData = false) {
    return request("PATCH", url, body, authorization, formData)
}
export function DELETE(url, authorization = true) {
    return request("DELETE", url, null, authorization)
}


// --------------NOT EXPORTED------------------
function request(method, url, body, authorization, formData) {
    let headers = {"Content-Type": "application/json"};
    if (authorization) {
        headers["Authorization"] = "Bearer " + localStorage.getItem("token")
    }
    let reqBody = {
        method: method,
        headers: headers
    };
    if (method === "POST" || method === "PUT" || method === "PATCH") {
        if (formData) {
            reqBody.body = body;
            delete reqBody.headers["Content-Type"];
        } else {
            reqBody.body = JSON.stringify(body);
        }
    }

    let error = null;
    return fetch('BACKEND_URL' + url, reqBody).then(res => {
        if(!res.ok) {
            if(authorization && res.status === 401) {
                window.location.replace("/login");
                localStorage.clear();
                return;
            }
            error = {
                status:     res.status,
                statusText: res.statusText,
            };
        }
        return res.text();
    }).then(data => {
        if (error != null) {
            error.body = data ? JSON.parse(data) : {};
            return {
                status: false,
                data: error
            };
        }
        return {
            status: true,
            data: data ? JSON.parse(data) : {}
        };
    });
}

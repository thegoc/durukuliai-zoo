package main

import (
	"github.com/labstack/echo"
	"os"
	"users-api/server"
)

func main() {
	e := echo.New()
	server.New()
	server.Routes(e)

	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
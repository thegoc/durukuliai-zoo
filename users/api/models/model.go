package models

import (
	"github.com/jmoiron/sqlx"
	"gopkg.in/gomail.v2"
	"os"
)

var db *sqlx.DB
var mail *gomail.Dialer

func SetDB(database *sqlx.DB) {
	db = database
}
func SetMailClient(m *gomail.Dialer) {
	mail = m
}

func SendMail(receiver string, subject string, message string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", os.Getenv("EMAIL_SYSTEM"))
	m.SetHeader("To", receiver)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", message)
	return mail.DialAndSend(m)
}

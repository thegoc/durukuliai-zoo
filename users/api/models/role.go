package models

type Role struct {
	ID			int64		`json:"id"`
	Name		string		`json:"pavadinimas" db:"pavadinimas" validate:"required"`
}

func (r *Role) Get() error {
	return db.Get(r, "SELECT * FROM roles WHERE id = ?", r.ID)
}

func (r *Role) List() (roles []Role, err error) {
	roles = make([]Role, 0)
	err = db.Select(&roles, `SELECT * FROM roles`)
	return roles, err
}

func (r *Role) Delete() (err error) {
	tx, _ := db.Begin()
	_, err = tx.Exec(`UPDATE vartotojai SET roles_id = null WHERE roles_id = ?`, r.ID)
	if err != nil {
		return
	}
	_, err = tx.Exec(`DELETE FROM roliu_teises WHERE roles_id = ?`, r.ID)
	if err != nil {
		return
	}
	_, err = tx.Exec(`DELETE FROM roles WHERE id = ?`, r.ID)
	if err != nil {
		return
	}
	return tx.Commit()
}

func (r *Role) Create() (err error) {
	res, err := db.Exec("INSERT INTO roles (pavadinimas) VALUES (?)", r.Name)
	if err != nil {
		return err
	}
	id, err := res.LastInsertId()
	r.ID = id
	return
}

func (r *Role) ListPermissions() (per []PermissionEx, err error) {
	per = make([]PermissionEx, 0)
	err = db.Select(&per, `SELECT t.*, roliu_teises.roles_id AS roles_id
								FROM roliu_teises
								LEFT JOIN teises t on roliu_teises.teises_id = t.id
								WHERE roles_id = ?`, r.ID)
	p := &Permission{}
	per2, err := p.List()
	if err != nil {
		return
	}

	f := func(id int64, p []PermissionEx) bool {
		ans := false
		for _, v := range p {
			if id == v.ID {
				ans = true
			}
		}
		return ans
	}
	for _, v := range per2 {
		if !f(v.ID, per) {
			per = append(per, PermissionEx{ Permission: v, RoleID: nil })
		}
	}

	return
}

func (r *Role) TogglePermission(id int64) error {
	var pID int64
	err := db.Get(&pID, `SELECT id FROM roliu_teises WHERE roles_id = ? AND teises_id = ?`, r.ID, id)
	if err != nil {
		_, err = db.Exec("INSERT INTO roliu_teises(roles_id, teises_id) VALUES (?,?)", r.ID, id)
		return err
	}
	_, err = db.Exec("DELETE FROM roliu_teises WHERE id = ?", pID)
	return err
}
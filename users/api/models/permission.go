package models

type (
	Permission struct {
		ID				int64	`json:"id"`
		Name 			string	`json:"pavadinimas" db:"pavadinimas" validate:"required"`
		SysName			string	`json:"sis_pavadinimas" db:"sis_pavadinimas" validate:"required"`
	}
	PermissionEx struct {
		Permission
		RoleID			*int64		`json:"roles_id" db:"roles_id"`
	}
)

func (p *Permission) Get() error {
	return db.Get(p, "SELECT * FROM teises WHERE id = ?", p.ID)
}

func (p *Permission) List() (per []Permission, err error) {
	per = make([]Permission, 0)
	err = db.Select(&per, `SELECT * FROM teises`)
	return
}

func (p *Permission) Create() (err error) {
	res, err := db.Exec("INSERT INTO teises (pavadinimas, sis_pavadinimas) VALUES (?,?)", p.Name, p.SysName)
	if err != nil {
		return err
	}
	id, err := res.LastInsertId()
	p.ID = id
	return
}

func (p *Permission) Delete() (err error) {
	tx, _ := db.Begin()
	_, err = tx.Exec(`DELETE FROM roliu_teises WHERE teises_id = ?`, p.ID)
	if err != nil {
		return
	}
	_, err = tx.Exec(`DELETE FROM teises WHERE id = ?`, p.ID)
	if err != nil {
		return
	}
	return tx.Commit()
}

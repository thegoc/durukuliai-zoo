package models

import (
	"fmt"
	"github.com/labstack/gommon/random"
	"time"
	h "users-api/helpers"
)
type (
	User struct {
		ID					int64			`json:"id"`
		Email				string			`json:"el_pastas" db:"el_pastas" validate:"required,email"`
		Password			string			`json:"-" db:"slaptazodis"`
		FirstName			string			`json:"vardas" db:"vardas" validate:"required"`
		LastName			string			`json:"pavarde" db:"pavarde" validate:"required"`
		IsAdmin				bool			`json:"ar_administratorius" db:"ar_administratorius"`
		CreatedAt 			time.Time		`json:"sukurimo_data" db:"sukurimo_data"`
		DeletedAt 			*time.Time		`json:"istrynimo_data" db:"istrynimo_data"`
		CreatedBy			int64			`json:"sukurta_vartotojo" db:"sukurta_vartotojo"`
		Permissions			*[]string		`json:"teises"`
		RoleID				*int64			`json:"roles_id" db:"roles_id"`
		Role				*string			`json:"role"`
	}
	UserExt struct {
		User
		*Permission
		RoleName	*string		`json:"roles_pavadinimas" db:"roles_pavadinimas"`
	}
)

func (u *User) Get() error {
	return db.Get(u, "SELECT * FROM vartotojai WHERE id = ?", u.ID)
}

func (u *User) GetByEmail() error {
	return db.Get(u, "SELECT * FROM vartotojai WHERE el_pastas = ?", u.Email)
}

func (u *User) GetExByEmail() error {
	var uxs []UserExt
	err := db.Select(&uxs, `
				SELECT 
					vartotojai.* ,
					r.pavadinimas AS roles_pavadinimas,
				    t.sis_pavadinimas
				FROM vartotojai 
				INNER JOIN roles r on vartotojai.roles_id = r.id
				INNER JOIN roliu_teises rt on r.id = rt.roles_id
				INNER JOIN teises t on rt.teises_id = t.id
				WHERE vartotojai.el_pastas = ?
			`, u.Email)

	if len(uxs) == 0 {
		return u.GetByEmail()
	}

	fmt.Println(uxs)

	*u = uxs[0].User
	u.Role = uxs[0].RoleName
	permissions := make([]string, 0)
	for _, v := range uxs {
		permissions = append(permissions, v.Permission.SysName)
	}
	u.Permissions = &permissions
	return err
}

func (u *User) List() (users []User, err error) {
	users = make([]User, 0)
	err = db.Select(&users, `SELECT * FROM vartotojai WHERE istrynimo_data IS NULL AND id > 1 ORDER BY id DESC`)
	return users, err
}

func (u *User) ListDeleted() (users []User, err error) {
	users = make([]User, 0)
	err = db.Select(&users, `SELECT * FROM vartotojai WHERE istrynimo_data IS NOT NULL AND id > 1 ORDER BY id DESC`)
	return users, err
}

func (u *User) ToggleDel() (err error) {
	err = u.Get()
	if err != nil {
		return
	}
	if u.DeletedAt == nil {
		_, err = db.Exec(`UPDATE vartotojai SET istrynimo_data = CURRENT_TIMESTAMP WHERE id = ?`, u.ID)
		return
	}
	_, err = db.Exec(`UPDATE vartotojai SET istrynimo_data = null WHERE id = ?`, u.ID)
	return
}

func (u *User) Create() error {
	randomPassword := random.String(8, random.Alphabetic)
	hh := h.Hash{}
	hash, err := hh.Generate(randomPassword)
	if err != nil {
		return err
	}
	u.Password = hash

	tx, _ := db.Begin()
	res, err := tx.Exec(
		"INSERT INTO vartotojai (el_pastas, slaptazodis, vardas, pavarde, ar_administratorius, roles_id, sukurta_vartotojo) VALUES (?,?,?,?,?,?,?)",
		u.Email, u.Password, u.FirstName, u.LastName, u.IsAdmin, u.RoleID, u.CreatedBy,
	)
	if err != nil {
		return err
	}
	id, _ := res.LastInsertId()
	u.ID = id
	u.CreatedAt = time.Now()

	err = SendMail(u.Email, "Prisijungimo duomenys",
		`Sveiki,<br> siunčiame prisijungimo duomenis prie DURUKULIAI sistemos: <br><br>
				<u>El. paštas</u>: <b style="text-decoration: none;">`+u.Email+`</b><br>
				<u>Slaptažodis</u>: <b>`+randomPassword+`</b> <br><br>
				<i>Prisijungus rekomenduojame pasikeisti slaptažodį.</i>`)

	if err == nil {
		err = tx.Commit()
	}

	return err
}

func (u *User) ChangeRole() (err error) {
	_, err = db.Exec(`UPDATE vartotojai SET roles_id = ? WHERE id = ?`, u.RoleID, u.ID)
	return
}

package server

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"users-api/controllers"
	"users-api/models"
)

func CurrentUser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		u := models.User{}
		user := (c.Get("user").(*jwt.Token)).Claims.(jwt.MapClaims)
		u.Email = user["el_pastas"].(string)
		u.IsAdmin = user["ar_administratorius"].(bool)
		u.ID = int64(user["id"].(float64))
		controllers.SetCurrUser(&u)
		return next(c)
	}
}
package server

import (
	"crypto/tls"
	"fmt"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"gopkg.in/gomail.v2"
	"log"
	"os"
	"users-api/models"
)

type CustomValidator struct {
	validator *validator.Validate
}
func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func New() {
	_ = godotenv.Load()

	con, err := sqlx.Connect(
		"mysql", fmt.Sprintf("%s:%s@(%s:%s)/%s?parseTime=true&loc=Local",
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASS"),
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_NAME")),
	)
	if err != nil {
		log.Fatalln(err)
	}
	models.SetDB(con)

	d := gomail.NewDialer(os.Getenv("EMAIL_SERVER"), 587, os.Getenv("EMAIL_SYSTEM"), os.Getenv("EMAIL_PASSWORD"))
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	models.SetMailClient(d)
}

package server

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"os"
	"users-api/controllers"
)

func Routes(e *echo.Echo) {
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{ "*" },
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete, http.MethodPatch},
	}))
	e.Validator = &CustomValidator{ validator: validator.New() }

	e.POST("/token", controllers.CreateToken)

	api := e.Group("/api")
	api.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
	api.Use(CurrentUser)

	api.GET("/users", controllers.UserList)
	api.GET("/users/deleted", controllers.UserListDeleted)
	api.POST("/users", controllers.UserCreate)
	api.PATCH("/users/:id/role", controllers.UserChangeRole)
	api.PATCH("/users/:id", controllers.UserToggleDeleted)

	api.GET("/roles", controllers.RoleList)
	api.GET("/roles/:id/permissions", controllers.RolePermissionsList)
	api.POST("/roles", controllers.RoleCreate)
	api.DELETE("/roles/:id", controllers.RoleDelete)
	api.PATCH("/roles/:role-id/permissions/:permission-id", controllers.RolePermissionToggle)

	api.GET("/permissions", controllers.PermissionsList)
	api.POST("/permissions", controllers.PermissionCreate)
	api.DELETE("/permissions/:id", controllers.PermissionDelete)
}

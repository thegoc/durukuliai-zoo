package controllers

import (
	e "github.com/labstack/echo"
	"net/http"
	"strconv"
	m "users-api/models"
)

func UserList(c e.Context) error {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	u := m.User{}
	users, err := u.List()
	if err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't get users from database. " + err.Error())
	}
	return c.JSON(http.StatusOK, users)
}

func UserListDeleted(c e.Context) error {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	u := m.User{}
	users, err := u.ListDeleted()
	if err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't get users from database. " + err.Error())
	}
	return c.JSON(http.StatusOK, users)
}

func UserToggleDeleted(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	u := m.User{}
	u.ID, _ = strconv.ParseInt(c.Param("id"), 10, 64)
	if err = u.ToggleDel(); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't toggle deletion. " + err.Error())
	}
	return c.NoContent(http.StatusOK)
}

func UserCreate(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	u := &m.User{}
	if err = c.Bind(u); err != nil {
		return
	}
	u.CreatedBy = currUser.ID
	if u.RoleID != nil {
		r := &m.Role{ ID: *u.RoleID }
		if err := r.Get(); err != nil {
			return e.NewHTTPError(http.StatusUnprocessableEntity, "Validation error. Role does not exists.")
		}
	}
	if err = c.Validate(u); err != nil {
		return e.NewHTTPError(http.StatusUnprocessableEntity, "Validation error. " + err.Error())
	}
	err = u.Create()
	if err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't create new user. " + err.Error())
	}
	return c.JSON(http.StatusOK, u)
}

func UserChangeRole(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	u := &m.User{}
	if err = c.Bind(u); err != nil {
		return
	}
	u.ID, _ = strconv.ParseInt(c.Param("id"), 10, 64)

	if u.RoleID != nil {
		r := &m.Role{ ID: *u.RoleID }
		if err := r.Get(); err != nil {
			return e.NewHTTPError(http.StatusUnprocessableEntity, "Validation error. Role does not exists.")
		}
	}

	if err = u.ChangeRole(); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't change user's role. " + err.Error())
	}
	return c.NoContent(http.StatusOK)
}
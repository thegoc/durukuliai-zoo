package controllers

import (
	"github.com/dgrijalva/jwt-go"
	e "github.com/labstack/echo"
	"net/http"
	"os"
	"time"
	"users-api/helpers"
	"users-api/models"
)

type jwtCustomClaims struct {
	models.User
	jwt.StandardClaims
}

func CreateToken(c e.Context) (err error) {
	type AuthUser struct {
		Email		string		`json:"el_pastas"`
		Password	string		`json:"slaptazodis"`
	}
	aUser := &AuthUser{}
	err = c.Bind(aUser)
	if err != nil {
		return err
	}
	user := models.User{ Email: aUser.Email, Password: aUser.Password }
	err = user.GetExByEmail()
	if err != nil {
		return e.ErrNotFound
	}

	h := helpers.Hash{}
	if err = h.Compare(user.Password, aUser.Password); err != nil {
		return e.ErrUnauthorized
	}

	claims := &jwtCustomClaims{
		user,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"user": user,
		"token": t,
	})
}
package controllers

import (
	e "github.com/labstack/echo"
	"net/http"
	"strconv"
	m "users-api/models"
)

func RoleList(c e.Context) error {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	r := m.Role{}
	roles, err := r.List()
	if err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't get roles from database. " + err.Error())
	}
	return c.JSON(http.StatusOK, roles)
}

func RoleDelete(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	r := m.Role{}
	r.ID, _ = strconv.ParseInt(c.Param("id"), 10, 64)
	if err = r.Delete(); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't delete role from database. " + err.Error())
	}
	return c.NoContent(http.StatusOK)
}

func RoleCreate(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	r := &m.Role{}
	if err = c.Bind(r); err != nil {
		return
	}
	if err = c.Validate(r); err != nil {
		return e.NewHTTPError(http.StatusUnprocessableEntity, "Validation error. " + err.Error())
	}
	if err = r.Create(); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't create a role. " + err.Error())
	}
	return c.JSON(http.StatusOK, r)
}

func RolePermissionsList(c e.Context) error {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	r := &m.Role{}
	r.ID, _ = strconv.ParseInt(c.Param("id"), 10, 64)
	permissions, err := r.ListPermissions()
	if err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't get permissions from database. " + err.Error())
	}
	return c.JSON(http.StatusOK, permissions)
}

func RolePermissionToggle(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	r := &m.Role{}
	p := &m.Permission{}
	r.ID, _ = strconv.ParseInt(c.Param("role-id"), 10, 64)
	p.ID, _ = strconv.ParseInt(c.Param("permission-id"), 10, 64)

	if err = r.Get(); err != nil {
		return e.NewHTTPError(http.StatusUnprocessableEntity, "Role does not exists. " + err.Error())
	}
	if err = p.Get(); err != nil {
		return e.NewHTTPError(http.StatusUnprocessableEntity, "Permission does not exists. " + err.Error())
	}

	if err = r.TogglePermission(p.ID); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't toggle. " + err.Error())
	}
	return c.NoContent(http.StatusOK)
}
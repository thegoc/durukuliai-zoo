package controllers

import (
	e "github.com/labstack/echo"
	"net/http"
	"strconv"
	m "users-api/models"
)

func PermissionsList(c e.Context) error {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	p := &m.Permission{}
	permissions, err := p.List()
	if err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't get permissions from database. " + err.Error())
	}
	return c.JSON(http.StatusOK, permissions)
}

func PermissionDelete(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	p := &m.Permission{}
	p.ID, _ = strconv.ParseInt(c.Param("id"), 10, 64)
	if err = p.Delete(); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't delete permission from database. " + err.Error())
	}
	return c.NoContent(http.StatusOK)
}

func PermissionCreate(c e.Context) (err error) {
	if !currUser.IsAdmin {
		return e.ErrUnauthorized
	}
	p := &m.Permission{}
	if err = c.Bind(p); err != nil {
		return
	}
	if err = c.Validate(p); err != nil {
		return e.NewHTTPError(http.StatusUnprocessableEntity, "Validation error. " + err.Error())
	}
	if err = p.Create(); err != nil {
		return e.NewHTTPError(http.StatusInternalServerError, "Can't create a permission. " + err.Error())
	}
	return c.JSON(http.StatusOK, p)
}
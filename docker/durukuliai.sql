-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Dec 11, 2019 at 04:09 PM
-- Server version: 5.7.28
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `durukuliai`
--

-- --------------------------------------------------------

--
-- Table structure for table `aptvaras`
--

CREATE TABLE `aptvaras` (
  `id` int(11) NOT NULL,
  `tipas` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_lithuanian_ci NOT NULL,
  `dydis_kub_m` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aptvaras`
--

INSERT INTO `aptvaras` (`id`, `tipas`, `dydis_kub_m`) VALUES
(1, 'Narvas', 150),
(2, 'Atvira Savana', 100000),
(3, 'Stiklinis', 0.5);

-- --------------------------------------------------------

--
-- Table structure for table `gyvunas`
--

CREATE TABLE `gyvunas` (
  `id` int(11) NOT NULL,
  `rusis` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `vardas` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `gimimo_data` date NOT NULL,
  `ar_skiepytas` tinyint(1) NOT NULL,
  `paskutinis_skiepijimas` date NOT NULL,
  `ar_sertas` tinyint(1) NOT NULL,
  `aptvaro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gyvunas`
--

INSERT INTO `gyvunas` (`id`, `rusis`, `vardas`, `gimimo_data`, `ar_skiepytas`, `paskutinis_skiepijimas`, `ar_sertas`, `aptvaro_id`) VALUES
(6, 'Liūtas', 'Karalius', '2015-09-09', 1, '2019-10-22', 0, 1),
(7, 'Liūtas', 'Karalienė', '2016-12-12', 0, '2019-12-16', 1, 1),
(8, 'Liūtas', 'Simba', '2018-02-05', 1, '2019-03-08', 1, 1),
(9, 'Gazelė', 'Dora', '2017-10-18', 0, '2016-01-02', 0, 2),
(10, 'Žirafa', 'Nora', '2016-12-31', 1, '2019-10-08', 1, 2),
(11, 'Voras', 'Pomidoras', '2018-11-11', 1, '2019-07-15', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `kontaktai`
--

CREATE TABLE `kontaktai` (
  `id` int(11) NOT NULL,
  `el_pastas` varchar(50) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `telefonas` varchar(50) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `twitter` varchar(50) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `leisti_el_pastas` tinyint(1) NOT NULL,
  `leisti_telefonas` tinyint(1) NOT NULL,
  `leisti_twitter` tinyint(1) NOT NULL,
  `atnaujinta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontaktai`
--

INSERT INTO `kontaktai` (`id`, `el_pastas`, `telefonas`, `twitter`, `leisti_el_pastas`, `leisti_telefonas`, `leisti_twitter`, `atnaujinta`) VALUES
(1, 'benas@benaitis.com', '', 'ponasbenas', 0, 0, 0, '2019-12-10 16:51:33'),
(2, 'Tadas@gmail.com', '+37654135746513', '', 1, 1, 1, '2019-12-10 16:51:45'),
(3, 'abendoraitis@132.com', '', '', 1, 0, 1, '2019-12-10 16:51:57'),
(4, '', '8642316713', '', 0, 1, 0, '2019-12-10 16:52:08'),
(5, 'braskytea@gmail.com', '+36482181158', 'ledinebraske', 1, 1, 1, '2019-12-10 16:52:28'),
(6, 'yahoo@yahoo.com', '+3618249841', 'yahhoisbest', 0, 1, 0, '2019-12-10 16:52:43'),
(7, 'nuriu5@ktu.edu', '', '', 1, 0, 1, '2019-12-10 16:52:58'),
(8, 'koko@lado.com', '+1684513123', '', 0, 1, 0, '2019-12-10 16:53:09'),
(9, 'gal@jau.gana', '', 'galganajau', 0, 0, 0, '2019-12-10 16:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `nurasymas`
--

CREATE TABLE `nurasymas` (
  `id` int(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kiekis` int(10) NOT NULL,
  `zoo_pasaras_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parduoti_bilietai`
--

CREATE TABLE `parduoti_bilietai` (
  `id` int(11) NOT NULL,
  `kiekis` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parduoti_bilietai`
--

INSERT INTO `parduoti_bilietai` (`id`, `kiekis`, `data`) VALUES
(1, 5, '2019-12-10 00:00:00'),
(2, 10, '2019-12-01 00:00:00'),
(3, 11, '2019-11-09 00:00:00'),
(4, 3, '2019-11-02 00:00:00'),
(5, 50, '2019-12-11 14:28:47'),
(6, 2, '2019-10-28 00:00:00'),
(7, 4, '2019-10-12 00:00:00'),
(8, 13, '2019-09-09 00:00:00'),
(9, 5, '2019-08-01 00:00:00'),
(10, 8, '2019-09-17 00:00:00'),
(11, 89, '2019-10-23 00:00:00'),
(12, 6, '2019-10-16 00:00:00'),
(13, 3, '2019-11-15 00:00:00'),
(14, 4, '2019-12-06 00:00:00'),
(15, 3, '2019-12-08 00:00:00'),
(16, 54, '2019-12-07 00:00:00'),
(17, 23, '2019-11-25 00:00:00'),
(18, 45, '2019-12-07 00:00:00'),
(19, 23, '2019-12-09 00:00:00'),
(20, 8, '2019-09-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `reklamos`
--

CREATE TABLE `reklamos` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(50) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `pradzia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pabaiga` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sukurimo_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kaina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reklamos`
--

INSERT INTO `reklamos` (`id`, `pavadinimas`, `pradzia`, `pabaiga`, `sukurimo_data`, `kaina`) VALUES
(1, 'Gruodžio reklama laikraštyje', '2019-12-01 00:00:00', '2019-12-07 00:00:00', '2019-12-10 16:54:10', 1002356),
(2, 'Facebook reklama pirmas ketvirtis', '2019-01-01 00:00:00', '2019-03-31 00:00:00', '2019-12-10 16:54:46', 12568945),
(3, 'Lankstinukai Lapkritis', '2019-11-01 00:00:00', '2019-11-30 00:00:00', '2019-12-10 16:56:04', 56400),
(4, 'Stonkaus Bandymas', '2019-10-01 00:00:00', '2019-10-01 00:00:00', '2019-12-10 16:57:06', 64975156);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `pavadinimas` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `pavadinimas`) VALUES
(1, 'Pardavimų vadybininkas'),
(2, 'Vartotojas'),
(3, 'Marketingo vadybininkas'),
(4, 'Prižiūrėtojas');

-- --------------------------------------------------------

--
-- Table structure for table `roliu_teises`
--

CREATE TABLE `roliu_teises` (
  `id` int(10) UNSIGNED NOT NULL,
  `roles_id` int(10) UNSIGNED NOT NULL,
  `teises_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `roliu_teises`
--

INSERT INTO `roliu_teises` (`id`, `roles_id`, `teises_id`) VALUES
(5, 2, 3),
(6, 1, 3),
(7, 1, 2),
(8, 1, 1),
(9, 3, 4),
(10, 3, 3),
(11, 3, 1),
(12, 4, 3),
(13, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `serimas`
--

CREATE TABLE `serimas` (
  `id` int(11) NOT NULL,
  `laikas` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gyvunas_id` int(11) NOT NULL,
  `vartotojas_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teises`
--

CREATE TABLE `teises` (
  `id` int(10) UNSIGNED NOT NULL,
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `sis_pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `teises`
--

INSERT INTO `teises` (`id`, `pavadinimas`, `sis_pavadinimas`) VALUES
(1, 'Redaguoti twiterį', 'edit_twitter'),
(2, 'Sukurti sąslaitą', 'create_invoice'),
(3, 'Peržiūrėti profilį', 'view_current_user_info'),
(4, 'Marketingo posistemės prieiga', 'marketing_access'),
(5, 'Gyvunų posistemės prieiga', 'animal_access');

-- --------------------------------------------------------

--
-- Table structure for table `tiekejas`
--

CREATE TABLE `tiekejas` (
  `pavadinimas` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `miestas` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `tel_numeris` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiekejas`
--

INSERT INTO `tiekejas` (`pavadinimas`, `miestas`, `tel_numeris`) VALUES
('Maistas', 'Kaunas', '+370625611'),
('UAB \"Gyvūnams\"', 'Utena', '+37069874125682'),
('UAB \"Maistas Gyvūnams\"', 'Utena', '+370698458'),
('UAB \"Maistas\"', 'Kaunas', '+370684265'),
('UAB \"Pašarai gyvūnams\"', 'Klaipėda', '+37068425654'),
('UAB \"Pašarai\"', 'Kaunas', '+370698546985');

-- --------------------------------------------------------

--
-- Table structure for table `tiekejo_pasaras`
--

CREATE TABLE `tiekejo_pasaras` (
  `id` int(10) NOT NULL,
  `pavadinimas` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `kam_skirta` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL DEFAULT 'universalus',
  `kaina` float NOT NULL,
  `pagaminimo_data` date DEFAULT NULL,
  `galioja_iki` date DEFAULT NULL,
  `papildoma_info` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci,
  `tiekejas_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiekejo_pasaras`
--

INSERT INTO `tiekejo_pasaras` (`id`, `pavadinimas`, `kam_skirta`, `kaina`, `pagaminimo_data`, `galioja_iki`, `papildoma_info`, `tiekejas_id`) VALUES
(1, 'Morkos', 'universalus', 0.74, '2019-12-10', '2019-12-23', NULL, 'Maistas'),
(2, 'Morkos', 'universalus', 0.41, '2019-12-10', '2019-08-06', NULL, 'UAB \"Gyvūnams\"'),
(3, 'Morkos', 'universalus', 0.52, '2019-12-10', '2019-12-25', NULL, 'UAB \"Maistas\"'),
(4, 'Morkos', 'universalus', 0.32, '2019-09-16', '2020-03-26', NULL, 'UAB \"Maistas Gyvūnams\"'),
(5, 'Morkos', 'universalus', 0.63, '2019-09-09', '2019-12-28', NULL, 'UAB \"Pašarai\"'),
(6, 'Mokros', 'universalus', 0.96, '2019-10-14', '2020-03-20', NULL, 'UAB \"Pašarai gyvūnams\"'),
(7, 'Mėsa', 'Liūtai', 2.93, '2019-09-09', '2020-02-12', NULL, 'Maistas'),
(8, 'Mėsa', 'Lokiai', 8.69, '2019-08-05', '2019-12-25', NULL, 'UAB \"Maistas Gyvūnams\"'),
(9, 'Mėsa ', 'Vilkai', 2.36, '2019-12-20', '2020-02-12', NULL, 'UAB \"Pašarai gyvūnams\"'),
(10, 'Bananai', 'Beždžionės', 0.7, '2019-09-02', '2020-01-01', NULL, 'UAB \"Pašarai\"');

-- --------------------------------------------------------

--
-- Table structure for table `uzsakymas`
--

CREATE TABLE `uzsakymas` (
  `id` int(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verte` float DEFAULT NULL,
  `vartotojas_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uzsakymas`
--

INSERT INTO `uzsakymas` (`id`, `timestamp`, `verte`, `vartotojas_id`) VALUES
(1, '2019-12-02 17:51:27', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `uzsakymo_pasarai`
--

CREATE TABLE `uzsakymo_pasarai` (
  `tiekejo_pasaras_id` int(10) NOT NULL,
  `uzsakymas_id` int(10) NOT NULL,
  `kiekis` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uzsakymo_pasarai`
--

INSERT INTO `uzsakymo_pasarai` (`tiekejo_pasaras_id`, `uzsakymas_id`, `kiekis`) VALUES
(1, 1, NULL),
(2, 1, NULL),
(3, 1, NULL),
(4, 1, NULL),
(6, 1, NULL),
(7, 1, NULL),
(8, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vartotojai`
--

CREATE TABLE `vartotojai` (
  `id` int(10) UNSIGNED NOT NULL,
  `el_pastas` varchar(120) COLLATE utf8_lithuanian_ci NOT NULL,
  `slaptazodis` varchar(60) COLLATE utf8_lithuanian_ci NOT NULL,
  `vardas` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `pavarde` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `ar_administratorius` int(1) NOT NULL DEFAULT '0',
  `roles_id` int(10) UNSIGNED DEFAULT NULL,
  `sukurimo_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `istrynimo_data` timestamp NULL DEFAULT NULL,
  `sukurta_vartotojo` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `vartotojai`
--

INSERT INTO `vartotojai` (`id`, `el_pastas`, `slaptazodis`, `vardas`, `pavarde`, `ar_administratorius`, `roles_id`, `sukurimo_data`, `istrynimo_data`, `sukurta_vartotojo`) VALUES
(1, 'system', '', '', '', 0, NULL, '2019-11-18 17:10:36', NULL, 1),
(2, 'admin@system', '$2a$10$AVeyxPewXnyH4qWTTdlvnu0JVAQFEvaMIy6AAG1R6RrMvrhhFcvgi', 'System', 'Admin', 1, NULL, '2019-11-01 00:00:00', NULL, 1),
(3, 'basic@system', '$2a$10$AVeyxPewXnyH4qWTTdlvnu0JVAQFEvaMIy6AAG1R6RrMvrhhFcvgi', 'Basic', 'User', 0, 1, '2019-11-18 18:36:33', NULL, 2),
(4, 'mark@market', '$2a$10$AVeyxPewXnyH4qWTTdlvnu0JVAQFEvaMIy6AAG1R6RrMvrhhFcvgi', 'Mike', 'Bobler', 0, 3, '2019-12-08 19:24:06', NULL, 2),
(5, 'animal@animal', '$2a$10$AVeyxPewXnyH4qWTTdlvnu0JVAQFEvaMIy6AAG1R6RrMvrhhFcvgi', 'Bober', 'Bobbler', 0, 4, '2019-12-11 16:05:22', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `zoo_pasaras`
--

CREATE TABLE `zoo_pasaras` (
  `id` int(10) NOT NULL,
  `pavadinimas` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `kam_skirta` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL DEFAULT 'universalus',
  `pagaminimo_data` date DEFAULT NULL,
  `galioja_iki` date DEFAULT NULL,
  `kiekis` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zoo_pasaras`
--

INSERT INTO `zoo_pasaras` (`id`, `pavadinimas`, `kam_skirta`, `pagaminimo_data`, `galioja_iki`, `kiekis`) VALUES
(1, 'dfcgvbhjn', 'universalus', '2019-11-06', '2019-11-27', 20),
(2, 'Morkos', 'universalus', '2019-12-10', '2019-12-23', 30),
(3, 'Mėsa', 'Liūtai', '2019-09-09', '2020-02-12', 30),
(4, 'Morkos', 'universalus', '2019-12-10', '2019-08-06', 30),
(5, 'Morkos', 'universalus', '2019-09-16', '2020-03-26', 30),
(6, 'Mėsa', 'Lokiai', '2019-08-05', '2019-12-25', 30),
(7, 'Morkos', 'universalus', '2019-12-10', '2019-12-25', 30),
(8, 'Mokros', 'universalus', '2019-10-14', '2020-03-20', 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aptvaras`
--
ALTER TABLE `aptvaras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gyvunas`
--
ALTER TABLE `gyvunas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aptvaro_id_fk` (`aptvaro_id`);

--
-- Indexes for table `kontaktai`
--
ALTER TABLE `kontaktai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurasymas`
--
ALTER TABLE `nurasymas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zoo_pasaras_id` (`zoo_pasaras_id`);

--
-- Indexes for table `parduoti_bilietai`
--
ALTER TABLE `parduoti_bilietai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reklamos`
--
ALTER TABLE `reklamos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roliu_teises`
--
ALTER TABLE `roliu_teises`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roliu_teises_roles_id_fk` (`roles_id`),
  ADD KEY `roliu_teises_teises_id_fk` (`teises_id`);

--
-- Indexes for table `serimas`
--
ALTER TABLE `serimas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gyvunas_id` (`gyvunas_id`),
  ADD KEY `vartotojas_id` (`vartotojas_id`);

--
-- Indexes for table `teises`
--
ALTER TABLE `teises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tiekejas`
--
ALTER TABLE `tiekejas`
  ADD PRIMARY KEY (`pavadinimas`);

--
-- Indexes for table `tiekejo_pasaras`
--
ALTER TABLE `tiekejo_pasaras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tiekejas_id` (`tiekejas_id`);

--
-- Indexes for table `uzsakymas`
--
ALTER TABLE `uzsakymas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vartotojas_id` (`vartotojas_id`);

--
-- Indexes for table `uzsakymo_pasarai`
--
ALTER TABLE `uzsakymo_pasarai`
  ADD PRIMARY KEY (`tiekejo_pasaras_id`,`uzsakymas_id`),
  ADD KEY `tiekejo_pasaras_id` (`tiekejo_pasaras_id`),
  ADD KEY `uzsakymas_id` (`uzsakymas_id`);

--
-- Indexes for table `vartotojai`
--
ALTER TABLE `vartotojai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vartotojai_el_pastas` (`el_pastas`),
  ADD KEY `vartotojai_roles_id_fk` (`roles_id`),
  ADD KEY `vartotojai_vartotojai_id_fk` (`sukurta_vartotojo`);

--
-- Indexes for table `zoo_pasaras`
--
ALTER TABLE `zoo_pasaras`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aptvaras`
--
ALTER TABLE `aptvaras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gyvunas`
--
ALTER TABLE `gyvunas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kontaktai`
--
ALTER TABLE `kontaktai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `nurasymas`
--
ALTER TABLE `nurasymas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parduoti_bilietai`
--
ALTER TABLE `parduoti_bilietai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `reklamos`
--
ALTER TABLE `reklamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roliu_teises`
--
ALTER TABLE `roliu_teises`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `serimas`
--
ALTER TABLE `serimas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teises`
--
ALTER TABLE `teises`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tiekejo_pasaras`
--
ALTER TABLE `tiekejo_pasaras`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `uzsakymas`
--
ALTER TABLE `uzsakymas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vartotojai`
--
ALTER TABLE `vartotojai`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `zoo_pasaras`
--
ALTER TABLE `zoo_pasaras`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gyvunas`
--
ALTER TABLE `gyvunas`
  ADD CONSTRAINT `aptvaro_id_fk` FOREIGN KEY (`aptvaro_id`) REFERENCES `aptvaras` (`id`);

--
-- Constraints for table `nurasymas`
--
ALTER TABLE `nurasymas`
  ADD CONSTRAINT `nurasymas_ibfk_1` FOREIGN KEY (`zoo_pasaras_id`) REFERENCES `zoo_pasaras` (`id`);

--
-- Constraints for table `roliu_teises`
--
ALTER TABLE `roliu_teises`
  ADD CONSTRAINT `roliu_teises_roles_id_fk` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `roliu_teises_teises_id_fk` FOREIGN KEY (`teises_id`) REFERENCES `teises` (`id`);

--
-- Constraints for table `serimas`
--
ALTER TABLE `serimas`
  ADD CONSTRAINT `serimas_ibfk_1` FOREIGN KEY (`gyvunas_id`) REFERENCES `gyvunas` (`id`),
  ADD CONSTRAINT `serimas_ibfk_2` FOREIGN KEY (`vartotojas_id`) REFERENCES `vartotojai` (`id`);

--
-- Constraints for table `tiekejo_pasaras`
--
ALTER TABLE `tiekejo_pasaras`
  ADD CONSTRAINT `tiekejo_pasaras_ibfk_1` FOREIGN KEY (`tiekejas_id`) REFERENCES `tiekejas` (`pavadinimas`);

--
-- Constraints for table `uzsakymas`
--
ALTER TABLE `uzsakymas`
  ADD CONSTRAINT `uzsakymas_ibfk_1` FOREIGN KEY (`vartotojas_id`) REFERENCES `vartotojai` (`id`);

--
-- Constraints for table `uzsakymo_pasarai`
--
ALTER TABLE `uzsakymo_pasarai`
  ADD CONSTRAINT `uzsakymo_pasarai_ibfk_1` FOREIGN KEY (`tiekejo_pasaras_id`) REFERENCES `tiekejo_pasaras` (`id`),
  ADD CONSTRAINT `uzsakymo_pasarai_ibfk_2` FOREIGN KEY (`uzsakymas_id`) REFERENCES `uzsakymas` (`id`);

--
-- Constraints for table `vartotojai`
--
ALTER TABLE `vartotojai`
  ADD CONSTRAINT `vartotojai_roles_id_fk` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `vartotojai_vartotojai_id_fk` FOREIGN KEY (`sukurta_vartotojo`) REFERENCES `vartotojai` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

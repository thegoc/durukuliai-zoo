$(document).ready(function () {
    var token = window.localStorage.getItem('token');
    var permissions = window.localStorage.getItem('permissions');
    if (token != null && permissions != null) {
        var perms = permissions.split(',');
        if (perms.findIndex(v => v == 'animal_access') == -1) {
            window.location.href = 'http://localhost';
        }
    } else {
        window.location.href = 'http://localhost';
    }
});

<?php
// meniu.php  rodomas meniu pagal vartotojo rolę

if (!isset($_SESSION)) { header("Location:include/logout.php");exit;}
include("include/nustatymai.php");
$user=$_SESSION['user'];
$userlevel=$_SESSION['ulevel'];
$role="";
{foreach($user_roles as $x=>$x_value)
			      {if ($x_value == $userlevel) $role=$x;}
} 

echo "
<div class=\"sidebar\" data-color=\"purple\">
    <div class=\"sidebar-wrapper\">
        <div class=\"logo\">
            <a class=\"simple-text\">Zoo</a>
        </div>
        <ul class=\"nav\">



            <li class=\"nav-item\">
                <a href=\"/users\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Vartotojai</p>
                </a>
            </li>
            <li class=\"nav-item active\">
                <a href=\"/animals\" class=\"nav-link\">
                    <i class=\"nc-icon nc-favourite-28\"></i>
                    <p>Gyvūnai</p>
                </a>
            </li>
            <li class=\"nav-item\">
                <a href=\"/feed\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Pašarai</p>
                </a>
            </li>
            <li class=\"nav-item\">
                <a href=\"../tickets/index.php\" class=\"nav-link\">
                    <i class=\"nc-icon nc-circle-09\"></i>
                    <p>Bilietai</p>
                </a>
            </li>
            <li class=\"nav-item\">
                <a href=\"/marketing\" class=\"nav-link\">
                    <i class=\"nc-icon nc-notification-70\"></i>
                    <p>Marketingas</p>
                </a>
            </li>
        </ul>
    </div>
</div>


<div class=\"main-panel\">
    <nav class=\"navbar navbar-expand-lg\" color-on-scroll=\"500\">
        <div class=\"container-fluid\">

            <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\">

                <ul class=\"nav navbar-nav mr-auto\">

                    <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"../animals\">
                                <span class=\"no-icon\">Gyvūnai</span>
                            </a>
                        </li>";
                echo "</ul>

            </div>
        </div>
   <div>
    <!-- End Navbar -->            
"
?>       
    
 
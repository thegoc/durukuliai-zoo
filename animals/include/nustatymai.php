﻿<?php
//nustatymai.php

define("DB_SERVER", "mysql");
define("DB_USER", "durukulis");
define("DB_PASS", "abc123");
define("DB_NAME", "durukuliai");
define("TBL_USERS", "vartotojai");

$user_roles=array(      // vartotojų rolių vardai lentelėse ir  atitinkamos userlevel reikšmės
	"Administratorius"=>"9",
	"Klientas"=>"7"
					,);   // galioja ir vartotojas "guest", kuris neturi userlevel

//define("DEFAULT_LEVEL","Studentas");  // kokia rolė priskiriama kai registruojasi
define("DEFAULT_LEVEL","Klientas");  // kokia rolė priskiriama kai registruojasi
define("ADMIN_LEVEL","Administratorius");  // kas turi vartotojų valdymo teisę
define("UZBLOKUOTAS","255");      // vartotojas negali prisijungti kol administratorius nepakeis rolės

$uregister="both";  // kaip registruojami vartotojai
// self - pats registruojasi, admin - tik ADMIN_LEVEL, both - abu atvejai

// * Email Constants - 
define("EMAIL_FROM_NAME", "Demo");
define("EMAIL_FROM_ADDR", "demo@ktu.lt");
define("EMAIL_WELCOME", false);

?>

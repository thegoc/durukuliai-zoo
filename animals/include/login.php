<?php 
// login.php - tai prisijungimo forma, index.php puslapio dalis 
// formos reikšmes tikrins proclogin.php. Esant klaidų pakartotinai rodant formą rodomos klaidos
// formos laukų reikšmės ir klaidų pranešimai grįžta per sesijos kintamuosius
// taip pat iš čia išeina priminti slaptažodžio.
// perėjimas į registraciją rodomas jei nustatyta $uregister kad galima pačiam registruotis

if (!isset($_SESSION)) { header("Location:logout.php");exit;}
$_SESSION['prev'] = "login";
include("include/nustatymai.php");
?>

<section class="container-fluid bg">
	<section class="row justify-content-center">
			<form class="form-container" action="proclogin.php" method="POST" class="login">
				<div class="form-group">
					<center style="font-size:18pt;"><b>Prisijungimas</b></center>
				        <p style="text-align:left;"><br>
					<label for="exampleInputEmail1">Vartotojo vardas</label>
					<input class="form-control" name="user" type="text" value="<?php echo $_SESSION['name_login'];  ?>"/>
					<?php echo $_SESSION['name_error']; 
					?>
			  	</div>				
			  	<div class="form-group">
					<p style="text-align:left;">
					<label for="exampleInputPassword1">Slaptažodis</label>
					<input class="form-control" name="pass" type="password" value="<?php echo $_SESSION['pass_login']; ?>"/>
					<?php echo $_SESSION['pass_error']; 
					?>
			  	</div>
				  	<button type="submit" name="login" class="btn btn-primary">Prisijungti</button>
				<p>
					<?php
						if ($uregister != "admin") { echo "<a href=\"register.php\">Registracija</a>";}
					?>
				</p>  
			</form>
	</section>
</section>

	



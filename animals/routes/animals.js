const express = require('express');
const router = express.Router({mergeParams:true});
const path = require('path');

router.get('/', (req,res) => {
    res.sendFile('index.php', { root:path.join(__dirname, '../public') });
});
router.get('/gyvunuisklotine', (req,res) => {
    res.sendFile('GyvunuIsklotine.php', {root: path.join(__dirname,'../public')});
});
router.get('/iterptigyvuna', (req,res) => {
    res.sendFile('IterptiGyvuna.php', {root: path.join(__dirname,'../public')});
});
router.get('/priskirtiprieaptvaro', (req,res) => {
    res.sendFile('PriskirtiPrieAptvaro.php', {root: path.join(__dirname,'../public')});
});
router.get('/redaguotigyvuna', (req,res) => {
    res.sendFile('RedaguotiGyvuna.php', {root: path.join(__dirname,'../public')});
});
router.get('/skiepyti', (req,res) => {
    res.sendFile('skiepyti.php', {root: path.join(__dirname,'../public')});
});

module.exports = router;

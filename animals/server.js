const express = require('express');
const app = express();
const path = require('path');
const animalRoutes = require('./routes/animals');

app.use(express.static(__dirname+'/public'));

app.use('/animals',animalRoutes );

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`server listening on port ${port}`);
});

<?php
session_start();
include("include/functions.php");
?>

<!-- public/index.php -->
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link
            rel="stylesheet"
            href="light-bootstrap-dashboard.css"
            type="text/css"
        />
        <link
            rel="stylesheet"
            href="css/light-bootstrap-dashboard.css"
            type="text/css"
        />
        <script type="text/javascript" src="js/jquery-3.4.1.js"></script>
        <title></title>
    </head>   
    
    <body>
        
        <?php
           
        if (!empty($_SESSION['user']))     //Jei vartotojas prisijungęs, valom logino kintamuosius ir rodom meniu
        {                                  // Sesijoje nustatyti kintamieji su reiksmemis is DB
                                       // $_SESSION['user'],$_SESSION['ulevel'],$_SESSION['userid'],$_SESSION['umail']
		
		inisession("part");   //   pavalom prisijungimo etapo kintamuosius
		$_SESSION['prev']="index"; 
        }
        
?>
        
        <div class=\"wrapper\">
        <?php
        include("include/meniu.php"); //įterpiamas meniu pagal vartotojo rolę
        ?>
            
            <div style="width: 90%; margin-left: 30px">
    <?php  
                    
    $dbc=mysqli_connect('mysql','durukulis','abc123','durukuliai');
    if(!$dbc){die("Negaliu prisijungti prie MySQL:" .mysqli_error($dbc));}
    $dbc->set_charset("utf8");
    $result = mysqli_query($dbc, "SELECT MAX(id) FROM gyvunas");
    $row = mysqli_fetch_array($result);
    $count = $row[0];
    
    for($x = 0; $x <= $count; $x++){
        if (isset($_POST[$x."_Priskirti"])) {
            $ID = $x;
            break;
        }
    }
    
    $sql = "SELECT*FROM aptvaras";
    $result = mysqli_query($dbc,$sql);

    echo"           <table
                    class=\"table table-bordered table-hover table-striped\"
                    id=\"AptvaruTable\"
                    >";
    
    echo" <thead>
                        <th>ID</th>
                        <th>Tipas</th>
                        <th>Talpa (m^3)</th>
                        <th></th>
                    </thead>
                    
                    <tbody>
                    ";
    while($row = mysqli_fetch_assoc($result))
    {
        echo
        "<tr><td>".$row['id']."</td><td>".$row['tipas']."</td><td>".$row['dydis_kub_m']."</td>"
                
                                ."<td>
                                <form action=\"./priskirti.php\" method=\"post\">
                                    <input
                                        type=\"submit\"
                                        name=".$ID."_".$row['id']."_PriskirtiAntras
                                        value=\"Priskirti Prie Aptvaro\"
                                        style=\"width:100%\"
                                        class=\"btn btn-primary\"
                                    />
                                </form>
                            </td>
                            </tr>";
    }
    echo "</tbody> </table>";
    ?> 
            </div>
            
        </div>
        
    </body>
</html>
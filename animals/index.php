<?php
session_start();
?>

<!-- public/index.php -->
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link
            rel="stylesheet"
            href="light-bootstrap-dashboard.css"
            type="text/css"
        />
        <link
            rel="stylesheet"
            href="css/light-bootstrap-dashboard.css"
            type="text/css"
        />
        <script type="text/javascript" src="js/jquery-3.4.1.js"></script>
        <title></title>
    </head>   
    
    <body>
        
        <div class=\"wrapper\">
        <?php
        include("include/meniu.php"); //įterpiamas meniu pagal vartotojo rolę
        ?>   
                   
        <div style="margin-left: 30px; margin-top: 0px">
            <div>
                <form action="./IterptiGyvuna.php">
                        <button class="btn btn-success">Įterpti Naują Gyvūną</button>
                </form>
                <form action="./GyvunuIsklotine.php">
                    <button class="btn btn-primary">Gyvūnų Išklotinė</button>
                </form>
            </div>
            
        </div>
            
            <div style="width: 90%; margin-left: 30px">
    <?php  
                    
    $dbc=mysqli_connect('mysql','durukulis','abc123','durukuliai');
    if(!$dbc){die("Negaliu prisijungti prie MySQL:" .mysqli_error($dbc));}
    $dbc->set_charset("utf8");

    echo"           <table
                    class=\"table table-bordered table-hover table-striped\"
                    id=\"GyvunuTable\"
                    >";
    
    echo" <thead>
                        <th>ID</th>
                        <th>Rūšis</th>
                        <th>Vardas</th>
                        <th>Gimimo Data</th>
                        <th>Skiepytas</th>
                        <th>Skiepytas Paskutinį Kartą</th>
                        <th>Šertas</th>
                        <th>Aptvaras</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    
                    <tbody>
                    ";
    
    $result = mysqli_query($dbc, "SELECT MAX(id) FROM gyvunas");
    $row = mysqli_fetch_array($result);
    $count = $row[0];
    
    for($x = 0; $x <= $count; $x++){
        $sql = "SELECT*FROM gyvunas WHERE id=".$x;
        $result = mysqli_query($dbc,$sql);
    while($row = mysqli_fetch_assoc($result))
    {
        $aptvaras1 = mysqli_query($dbc,"SELECT tipas FROM aptvaras WHERE id=".$row['aptvaro_id']);
        $aptvaras2 = mysqli_fetch_array($aptvaras1);
        $aptvaras = $aptvaras2[0];
        echo
        "<tr><td>".$row['id']."</td><td>".$row['rusis']."</td><td>".$row['vardas']."</td><td>".$row['gimimo_data']."</td><td>".$row['ar_skiepytas']."</td><td>".$row['paskutinis_skiepijimas']."</td><td>".$row['ar_sertas']."</td><td>".$aptvaras."</td><td>
                                <form action=\"./RedaguotiGyvuna.php\" method=\"post\">
                                    <input
                                        type=\"submit\"
                                        value=\"Redaguoti Gyvūną\"
                                        name=".$x."_Redaguoti
                                        style=\"width:100%\"
                                        class=\"btn btn-success\"
                                    />
                                </form>
                            </td><td>
                                <form action=\"./PriskirtiPrieAptvaro.php\" method=\"post\">
                                    <input
                                        type=\"submit\"
                                        value=\"Priskirti Prie Aptvaro\"
                                        name=".$x."_Priskirti
                                        style=\"width:100%\"
                                        class=\"btn btn-primary\"
                                    />
                                </form>
                            </td>
                            <td>
                                <form action=\"./skiepyti.php\" method=\"post\">
                                    <input
                                        type=\"submit\"
                                        value=\"Skiepyti\"
                                        name=".$x."_Skiepyti
                                        style=\"width:100%\"
                                        class=\"btn btn-warning\"
                                    />
                                </form>
                            </td></tr>";
    }
    }
    echo "</tbody> </table>";
    ?> 
            </div>
            
        </div>
    </body>
</html>

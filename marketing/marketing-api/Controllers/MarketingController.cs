﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TweetSharp;
using System.Net;
using System.Net.Mail;

namespace marketing_api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class MarketingController : ControllerBase
    {
        private readonly IRepository repo;

        public MarketingController(IRepository repository)
        {
            repo = repository;
        }

        [HttpPost]
        [Route("tweet")]
        public ActionResult<TwitterStatus> Tweet([FromQuery]string content)
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                if(string.IsNullOrEmpty(content))
                    return BadRequest(new { message = "Pranešimas tusčias."});
                if(content.Length > 280)
                    return BadRequest(new { message = "Pranešimas viršyja 280 simbolių ilgį." });

                TwitterService service = new TwitterService("a1ktYNRYIJhwpbbvCZhtUKEIS", "EKG2mGQJTOrwz7RWT6PKy7tbqArTfbQ41vpKgOsCN7IZRmlbEX");
                service.AuthenticateWith("1196166947760738311-lTpK58JRCXQBNYsWqhQw9W4FE4qjCK", "IpaAHbZJ7lTwgXHsmbMxeoJwVZP7qantHZTSzSpZjCYGJ");
                TwitterStatus result = service.SendTweet(new SendTweetOptions { Status = content });
                if (result == null)
                    return BadRequest();
                return result;
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpDelete]
        [Route("contacts/{id}")]
        public ActionResult DeleteContact([FromRoute]int id)
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                repo.DeleteContact(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }

        [HttpPost]
        [Route("contacts")]
        public ActionResult CreateContact([FromBody]Contact contact)
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                repo.CreateContact(contact);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
            
            return Ok();
        }

        [HttpPut]
        [Route("contacts")]
        public ActionResult EditPerms([FromBody]Contact contact)
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                repo.UpdateContact(contact);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }

        [HttpGet]
        [Route("ads")]
        public ActionResult<Advert[]> GetAds()
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                return repo.GetAds().ToArray();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("ads")]
        public ActionResult CreateAd ([FromBody]Advert advert)
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                repo.CreateAdvert(advert);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            
            return Ok();
        }

        [HttpGet]
        [Route("analysis/{id}")]
        public ActionResult<AdvertAnalysis> GetAdvertAnalysis([FromRoute] int id)
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                return repo.FormAdvertAnalysis(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("advertise")]
        public ActionResult<string[]> DoEmailAdvert([FromBody] EmailAdvertRequest content)
        {
            if (!CheckPermissions())
                return Unauthorized();

            string[] targetEmails;
            List<string> receivers = new List<string>();

            try
            {
                targetEmails = repo.GetEmailsWithPermissions();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }

            foreach(string targetEmail in targetEmails)
            {
                try
                {
                    var fromAddress = new MailAddress("durukuliai@gmail.com", "Durukuliai");
                    var toAddress = new MailAddress(targetEmail, "Klientas");
                    const string fromPassword = "durukuliaizoo123";
                    const string subject = "Durukuliai - Reklama";
                    string body = content.Content;

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }

                    receivers.Add(targetEmail);
                }
                catch
                {

                }

            }

            return Ok(receivers.ToArray());
        }


        [HttpGet]
        [Route("contacts")]
        public ActionResult<Contact[]> GetContacts()
        {
            if (!CheckPermissions())
                return Unauthorized();

            try
            {
                return repo.GetContacts().ToArray();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        private bool CheckPermissions()
        {
            var values = HttpContext.User.Claims.Where(c => c.Type.CompareTo("teises") == 0).Select(c => c.Value);
            return values.Contains("marketing_access");
        }
    }

    public class EmailAdvertRequest
    {
        public string Content { get; set; }
    }
}
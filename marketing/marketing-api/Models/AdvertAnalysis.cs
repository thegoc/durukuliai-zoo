﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class AdvertAnalysis
{
    public int AdvertID { get; set; }
    public string AdvertTitle { get; set; }
    public float IncreaseWeekBefore { get; set; }
    public float IncreaseWeekAfter { get; set; }
    public float IncreaseMonthBefore { get; set; }
    public float IncreaseMonthAfter { get; set; }
}
﻿using System;
public class Advert
{
    public int ID { get; set; }

    public string Title { get; set; }

    public DateTime Start { get; set; }

    public DateTime End { get; set; }

    public DateTime CreatedAt { get; set; }

    public int Price { get; set; }
}
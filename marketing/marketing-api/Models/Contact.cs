using System;

public class Contact
{
    public int ID { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Twitter { get; set; }

    public bool AllowEmail { get; set; }
    public bool AllowPhone { get; set; }
    public bool AllowTwitter { get; set; }
    public DateTime UpdatedAt { get; set; }
}
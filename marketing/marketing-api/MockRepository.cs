﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketing_api
{
    public class MockRepository : IRepository
    {
        public void CreateAdvert(Advert advert)
        {
            
        }

        public void CreateContact(Contact contact)
        {
            
        }

        public void DeleteContact(int id)
        {
            
        }

        public AdvertAnalysis FormAdvertAnalysis(int id)
        {
            return new AdvertAnalysis
            {
                AdvertID = id,
                AdvertTitle = "Test title",
                IncreaseWeekAfter = (float)new Random().NextDouble() * 2 - 1,
                IncreaseMonthAfter = (float)new Random().NextDouble() * 2 - 1,
                IncreaseMonthBefore = (float)new Random().NextDouble() * 2 - 1,
                IncreaseWeekBefore = (float)new Random().NextDouble() * 2 - 1
            };
        }

        public List<Advert> GetAds()
        {
            return new List<Advert>
            {
                new Advert
                {
                    ID = 23,
                    Title = "Reklama feisbuke",
                    Price = 5132,
                    Start = new DateTime(2007, 2, 13),
                    End = new DateTime(2008, 3, 4),
                    CreatedAt = DateTime.Now
                },
                new Advert
                {
                    ID = 56,
                    Title = "Laikrascio valscius reklama",
                    Price = 56891,
                    Start = new DateTime(2010, 2, 13),
                    End = new DateTime(2010, 2, 14),
                    CreatedAt = DateTime.Now
                },
                new Advert
                {
                    ID = 897,
                    Title = "Skrajutes",
                    Price = 123,
                    Start = new DateTime(2018, 10, 20),
                    End = new DateTime(2018, 10, 27),
                    CreatedAt = DateTime.Now
                }
            };
        }

        public List<Contact> GetContacts()
        {
            return new List<Contact> {
                new Contact
                {
                    ID = 1,
                    Email = "someemail@email.lt",
                    Phone = "4651321234",
                    Twitter = "sometwitter",
                    AllowEmail = true,
                    AllowPhone = false,
                    AllowTwitter = true
                },
                new Contact
                {
                    ID = 21,
                    Email = "mrrobers@gmail.lt",
                    Phone = "2218451315484",
                    Twitter = "robistheguy",
                    AllowEmail = false,
                    AllowPhone = false,
                    AllowTwitter = true
                },
                new Contact
                {
                    ID = 3,
                    Email = "sandrute@yahoo.lt",
                    Phone = "+3651821625",
                    Twitter = "netavoreikalas",
                    AllowEmail = true,
                    AllowPhone = false,
                    AllowTwitter = false
                }
            };
        }

        public string[] GetEmailsWithPermissions()
        {
            return new string[] { @"abendoraitis300@gmail.com", @"asfasfasf#asfasfasf", @"asfasf@asfasf.com" };
        }

        public void UpdateContact(Contact contact)
        {
            
        }
    }
}

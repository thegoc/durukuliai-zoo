﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace marketing_api
{
    public class MarketingRepository : IRepository
    {
        private readonly string conString;

        public MarketingRepository(IConfiguration configuration)
        {
            conString = configuration.GetConnectionString("Main");
        }

        public List<Contact> GetContacts()
        {
            List<Contact> result = new List<Contact>();
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT * FROM kontaktai", con);

                MySqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    result.Add(new Contact
                    {
                        ID = reader.GetInt32(reader.GetOrdinal("id")),
                        Email = reader.GetString(reader.GetOrdinal("el_pastas")),
                        Phone = reader.GetString(reader.GetOrdinal("telefonas")),
                        Twitter = reader.GetString(reader.GetOrdinal("twitter")),
                        AllowEmail = reader.GetBoolean(reader.GetOrdinal("leisti_el_pastas")),
                        AllowPhone = reader.GetBoolean(reader.GetOrdinal("leisti_telefonas")),
                        AllowTwitter = reader.GetBoolean(reader.GetOrdinal("leisti_twitter")),
                        UpdatedAt = reader.GetDateTime(reader.GetOrdinal("atnaujinta"))
                    });
                }
            }
            return result;
        }

        public void DeleteContact(int id)
        {
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand("DELETE FROM kontaktai WHERE id = @id", con);
                cmd.Parameters.AddWithValue("@id", id);
                
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateContact(Contact contact)
        {
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                string sql = "UPDATE kontaktai SET " +
                    "el_pastas = @el_pastas, " +
                    "telefonas = @telefonas, " +
                    "twitter = @twitter, " +
                    "leisti_el_pastas = @leisti_el_pastas, " +
                    "leisti_telefonas = @leisti_telefonas, " +
                    "leisti_twitter = @leisti_twitter " +
                    "WHERE id = @id";

                MySqlCommand cmd = new MySqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@id", contact.ID);
                cmd.Parameters.AddWithValue("@el_pastas", contact.Email);
                cmd.Parameters.AddWithValue("@telefonas", contact.Phone);
                cmd.Parameters.AddWithValue("@twitter", contact.Twitter);
                cmd.Parameters.AddWithValue("@leisti_el_pastas", contact.AllowEmail);
                cmd.Parameters.AddWithValue("@leisti_telefonas", contact.AllowPhone);
                cmd.Parameters.AddWithValue("@leisti_twitter", contact.AllowTwitter);

                cmd.ExecuteNonQuery();
            }
        }

        public void CreateContact(Contact contact)
        {
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                string sql = "INSERT INTO kontaktai " +
                    "(el_pastas, telefonas, twitter, leisti_el_pastas, leisti_telefonas, leisti_twitter) " +
                    "VALUES(@el_pastas, @telefonas, @twitter, @leisti_el_pastas, @leisti_telefonas, @leisti_twitter)";

                MySqlCommand cmd = new MySqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@el_pastas", contact.Email);
                cmd.Parameters.AddWithValue("@telefonas", contact.Phone);
                cmd.Parameters.AddWithValue("@twitter", contact.Twitter);
                cmd.Parameters.AddWithValue("@leisti_el_pastas", contact.AllowEmail);
                cmd.Parameters.AddWithValue("@leisti_telefonas", contact.AllowPhone);
                cmd.Parameters.AddWithValue("@leisti_twitter", contact.AllowTwitter);

                cmd.ExecuteNonQuery();
            }
        }

        public List<Advert> GetAds()
        {
            List<Advert> result = new List<Advert>();
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT * FROM reklamos", con);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new Advert
                    {
                        ID = reader.GetInt32(reader.GetOrdinal("id")),
                        Title = reader.GetString(reader.GetOrdinal("pavadinimas")),
                        Start = reader.GetDateTime(reader.GetOrdinal("pradzia")),
                        End = reader.GetDateTime(reader.GetOrdinal("pabaiga")),
                        CreatedAt = reader.GetDateTime(reader.GetOrdinal("sukurimo_data")),
                        Price = reader.GetInt32(reader.GetOrdinal("kaina"))
                    });
                }
            }
            return result;
        }

        public void CreateAdvert(Advert advert)
        {
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                string sql = "INSERT INTO reklamos " +
                    "(pavadinimas, pradzia, pabaiga, kaina) " +
                    "VALUES(@pavadinimas, @pradzia, @pabaiga, @kaina)";

                MySqlCommand cmd = new MySqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@pavadinimas", advert.Title);
                cmd.Parameters.AddWithValue("@pradzia", advert.Start);
                cmd.Parameters.AddWithValue("@pabaiga", advert.End);
                cmd.Parameters.AddWithValue("@kaina", advert.Price);

                cmd.ExecuteNonQuery();
            }
        }

        public AdvertAnalysis FormAdvertAnalysis(int id)
        {
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                // Get advert
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM reklamos WHERE id = @id", con);
                cmd.Parameters.AddWithValue("@id", id);

                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                Advert advert = new Advert
                {
                    ID = id,
                    Title = reader.GetString(reader.GetOrdinal("pavadinimas")),
                    Start = reader.GetDateTime(reader.GetOrdinal("pradzia")).Date,
                    End = reader.GetDateTime(reader.GetOrdinal("pabaiga")).Date,
                    CreatedAt = reader.GetDateTime(reader.GetOrdinal("sukurimo_data")),
                    Price = reader.GetInt32(reader.GetOrdinal("kaina"))
                };

                reader.Close();

                DateTime monthBefore = advert.Start.AddMonths(-1);
                DateTime weekBefore = advert.Start.AddDays(-7);
                DateTime weekAfter = advert.End.AddDays(7);
                DateTime monthAfter = advert.End.AddMonths(1);

                // Get tickets
                List<Tuple<DateTime, int>> tickets = new List<Tuple<DateTime, int>>();

                cmd = new MySqlCommand("SELECT kiekis, data FROM parduoti_bilietai " +
                    "WHERE data > @from AND data < @to", con);
                cmd.Parameters.AddWithValue("@from", monthBefore);
                cmd.Parameters.AddWithValue("@to", monthAfter);

                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    tickets.Add(new Tuple<DateTime, int>(
                        reader.GetDateTime(reader.GetOrdinal("data")).Date,
                        reader.GetInt32(reader.GetOrdinal("kiekis"))
                    ));    
                }


                int duringTickets = tickets.Where(t => t.Item1 >= advert.Start && t.Item1 <= advert.End).Sum(t => t.Item2);
                int monthBeforeTickets = tickets.Where(t => t.Item1 > monthBefore && t.Item1 < advert.Start).Sum(t => t.Item2);
                int weekBeforeTickets = tickets.Where(t => t.Item1 > weekBefore && t.Item1 < advert.Start).Sum(t => t.Item2);
                int weekAfterTickets = tickets.Where(t => t.Item1 > advert.End && t.Item1 < weekAfter).Sum(t => t.Item2);
                int monthAfterTickets = tickets.Where(t => t.Item1 > advert.End && t.Item1 < monthAfter).Sum(t => t.Item2);

                AdvertAnalysis analysis = new AdvertAnalysis
                {
                    AdvertID = advert.ID,
                    AdvertTitle = advert.Title,
                    IncreaseMonthBefore = monthBeforeTickets == 0 ? 0 : (float) (duringTickets) / monthBeforeTickets - 1,
                    IncreaseWeekBefore = weekBeforeTickets == 0 ? 0 : (float)(duringTickets) / weekBeforeTickets - 1,
                    IncreaseWeekAfter = weekAfterTickets == 0 ? 0 : (float)(duringTickets) / weekAfterTickets - 1,
                    IncreaseMonthAfter = monthAfterTickets == 0 ? 0 : (float)(duringTickets) / monthAfterTickets - 1
                };

                return analysis;
            }
        }

        public string[] GetEmailsWithPermissions()
        {
            List<string> result = new List<string>();
            using (MySqlConnection con = new MySqlConnection(conString))
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand("SELECT el_pastas FROM kontaktai WHERE leisti_el_pastas = 1", con);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(reader.GetString(reader.GetOrdinal("el_pastas")));
                }
            }
            return result.ToArray();
        }
    }
}

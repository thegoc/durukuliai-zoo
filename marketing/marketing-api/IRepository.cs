﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketing_api
{
    public interface IRepository
    {
        void DeleteContact(int id);
        void CreateContact(Contact contact);
        void UpdateContact(Contact contact);
        List<Contact> GetContacts();
        List<Advert> GetAds();
        void CreateAdvert(Advert advert);

        AdvertAnalysis FormAdvertAnalysis(int id);
        string[] GetEmailsWithPermissions();
    }
}

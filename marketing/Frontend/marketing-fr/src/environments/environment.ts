// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "https://localhost:44316/api/marketing/",
  debugJwtKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MCwiZWxfcGFzdGFzIjoibWFya0BtYXJrZXQiLCJ2YXJkYXMiOiJNaWtlIiwicGF2YXJkZSI6IkJvYmxlciIsImFyX2FkbWluaXN0cmF0b3JpdXMiOmZhbHNlLCJzdWt1cmltb19kYXRhIjoiMjAxOS0xMi0wOFQxOToyNDowNloiLCJpc3RyeW5pbW9fZGF0YSI6bnVsbCwic3VrdXJ0YV92YXJ0b3Rvam8iOjIsInRlaXNlcyI6WyJlZGl0X3R3aXR0ZXIiLCJ2aWV3X2N1cnJlbnRfdXNlcl9pbmZvIiwibWFya2V0aW5nX2FjY2VzcyJdLCJyb2xlc19pZCI6Mywicm9sZSI6Ik1hcmtldGluZ28gdmFkeWJpbmlua2FzIiwiZXhwIjoxNTc2MTAyODE3fQ.wvrEFRVsvmexND6iNjxgj06piVYjLdjFJKK9yWAaj1c"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

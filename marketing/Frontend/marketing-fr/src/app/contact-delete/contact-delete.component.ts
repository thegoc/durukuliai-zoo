import { Component, OnInit, OnDestroy } from '@angular/core';
import { Contact } from '../contact-list/contact';
import { ApiService } from '../api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contact-delete',
  templateUrl: './contact-delete.component.html',
  styleUrls: ['./contact-delete.component.css']
})
export class ContactDeleteComponent implements OnInit, OnDestroy {
  private paramsSub: Subscription = Subscription.EMPTY;

  contact: Contact = new Contact({ email: '', twitter: '', phone: '' });
  errorMessage: string = '';
  deleting: boolean;

  constructor(private api: ApiService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.paramsSub = this.route.params.subscribe(params => {
      this.contact = (<Contact[]>this.route.snapshot.data[0]).find(c => c.id == params['id']);
    })
  }

  confirmDelete() {
    this.deleting = true;
    this.api.deleteContact(this.contact.id).subscribe(
      (result) => {
        this.router.navigate(['contacts']);
        this.deleting = false;
      },
      (error) => {
        if (error['error'] != null && error['error']['message'] != null) {
          this.errorMessage = error['error']['message'];
        } else {
          this.errorMessage = "Įvyko klaida."
        }

        this.deleting = false;
      }
    )
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }
}

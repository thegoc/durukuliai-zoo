import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactEditRightsComponent } from './contact-edit-rights.component';

describe('ContactEditRightsComponent', () => {
  let component: ContactEditRightsComponent;
  let fixture: ComponentFixture<ContactEditRightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactEditRightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactEditRightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

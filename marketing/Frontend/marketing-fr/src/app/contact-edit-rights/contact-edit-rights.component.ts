import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { Contact } from '../contact-list/contact';

@Component({
  selector: 'app-contact-edit-rights',
  templateUrl: './contact-edit-rights.component.html',
  styleUrls: ['./contact-edit-rights.component.css']
})
export class ContactEditRightsComponent implements OnInit {
  private paramsSub: Subscription = Subscription.EMPTY;

  submitting: boolean = false;
  errorMessage: string = '';
  successMessage: string = '';

  form: FormGroup = new FormGroup({
    'email-allowed': new FormControl(false),
    'phone-allowed': new FormControl(false),
    'twitter-allowed': new FormControl(false)
  });

  contact: Contact = new Contact();

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.paramsSub = this.route.params.subscribe(params => {
      this.contact = (<Contact[]>this.route.snapshot.data[0]).find(c => c.id == params['id']);

      this.form = new FormGroup({
        'email-allowed': new FormControl(this.contact.allow_email),
        'phone-allowed': new FormControl(this.contact.allow_phone),
        'twitter-allowed': new FormControl(this.contact.allow_twitter)
      });
    })
  }

  onSubmit() {
    this.errorMessage = '';
    this.successMessage = '';
    const contact = this.createFromValues();

    this.submitting = true;
    this.api.updateContactPerms(contact).subscribe(
      (result) => {
        this.successMessage = 'Kontaktas atnaujintas';
        this.submitting = false;
      },
      (error) => {
        if (error['error'] != null && error['error']['message'] != null) {
          this.errorMessage = error['error']['message'];
        } else {
          this.errorMessage = "Įvyko klaida."
        }

        this.submitting = false;
      }
    )
  }

  createFromValues(): Contact {
    return new Contact({
      id: this.contact.id,
      email: this.contact.email,
      phone: this.contact.phone,
      twitter: this.contact.twitter,
      allow_email: this.form.get('email-allowed').value,
      allow_phone: this.form.get('phone-allowed').value,
      allow_twitter: this.form.get('twitter-allowed').value
    })
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }

}

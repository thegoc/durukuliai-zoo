import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Contact } from '../contact-list/contact';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.css']
})
export class NewContactComponent implements OnInit {

  submitting: boolean = false;
  errorMessage: string = '';
  successMessage: string = '';

  form: FormGroup = new FormGroup({
    'email': new FormControl(''),
    'phone': new FormControl(''),
    'twitter': new FormControl(''),
    'email-allowed': new FormControl(false),
    'phone-allowed': new FormControl(false),
    'twitter-allowed': new FormControl(false)
  });
  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.errorMessage = '';
    this.successMessage = '';
    const contact = this.createFromValues();

    if (contact.email == '' && contact.phone == '' && contact.twitter == '') {
      this.errorMessage = 'Turi būti nurodytas bent vienas kontakto tipas';
      return;
    }

    this.submitting = true;
    this.api.createContact(contact).subscribe(
      (result) => {
        this.successMessage = 'Kontaktas sukurtas sėkmingai!';
        this.resetForm();
        this.submitting = false;
      },
      (error) => {
        if (error['error'] != null && error['error']['message'] != null) {
          this.errorMessage = error['error']['message'];
        } else {
          this.errorMessage = "Įvyko klaida."
        }

        this.submitting = false;
      }
    )
  }

  resetForm() {
    this.form.get('email').setValue('');
    this.form.get('phone').setValue('');
    this.form.get('twitter').setValue('');
    this.form.get('email-allowed').setValue(false);
    this.form.get('phone-allowed').setValue(false);
    this.form.get('twitter-allowed').setValue(false);
  }

  createFromValues(): Contact {
    return new Contact({
      email: this.form.get('email').value,
      phone: this.form.get('phone').value,
      twitter: this.form.get('twitter').value,
      allow_email: this.form.get('email-allowed').value,
      allow_phone: this.form.get('phone-allowed').value,
      allow_twitter: this.form.get('twitter-allowed').value
    })
  }
}

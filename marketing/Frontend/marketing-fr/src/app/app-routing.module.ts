import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactListComponent } from './contact-list/contact-list.component';
import { TweetComponent } from './tweet/tweet.component';
import { AdListComponent } from './ad-list/ad-list.component';
import { EmailAdComponent } from './email-ad/email-ad.component';
import { HomeComponent } from './home/home.component';
import { NewContactComponent } from './new-contact/new-contact.component';
import { ContactDeleteComponent } from './contact-delete/contact-delete.component';
import { ContactEditRightsComponent } from './contact-edit-rights/contact-edit-rights.component';
import { AdsCreateComponent } from './ads-create/ads-create.component';
import { AuthGuard } from './auth.guard';
import { ContactsResolver } from './contact-list/contacts.resolver';
import { AdvertsResolver } from './ad-list/advert.resolver';
import { AdAnalysisComponent } from './ad-analysis/ad-analysis.component';
import { AdvertAnalysisResolver } from './ad-analysis/advert-analysis.resolver';


const routes: Routes = [
  { path: "", component: HomeComponent },
  {
    path: "contacts", canActivate: [AuthGuard], resolve: [ContactsResolver], runGuardsAndResolvers: "always", children: [
      { path: "", component: ContactListComponent },
      { path: "create", component: NewContactComponent },
      { path: "delete/:id", component: ContactDeleteComponent },
      { path: "edit/:id", component: ContactEditRightsComponent }
    ]
  },
  { path: "tweet", canActivate: [AuthGuard], component: TweetComponent },
  {
    path: "ads", canActivate: [AuthGuard], resolve: [AdvertsResolver], runGuardsAndResolvers: "always", children: [
      { path: "", component: AdListComponent },
      { path: "create", component: AdsCreateComponent },
      { path: "analysis/:id", resolve: [AdvertAnalysisResolver], runGuardsAndResolvers: "always", component: AdAnalysisComponent },
    ]
  },
  { path: "email-ad", canActivate: [AuthGuard], component: EmailAdComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

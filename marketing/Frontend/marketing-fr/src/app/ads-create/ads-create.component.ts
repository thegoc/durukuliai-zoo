import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { Advert } from '../ad-list/advert';

@Component({
  selector: 'app-ads-create',
  templateUrl: './ads-create.component.html',
  styleUrls: ['./ads-create.component.css']
})
export class AdsCreateComponent implements OnInit {

  submitting: boolean = false;
  errorMessage: string = '';

  form: FormGroup = new FormGroup({
    'title': new FormControl(''),
    'start': new FormControl(Date.now()),
    'end': new FormControl(Date.now()),
    'price': new FormControl(0)
  });
  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.errorMessage = '';
    const advert = this.createFromValues();

    if (advert.price < 0) {
      this.errorMessage = 'Kaina turi būti teigiamas skaičius';
      return;
    }

    if (advert.title.length < 3) {
      this.errorMessage = 'Pavadinimas turi būti bent 3 simbolių ilgio';
      return;
    }

    this.submitting = true;
    this.api.createAdvert(advert).subscribe(
      (result) => {
        this.resetForm();
        this.submitting = false;
        this.router.navigate(['/','ads']);
      },
      (error) => {
        if (error['error'] != null && error['error']['message'] != null) {
          this.errorMessage = error['error']['message'];
        } else if(error['error'].status == 400) {
          this.errorMessage = "Neteisinga užklausa. Įsitikinkite, kad abi datos nurodytos teisingai";
        }
        else {
          this.errorMessage = "Įvyko klaida."
        }

        this.submitting = false;
      }
    )
  }

  resetForm() {
    this.form.get('title').setValue('');
    this.form.get('start').setValue(Date.now());
    this.form.get('end').setValue(Date.now());
    this.form.get('price').setValue(0);
  }

  createFromValues(): Advert {
    return new Advert({
      title: this.form.get('title').value,
      start: this.form.get('start').value,
      end: this.form.get('end').value,
      price: Math.round(this.form.get('price').value * 100)
    })
  }
}

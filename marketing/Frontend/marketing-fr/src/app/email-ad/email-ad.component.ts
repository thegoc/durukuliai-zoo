import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-email-ad',
  templateUrl: './email-ad.component.html',
  styleUrls: ['./email-ad.component.css']
})
export class EmailAdComponent implements OnInit {

  successMessage: string = null;
  errorMessage: string = null;

  submitting: boolean = false;

  content: string ="";

  receivers: string[] = null;

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  submit() {
    this.successMessage = null;
    this.errorMessage = null;
    this.receivers = null;

    if(this.content.length == 0) {
      this.errorMessage = "Reklamos turinys negali būti tusčias";
      return;
    }

    this.submitting = true

    this.api.sendEmailAd(this.content).subscribe(
      (result) => {
        this.receivers = result;
        this.successMessage = "Reklama sėkmingai išsiųsta!";
        this.submitting = false;
      },
      (error) => {
        if(error['error'] != null && error['error']['message'] != null) {
          this.errorMessage = error['error']['message'];
        } else {
          this.errorMessage = "Įvyko klaida. Reklamos išsiųsti nepavyko."
        }

        this.submitting = false;
      }
    )

  }

  closeError() {
    this.errorMessage = null;
  }

  closeSuccess() {
    this.successMessage = null;
  }

}

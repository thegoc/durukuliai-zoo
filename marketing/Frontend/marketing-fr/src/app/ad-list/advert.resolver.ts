import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from '../api.service';
import { Advert } from './advert';

@Injectable()
export class AdvertsResolver implements Resolve<Advert[]> {
    constructor(private api: ApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.api.getAdverts();
    }
}

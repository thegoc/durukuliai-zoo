import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Advert } from './advert';
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.css']
})
export class AdListComponent implements OnInit, OnDestroy {

  private dataSub: Subscription = Subscription.EMPTY;

  adverts: Advert[] = [];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.dataSub = this.route.data.subscribe(d => {
      this.adverts = <Advert[]>this.route.snapshot.data[0];
    });
  }

  ngOnDestroy() {
    this.dataSub.unsubscribe();
  }

  date(value) {
    return formatDate(value, 'yyyy/MM/dd', 'en-us');
  }
}

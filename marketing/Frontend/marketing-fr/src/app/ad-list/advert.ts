export class Advert {
  id: number;
  title: string;
  start: Date;
  end: Date;
  createdAt: Date;
  price: number;

  public constructor(init?: Partial<Advert>) {
    Object.assign(this, init);
}
}

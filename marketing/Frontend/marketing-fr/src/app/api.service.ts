import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { environment } from "./../environments/environment";
import { Observable } from 'rxjs';
import { Contact } from './contact-list/contact';
import { map, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";
import { Advert } from "./ad-list/advert";
import { AdvertAnalysis } from "./ad-analysis/advert-analysis";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient, private router: Router) { }

  tweet(content: string) {
    return this.http.post(environment.apiUrl + 'tweet?content=' + content, null);
  }

  sendEmailAd(content: string) {
    return this.http.post<string[]>(environment.apiUrl + 'advertise', {content});
  }

  getContacts(): Observable<Contact[]> {
    return this.http.get<object[]>(environment.apiUrl + 'contacts').pipe(
      map(result => result.map<Contact>(entry => new Contact({
        id: entry['id'],
        email: entry['email'],
        phone: entry['phone'],
        twitter: entry['twitter'],
        allow_email: entry['allowEmail'],
        allow_phone: entry['allowPhone'],
        allow_twitter: entry['allowTwitter'],
        updatedAt: entry['updatedAt']
      })
      ))
    );
  }

  getAdverts(): Observable<Advert[]> {
    return this.http.get<Advert[]>(environment.apiUrl + 'ads');
  }

  createAdvert(advert: Advert) {
    return this.http.post(environment.apiUrl + 'ads', advert);
  }

  getAdvertAnalysis(id: number): Observable<AdvertAnalysis> {
    return this.http.get<AdvertAnalysis>(environment.apiUrl + "analysis/" + id);
  }

  createContact(contact: Contact) {
    const body = {
      email: contact.email,
      phone: contact.phone,
      twitter: contact.twitter,
      allowEmail: contact.allow_email,
      allowPhone: contact.allow_phone,
      allowTwitter: contact.allow_email
    }
    return this.http.post(environment.apiUrl + 'contacts', body);
  }

  updateContactPerms(contact: Contact) {
    const body = {
      id: contact.id,
      email: contact.email,
      phone: contact.phone,
      twitter: contact.twitter,
      allowEmail: contact.allow_email,
      allowPhone: contact.allow_phone,
      allowTwitter: contact.allow_email
    }
    return this.http.put(environment.apiUrl + 'contacts', body);
  }

  deleteContact(id: number) {
    return this.http.delete(environment.apiUrl + 'contacts/' + id).pipe(
      catchError((error:HttpErrorResponse) => {
        if(error.status == 401) {
          document.location.href = 'http://localhost';
        }
        throw error;
      })
    );
  }
}

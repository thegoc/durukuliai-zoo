import { Component, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeLt from '@angular/common/locales/lt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'marketing-fr';

  ngOnInit() {

  }
}

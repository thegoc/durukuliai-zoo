export class AdvertAnalysis {
  advertId: number;
  advertTitle: string;
  increaseWeekAfter: number;
  increaseMonthAfter: number;
  increaseMonthBefore: number;
  increaseWeekBefore: number;
}

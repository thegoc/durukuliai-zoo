import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Advert } from '../ad-list/advert';
import { ActivatedRoute } from '@angular/router';
import { AdvertAnalysis } from './advert-analysis';

@Component({
  selector: 'app-ad-analysis',
  templateUrl: './ad-analysis.component.html',
  styleUrls: ['./ad-analysis.component.css']
})
export class AdAnalysisComponent implements OnInit, OnDestroy {

  private dataSub: Subscription = Subscription.EMPTY;

  advert: AdvertAnalysis;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.dataSub = this.route.data.subscribe(d => {
      this.advert = <AdvertAnalysis>this.route.snapshot.data[0];
    });
  }

  ngOnDestroy() {
    this.dataSub.unsubscribe();
  }
}

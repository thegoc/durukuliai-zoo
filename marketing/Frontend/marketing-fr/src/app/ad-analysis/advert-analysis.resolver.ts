import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from '../api.service';
import { AdvertAnalysis } from './advert-analysis';

@Injectable()
export class AdvertAnalysisResolver implements Resolve<AdvertAnalysis> {
    constructor(private api: ApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.api.getAdvertAnalysis(route.params['id']);
    }
}

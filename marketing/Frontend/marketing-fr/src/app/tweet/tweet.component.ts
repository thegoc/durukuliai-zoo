import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css']
})
export class TweetComponent implements OnInit {

  successMessage: string = null;
  errorMessage: string = null;

  tweeting: boolean = false;

  content: string ="";

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  submit() {
    this.successMessage = null;
    this.errorMessage = null;
    this.tweeting = true

    this.api.tweet(this.content).subscribe(
      (result) => {
        this.successMessage = "Įrašas sėkmingai paskelbtas!";
        this.tweeting = false;
      },
      (error) => {
        if(error['error'] != null && error['error']['message'] != null) {
          this.errorMessage = error['error']['message'];
        } else {
          this.errorMessage = "Įvyko klaida. Įrašo paskelbti nepavyko. Įsitikinkite, kad neskelbiate to paties antrą kartą."
        }

        this.tweeting = false;
      }
    )

  }

  closeError() {
    this.errorMessage = null;
  }

  closeSuccess() {
    this.successMessage = null;
  }

}

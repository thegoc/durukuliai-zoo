import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Contact } from './contact';
import { Subscription } from 'rxjs';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit, OnDestroy {
  private dataSub: Subscription = Subscription.EMPTY;

  contacts: Contact[] = [];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.dataSub = this.route.data.subscribe(d => {
      this.contacts = <Contact[]>this.route.snapshot.data[0];
    });
  }

  ngOnDestroy() {
    this.dataSub.unsubscribe();
  }

  date(value) {
    return formatDate(value, 'yyyy/MM/dd', 'en-us');
  }
}

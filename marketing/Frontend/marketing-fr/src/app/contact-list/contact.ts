export class Contact {
    id: number;
    email: string;
    twitter: string;
    phone: string;
    allow_email: boolean;
    allow_twitter: boolean;
    allow_phone: boolean;
    updatedAt: Date;

    public constructor(init?: Partial<Contact>) {
        Object.assign(this, init);
    }
}
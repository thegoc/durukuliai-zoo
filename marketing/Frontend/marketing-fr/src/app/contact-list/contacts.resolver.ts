import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { map, filter, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Contact } from './contact';
import { ApiService } from '../api.service';

@Injectable()
export class ContactsResolver implements Resolve<Contact[]> {
    constructor(private api: ApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.api.getContacts();
    }
}
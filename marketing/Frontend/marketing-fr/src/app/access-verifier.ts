import { environment } from "../environments/environment";

export function VerifyAccess() : boolean {
  // Return true if we are in a development environment
  if(!environment.production)
    return true;

  const token = localStorage.getItem('token');
      const permissions = localStorage.getItem('permissions');
      if(token != null && permissions != null) {
        var perms = permissions.split(',');
        if(perms.findIndex(v => v == 'marketing_access') != -1)
          return true;
      }

      return false;
}

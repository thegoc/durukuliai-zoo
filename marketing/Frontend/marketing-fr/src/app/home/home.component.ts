import { Component, OnInit } from '@angular/core';
import { VerifyAccess } from '../access-verifier';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  errorMessage: string = null;
  constructor() { }

  ngOnInit() {
    this.errorMessage = VerifyAccess() ? null : 'Jūs neturite prieigos prie šios posistemės!';
    console.log("Production: " + environment.production);
  }

}

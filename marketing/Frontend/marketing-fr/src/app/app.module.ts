import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { TweetComponent } from './tweet/tweet.component';
import { AdListComponent } from './ad-list/ad-list.component';
import { EmailAdComponent } from './email-ad/email-ad.component';
import { HomeComponent } from './home/home.component';
import { NewContactComponent } from './new-contact/new-contact.component';
import { ContactEditRightsComponent } from './contact-edit-rights/contact-edit-rights.component';
import { ContactDeleteComponent } from './contact-delete/contact-delete.component';
import { AdsCreateComponent } from './ads-create/ads-create.component';
import { AdsDeleteComponent } from './ads-delete/ads-delete.component';
import { ApiService } from './api.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from './auth.guard';
import { RightsGuard } from './rights.guard';
import { ContactsResolver } from './contact-list/contacts.resolver';
import { ErrorAlertComponent } from './error-alert/error-alert.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { AuthInterceptor } from './auth.interceptor';
import { UnAuthInterceptor } from './unauth.interceptor';
import { AdvertsResolver } from './ad-list/advert.resolver';
import { AdAnalysisComponent } from './ad-analysis/ad-analysis.component';
import { AdvertAnalysisResolver } from './ad-analysis/advert-analysis.resolver';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ContactListComponent,
    TweetComponent,
    AdListComponent,
    EmailAdComponent,
    HomeComponent,
    NewContactComponent,
    ContactEditRightsComponent,
    ContactDeleteComponent,
    AdsCreateComponent,
    AdsDeleteComponent,
    ErrorAlertComponent,
    SuccessAlertComponent,
    AdAnalysisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ApiService,
    AuthGuard,
    RightsGuard,
    ContactsResolver,
    AdvertsResolver,
    AdvertAnalysisResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

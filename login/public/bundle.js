
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.head.appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function prevent_default(fn) {
        return function (event) {
            event.preventDefault();
            // @ts-ignore
            return fn.call(this, event);
        };
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        if (value != null || input.value) {
            input.value = value;
        }
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    function flush() {
        const seen_callbacks = new Set();
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    callback();
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update($$.dirty);
            run_all($$.before_update);
            $$.fragment && $$.fragment.p($$.dirty, $$.ctx);
            $$.dirty = null;
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = {};
        }
    }
    function make_dirty(component, key) {
        if (!component.$$.dirty) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty = blank_object();
        }
        component.$$.dirty[key] = true;
    }
    function init(component, options, instance, create_fragment, not_equal, props) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty: null
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (key, ret, value = ret) => {
                if ($$.ctx && not_equal($$.ctx[key], $$.ctx[key] = value)) {
                    if ($$.bound[key])
                        $$.bound[key](value);
                    if (ready)
                        make_dirty(component, key);
                }
                return ret;
            })
            : prop_values;
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(children(options.target));
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, detail));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function prop_dev(node, property, value) {
        node[property] = value;
        dispatch_dev("SvelteDOMSetProperty", { node, property, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.data === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
    }

    function POST(url, body, authorization = true, formData = false) {
        return request("POST", url, body, authorization, formData)
    }


    // --------------NOT EXPORTED------------------
    function request(method, url, body, authorization, formData) {
        let headers = {"Content-Type": "application/json"};
        if (authorization) {
            headers["Authorization"] = "Bearer " + localStorage.getItem("token");
        }
        let reqBody = {
            method: method,
            headers: headers
        };
        if (method === "POST" || method === "PUT" || method === "PATCH") {
            if (formData) {
                reqBody.body = body;
                delete reqBody.headers["Content-Type"];
            } else {
                reqBody.body = JSON.stringify(body);
            }
        }

        let error = null;
        return fetch('http://localhost:1323' + url, reqBody).then(res => {
            if(!res.ok) {
                error = {
                    status:     res.status,
                    statusText: res.statusText,
                };
            }
            return res.text();
        }).then(data => {
            if (error != null) {
                error.body = data ? JSON.parse(data) : {};
                return {
                    status: false,
                    data: error
                };
            }
            return {
                status: true,
                data: data ? JSON.parse(data) : {}
            };
        });
    }

    /* src/Main.svelte generated by Svelte v3.15.0 */
    const file = "src/Main.svelte";

    // (43:2) {#if error != null}
    function create_if_block_1(ctx) {
    	let div;
    	let span;
    	let t1;
    	let strong;
    	let t3;
    	let t4;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			span = element("span");
    			span.textContent = "×";
    			t1 = space();
    			strong = element("strong");
    			strong.textContent = "Klaida!";
    			t3 = space();
    			t4 = text(ctx.error);
    			attr_dev(span, "class", "closebtn");
    			add_location(span, file, 44, 4, 1072);
    			add_location(strong, file, 45, 4, 1144);
    			attr_dev(div, "class", "alert bg-danger");
    			add_location(div, file, 43, 3, 1038);
    			dispose = listen_dev(span, "click", ctx.click_handler, false, false, false);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, span);
    			append_dev(div, t1);
    			append_dev(div, strong);
    			append_dev(div, t3);
    			append_dev(div, t4);
    		},
    		p: function update(changed, ctx) {
    			if (changed.error) set_data_dev(t4, ctx.error);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(43:2) {#if error != null}",
    		ctx
    	});

    	return block;
    }

    // (72:8) {:else}
    function create_else_block(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Prisijungti");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(72:8) {:else}",
    		ctx
    	});

    	return block;
    }

    // (70:8) {#if loading}
    function create_if_block(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Kraunama...");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(70:8) {#if loading}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let div10;
    	let div9;
    	let t0;
    	let div8;
    	let div7;
    	let form;
    	let div6;
    	let div0;
    	let h3;
    	let t2;
    	let div4;
    	let div3;
    	let div1;
    	let label0;
    	let t4;
    	let input0;
    	let t5;
    	let div2;
    	let label1;
    	let t7;
    	let input1;
    	let t8;
    	let div5;
    	let button;
    	let button_disabled_value;
    	let dispose;
    	let if_block0 = ctx.error != null && create_if_block_1(ctx);

    	function select_block_type(changed, ctx) {
    		if (ctx.loading) return create_if_block;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(null, ctx);
    	let if_block1 = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div10 = element("div");
    			div9 = element("div");
    			if (if_block0) if_block0.c();
    			t0 = space();
    			div8 = element("div");
    			div7 = element("div");
    			form = element("form");
    			div6 = element("div");
    			div0 = element("div");
    			h3 = element("h3");
    			h3.textContent = "Prisijungimas";
    			t2 = space();
    			div4 = element("div");
    			div3 = element("div");
    			div1 = element("div");
    			label0 = element("label");
    			label0.textContent = "El. pašto adresas";
    			t4 = space();
    			input0 = element("input");
    			t5 = space();
    			div2 = element("div");
    			label1 = element("label");
    			label1.textContent = "Slaptažodis";
    			t7 = space();
    			input1 = element("input");
    			t8 = space();
    			div5 = element("div");
    			button = element("button");
    			if_block1.c();
    			attr_dev(h3, "class", "header text-center");
    			add_location(h3, file, 53, 7, 1413);
    			attr_dev(div0, "class", "card-header ");
    			add_location(div0, file, 52, 6, 1379);
    			add_location(label0, file, 58, 9, 1580);
    			attr_dev(input0, "type", "email");
    			attr_dev(input0, "placeholder", "El. paštas");
    			attr_dev(input0, "class", "form-control");
    			add_location(input0, file, 59, 9, 1622);
    			attr_dev(div1, "class", "form-group");
    			add_location(div1, file, 57, 8, 1546);
    			add_location(label1, file, 62, 9, 1770);
    			attr_dev(input1, "type", "password");
    			attr_dev(input1, "placeholder", "Slaptažodis");
    			attr_dev(input1, "class", "form-control");
    			add_location(input1, file, 63, 9, 1806);
    			attr_dev(div2, "class", "form-group mb-0");
    			add_location(div2, file, 61, 8, 1731);
    			attr_dev(div3, "class", "card-body");
    			add_location(div3, file, 56, 7, 1514);
    			attr_dev(div4, "class", "card-body ");
    			add_location(div4, file, 55, 6, 1482);
    			attr_dev(button, "type", "submit");
    			attr_dev(button, "class", "btn btn-primary");
    			button.disabled = button_disabled_value = ctx.password === "" || ctx.email === "";
    			add_location(button, file, 68, 7, 2001);
    			attr_dev(div5, "class", "card-footer ml-auto mr-auto mb-2");
    			add_location(div5, file, 67, 6, 1947);
    			attr_dev(div6, "class", "card card-login");
    			add_location(div6, file, 51, 5, 1343);
    			attr_dev(form, "class", "form");
    			add_location(form, file, 50, 4, 1283);
    			attr_dev(div7, "class", "col-md-4 col-sm-6 ml-auto mr-auto");
    			add_location(div7, file, 49, 3, 1231);
    			attr_dev(div8, "class", "container mt-5");
    			add_location(div8, file, 48, 2, 1199);
    			attr_dev(div9, "class", "content");
    			add_location(div9, file, 41, 1, 991);
    			attr_dev(div10, "class", "full-page section-image");
    			add_location(div10, file, 40, 0, 952);

    			dispose = [
    				listen_dev(input0, "input", ctx.input0_input_handler),
    				listen_dev(input1, "input", ctx.input1_input_handler),
    				listen_dev(form, "submit", prevent_default(ctx.login), false, false, true)
    			];
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div10, anchor);
    			append_dev(div10, div9);
    			if (if_block0) if_block0.m(div9, null);
    			append_dev(div9, t0);
    			append_dev(div9, div8);
    			append_dev(div8, div7);
    			append_dev(div7, form);
    			append_dev(form, div6);
    			append_dev(div6, div0);
    			append_dev(div0, h3);
    			append_dev(div6, t2);
    			append_dev(div6, div4);
    			append_dev(div4, div3);
    			append_dev(div3, div1);
    			append_dev(div1, label0);
    			append_dev(div1, t4);
    			append_dev(div1, input0);
    			set_input_value(input0, ctx.email);
    			append_dev(div3, t5);
    			append_dev(div3, div2);
    			append_dev(div2, label1);
    			append_dev(div2, t7);
    			append_dev(div2, input1);
    			set_input_value(input1, ctx.password);
    			append_dev(div6, t8);
    			append_dev(div6, div5);
    			append_dev(div5, button);
    			if_block1.m(button, null);
    		},
    		p: function update(changed, ctx) {
    			if (ctx.error != null) {
    				if (if_block0) {
    					if_block0.p(changed, ctx);
    				} else {
    					if_block0 = create_if_block_1(ctx);
    					if_block0.c();
    					if_block0.m(div9, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (changed.email && input0.value !== ctx.email) {
    				set_input_value(input0, ctx.email);
    			}

    			if (changed.password && input1.value !== ctx.password) {
    				set_input_value(input1, ctx.password);
    			}

    			if (current_block_type !== (current_block_type = select_block_type(changed, ctx))) {
    				if_block1.d(1);
    				if_block1 = current_block_type(ctx);

    				if (if_block1) {
    					if_block1.c();
    					if_block1.m(button, null);
    				}
    			}

    			if ((changed.password || changed.email) && button_disabled_value !== (button_disabled_value = ctx.password === "" || ctx.email === "")) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div10);
    			if (if_block0) if_block0.d();
    			if_block1.d();
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let email = "";
    	let password = "";
    	let error = null;
    	let loading = false;

    	function login(event) {
    		$$invalidate("loading", loading = true);
    		$$invalidate("error", error = null);

    		POST("/token", { el_pastas: email, slaptazodis: password }, false).then(res => {
    			$$invalidate("loading", loading = false);
    			localStorage.clear();

    			if (res.status) {
    				localStorage.setItem("user_id", res.data["user"]["id"]);
    				localStorage.setItem("is_admin", res.data["user"]["ar_administratorius"]);
    				localStorage.setItem("permissions", res.data["user"]["teises"]);
    				localStorage.setItem("token", res.data["token"]);
    				window.location.replace("/users");
    				return;
    			}

    			if (res.data.status === 401) {
    				$$invalidate("error", error = "Neteisingas el. pašto adresas arba slaptažodis.");
    				return;
    			} else if (res.data.status === 404) {
    				$$invalidate("error", error = "Toks el. pašto adresas neegzistuoja.");
    				return;
    			}

    			$$invalidate("error", error = "Įvyko klaida. Bandykite dar kartą.");
    		});
    	}

    	const click_handler = () => $$invalidate("error", error = null);

    	function input0_input_handler() {
    		email = this.value;
    		$$invalidate("email", email);
    	}

    	function input1_input_handler() {
    		password = this.value;
    		$$invalidate("password", password);
    	}

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("email" in $$props) $$invalidate("email", email = $$props.email);
    		if ("password" in $$props) $$invalidate("password", password = $$props.password);
    		if ("error" in $$props) $$invalidate("error", error = $$props.error);
    		if ("loading" in $$props) $$invalidate("loading", loading = $$props.loading);
    	};

    	return {
    		email,
    		password,
    		error,
    		loading,
    		login,
    		click_handler,
    		input0_input_handler,
    		input1_input_handler
    	};
    }

    class Main extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Main",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    const app = new Main({
    	target: document.body,
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map

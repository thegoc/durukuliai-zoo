import { writable } from 'svelte/store';

const users = writable({ isSet: false, users: [1] });

const usersStore = {
    subscribe: ()  => { return users.subscribe },
};

export default usersStore;
